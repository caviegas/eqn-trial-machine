<?php

/**
 * @file
 * Contains \Drupal\commerce_coupon\Entity\Controller\TermListBuilder.
 */

namespace Drupal\commerce_coupon\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Provides a list controller for commerce_coupon entity.
 *
 * @ingroup coupon
 */
class CouponListBuilder extends EntityListBuilder {

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;


  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    // $value = \Drupal::request()->get('campaign_id');
    // dd($container->get('entity.manager')->getStorage($entity_type->id()));
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('url_generator')
    );
  }

  /**
   * Constructs a new couponTermListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type term.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, UrlGeneratorInterface $url_generator) {
    parent::__construct($entity_type, $storage);
    $this->urlGenerator = $url_generator;
  }


  protected function getEntityIds() {
    $query = \Drupal::entityQuery($this->entityTypeId);
    $request = \Drupal::request();
  
    $campaign_id = $request->get('campaign_id') ?? 0;
    if ($campaign_id) {
      $query->condition('campaign_id', $campaign_id[0]['target_id']);
    }
  
    if ($this->limit) {
      $query->pager($this->limit);
    }
  
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    // $build['description'] = array(
    //   '#markup' => $this->t('Content Entity Example implements a couponTerms model. These are fieldable entities. You can manage the fields on the <a href="@adminlink">Term admin page</a>.', array(
    //     '@adminlink' => $this->urlGenerator->generateFromRoute('entity.coupon.term_settings'),
    //   )),
    // );
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\commerce_coupon\Form\CouponFormFilter');
    $build['table'] = parent::render();

    // dd($build);
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the commerce_coupon list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Título');
    $header['campaign'] = $this->t('Campanha');
    $header['code'] = $this->t('Código');
    $header['start_date'] = $this->t('Início');
    $header['end_date'] = $this->t('Término');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\commerce_coupon\Entity\Campaign */
    $row['id'] = $entity->id();
    $row['name'] = $entity->name->value;
    $row['campaign_id'] = $entity->campaign_id->entity->title->value;
    $row['code'] = $entity->code->value;
    $row['start_date'] = $entity->start_date->value;
    $row['end_date'] = $entity->end_date->value;
    return $row + parent::buildRow($entity);
  }

}

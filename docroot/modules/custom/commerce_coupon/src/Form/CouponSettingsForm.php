<?php
/**
 * @file
 * Contains \Drupal\commerce_coupon\Form\CouponSettingsForm.
 */

namespace Drupal\commerce_coupon\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContentEntityExampleSettingsForm.
 *
 * @package Drupal\commerce_coupon\Form
 *
 * @ingroup coupon
 */
class CouponSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'commerce_coupon_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['commerce_coupon_settings']['#markup'] = 'Settings form for coupon Term. Manage field settings here.';
    return $form;
  }

}

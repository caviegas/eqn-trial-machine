<?php
/**
 * @file
 * Contains \Drupal\commerce_coupon\Form\TermForm.
 */

namespace Drupal\commerce_coupon\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the content_entity_example entity edit forms.
 *
 * @ingroup content_entity_example
 */
class CouponForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\commerce_coupon\Entity\Coupon */
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // dd($form_state->getValues());
    // Redirect to term list after save.
    $form_state->setRedirect('entity.commerce_coupon.collection');
    $entity = $this->getEntity();
    $entity->save();
  }
}

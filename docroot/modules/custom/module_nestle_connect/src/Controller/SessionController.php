<?php

namespace Drupal\module_nestle_connect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Session\AccountProxy;
use Drupal\meeg_content\Controller\UserController;


/**
 * This is our session controller.
 */
class SessionController extends ControllerBase {

  // protected $configFactory;
  // protected $currentUser;

  // public static function create(ContainerInterface $container) {
  //   return new static(
  //     $container->get('config.factory'),
  //     $container->get('current_user')
  //   );
  // }

  // public function __construct(ConfigFactory $configFactory, AccountProxy $currentUser) {
  //   $this->configFactory = $configFactory;
  //   $this->currentUser = $currentUser;
  // }

  public function welcome() {
    return "ok";
  }

  public function setChildren($attr, $activeChild = 0, $activeAge = 0) {
    $userController = new UserController;

    // ## Organizing children in session
    // Index number of crias
    $indexes;
    foreach ($attr as $key => $value) {
      if (strpos($key, 'dt_nascimento') === 0) {
        $str = explode('_', $key);
        $indexes[] = $str[3] + 0;
      }
    }

    $children = [];
    // Loop through them
    if(!empty($indexes)){
      for ($i = 1; $i <= max($indexes); $i++) {
        if($i < 10){
          $i = sprintf("%02d", $i);
        }
        $children = array_merge($children, [
          [
            'name' => $attr['nm_filho_'.$i],
            'birthday' => $attr['dt_nascimento_filho_'.$i],
            'gender' => $attr['id_sexo_filho_'.$i]
          ]
        ]);
      }
    }


    if ($children != []) {
      // dd($_SESSION["get_attributes"]);
      // $activeChild = (isset($_SESSION["get_attributes"]["actives"]["child"])) ? $_SESSION["get_attributes"]["actives"]["child"] : 0;
      // $activeAge = (isset($_SESSION["get_attributes"]["actives"]["age_filter"])) ? $_SESSION["get_attributes"]["actives"]["age_filter"] : 0;

      $_SESSION["get_attributes"]["children"] = $children;
      $_SESSION["get_attributes"]["age_filters"] = $userController->setAvailableAgeFilters($children);
      $_SESSION["get_attributes"]["actives"]["child"] = $activeChild;
      $_SESSION["get_attributes"]["actives"]["age_filter"] = $activeAge;
    }
  }

}

<?php

namespace Drupal\module_nestle_connect\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Drupal\user;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Drupal\block_content\Controller\UserController;
use Drupal\module_nestle_connect\Controller\SessionController;
use Symfony\Component\HttpFoundation\RedirectResponse;



/**
 * Our custom ajax form.
 */
class formRecuperarSenha extends FormBase {

  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
     return "form_recuperar_senha";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Campos do formulário
     */
    if (\Drupal::currentUser()->isAuthenticated()) {
      return new RedirectResponse('/');   
    }

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Seu e-mail *'),
      '#attributes' => array(
        'autocomplete' => 'off',
      ),
    ];

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
    ]; 

    // $form['captcha'] = array(
    //   '#type' => 'captcha',
    //   '#captcha_type' => 'recaptcha/reCAPTCHA',
    // );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enviar'),
      '#button_type' => 'primary',
    ];
    

     return $form;

   }

   public function validateForm(array &$form, FormStateInterface $form_state){

      if($form_state->getValues()['email'] == ''){
        $form_state->setErrorByName('email', 'Você precisa informar o seu e-mail.');
        $_SESSION['loginAttempts'] = 2;
      }

   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sessionController = new SessionController;

    $email =  $form_state->getValues()['email'];
        try {
          $data = [
            'email' => $form_state->getValues()['email'],
            'domainName' => \Drupal::request()->getHost(),
          ];
          $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
              'trace' => 1,
          ));
          $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
          $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
          $client->__setSoapHeaders($header);
          $serviceStatus = $client->IsServiceAvailable();
          if ($serviceStatus != true) {
              $message = 'Serviço indisponível';
              return $message;
          }

          $res = $client->SendEmailForgotPasswordWithDomain($data);
          // $_SESSION['userForgotPasswordMail'] = $form_state->getValues()['email'];

          if($res->SendEmailForgotPasswordWithDomainResult !== null){
            // quando o email não existe na base
            \Drupal::messenger()->addMessage('Acesse seu e-mail e siga as orientações para recuperar sua senha.');
          }else{
            \Drupal::messenger()->addMessage('Acesse seu e-mail e siga as orientações para recuperar sua senha.');
          }
          
        } catch (SoapFault $exception) {
          // $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
          // \Drupal::messenger()->addError($response);
          // \Drupal::messenger()->addError($get_attributes);
          \Drupal::messenger()->addError('Login temporariamente indisponível');
        }
      
    return "Ok";
  }



}

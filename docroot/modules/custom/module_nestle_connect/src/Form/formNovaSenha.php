<?php

namespace Drupal\module_nestle_connect\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Drupal\user;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Drupal\block_content\Controller\UserController;
use Drupal\module_nestle_connect\Controller\SessionController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Our custom ajax form.
 */
class formNovaSenha extends FormBase {

  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
     return "form_nova_senha";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Campos do formulário
     */
    if (\Drupal::currentUser()->isAuthenticated()) {
      return new RedirectResponse('/');   
    }

    $form['novaSenha'] = [
      '#type' => 'password',
      '#title' => 'Sua nova senha',
    ];

    $form['confirmaNovaSenha'] = [
      '#type' => 'password',
      '#title' => 'Confirme a sua nova senha',
    ];

    $form['token'] = [
      '#type' => 'hidden',
      '#default_value' => $_GET['token'],
    ];

    // $form['message'] = [
    //   '#type' => 'markup',
    //   '#markup' => '<p>Token: ' . $_GET['token'] . '<p>'
    // ]; 

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Atualizar senha'),
      '#button_type' => 'primary',
    ];
    

     return $form;

   }

   public function validateForm(array &$form, FormStateInterface $form_state){

    if($form_state->getValues()['novaSenha'] != $form_state->getValues()['confirmaNovaSenha']){
      $form_state->setErrorByName('novaSenha', $this->t('As senhas que você informa precisam ser iguais.'));
    }else{
      $count_senha = 0;
      if (preg_match('/[\'^£$%&*()!}{@#~?><>,|=_+¬-]/', $form_state->getValues()['novaSenha']) )
        {
          $count_senha += 1;
        }
        
        if (preg_match("/[0-9]+/", $form_state->getValues()['novaSenha'])) {
          $count_senha += 1;
        } 

        if(preg_match('/[A-Z]/', $form_state->getValues()['novaSenha'])){
            $count_senha += 1;
        }

        if(preg_match('/[a-z]/',  $form_state->getValues()['novaSenha'])){
            $count_senha += 1;
        }

        if(strlen($form_state->getValues()['novaSenha']) <= 5 || strlen($form_state->getValues()['novaSenha']) > 10){
          $form_state->setErrorByName('novaSenha', $this->t('A senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maiúscula, letra minúscula, números e caracteres especiais.'));
        }

        if($count_senha < 3){
          $form_state->setErrorByName('novaSenha', $count_senha . 'A senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maiúscula, letra minúscula, números e caracteres especiais.');
          $form_state->setErrorByName('confirmaNovaSenha', $this->t('A senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maiúscula, letra minúscula, números e caracteres especiais.'));
        }
    }

   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sessionController = new SessionController;

        try {

          $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
              'trace' => 1,
          ));
          $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
          $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
          $client->__setSoapHeaders($header);
          $serviceStatus = $client->IsServiceAvailable();
          if ($serviceStatus != true) {
              \Drupal::messenger()->addError('Serviço indisponível');
          }

          $validateToken = $client->ValidateTokenForgotPassword(['token' => $form_state->getValues()['token']]);

          if($validateToken->ValidateTokenForgotPasswordResult){
            $emailToken = $validateToken->ValidateTokenForgotPasswordResult;

            $data = [
              'username' => $emailToken,
              'token' => $form_state->getValues()['token'],
              'newPassword' => $form_state->getValues()['novaSenha']
            ];
            $res = $client->ChangePasswordByToken($data);

            if($res->ChangePasswordByTokenResult){
              // \Drupal::messenger()->addMessage('Alterou a senha');
              
              // aqui loga o usuario
  
              $resUser = $client->GetUser(['username' => $emailToken]);
              $_SESSION['get_user']['email'] = $emailToken;
              updateUserSession();
  
              

              $user = user_load_by_mail($emailToken);
              if($user){
                $uid = $user->id();
                $user = \Drupal\user\Entity\User::load($uid);

                // user_login_finalize($user);
                // \Drupal::messenger()->addMessage('Senha alterada com sucesso!');

                updateUserSession();
                // $redirect_path = "/";
                // $url = url::fromUserInput($redirect_path);
                // $form_state->setRedirectUrl($url);
              }else{
                  /**
                   * Caso o usuário tente logar com um login que só existe na API, cria usuário no Drupal
                   */
                  $nm_user_drupal = $resUser->GetUserResult->Nome;
                  $randon_user = bin2hex(openssl_random_pseudo_bytes(8));
                  $nm_user_drupal = $nm_user_drupal."-".$randon_user;
                  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
                  $user = \Drupal\user\Entity\User::create();
                  $user->setPassword($resUser->GetUserResult->Senha);
                  $user->enforceIsNew();
                  $user->setEmail($emailToken);
                  $user->setUsername($nm_user_drupal);//This username must be unique and accept only a-Z,0-9, - _ @ .
                  $user->set("init", 'email');
                  $user->set("langcode", $language);
                  $user->set("preferred_langcode", $language);
                  $user->set("preferred_admin_langcode", $language);
                  // $user->activate();
                  $user->save();
                  // user_login_finalize($user);
              }
              // $uid = $user->id();
              // $user = \Drupal\user\Entity\User::load($uid);
              // \Drupal::messenger()->addMessage("Login efetuado!");
              // user_login_finalize($user);

              \Drupal::messenger()->addMessage('Senha alterada com sucesso!');
              $redirect_path = "/user/entrar";
              $url = url::fromUserInput($redirect_path);
              $form_state->setRedirectUrl($url);
  
              updateUserSession();

          }else{
            \Drupal::messenger()->addError('Erro ao alterar a senha');
            // algum erro no token
          }

   
          }else{
            \Drupal::messenger()->addError('Erro ao alterar a senha');
          }
          
        } catch (SoapFault $exception) {
          \Drupal::messenger()->addError('Login temporariamente indisponível');
          // $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
          // \Drupal::messenger()->addError($response);
          // \Drupal::messenger()->addError($get_attributes);
        }
      
    return "Ok";
  }



}

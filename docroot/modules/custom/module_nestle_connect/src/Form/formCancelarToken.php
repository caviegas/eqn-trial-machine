<?php

namespace Drupal\module_nestle_connect\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Drupal\user;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Drupal\block_content\Controller\UserController;
use Drupal\module_nestle_connect\Controller\SessionController;


/**
 * Our custom ajax form.
 */
class formCancelarToken extends FormBase {

  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
     return "form_cancelar_token";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Campos do formulário
     */
    $form['token'] = [
      '#type' => 'hidden',
      '#default_value' => $_GET['token'],
      '#attributes'    => [
        'window.onload' => 'this.form.submit();',
      ],
    ];

  if($_GET['TipoSolicitacao'] == 'ConfirmationRegister'){
    $form['#title'] = t('Cancelar confirmação');
    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => '<div><p class="opening">Ao clicar no botão abaixo, você cancela a sua solicitação de criação de conta.</p></div>'
    ];
  }else if($_GET['TipoSolicitacao'] == 'ForgotPassword'){
    $form['#title'] = t('Cancelar troca de senha');
    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => '<div><p class="opening">Ao clicar no botão abaixo, você cancela a sua solicitação de troca de senha.</p></div>'
    ];
  }


    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Cancelar solicitação',
      '#button_type' => 'primary',
      '#attributes'    => [
        'window.onload' => 'this.form.submit();',
      ],
    ];
    

     return $form;

   }

   public function validateForm(array &$form, FormStateInterface $form_state){



   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sessionController = new SessionController;

        try {
          $data['CancelSolicitation'] = [
            'token' => $form_state->getValues()['token'],
            // 'type' => $_GET['TipoSolicitacao']
          ];
          $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
              'trace' => 1,
          ));
          $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
          $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
          $client->__setSoapHeaders($header);
          $serviceStatus = $client->IsServiceAvailable();
          if ($serviceStatus != true) {
              $message = 'Serviço indisponível';
              return $message;
          }

          $res = $client->CancelSolicitation(['token' => $form_state->getValues()['token'], 'type' => $_GET['TipoSolicitacao']]);

          if ($res->CancelSolicitationResult == null){
            \Drupal::messenger()->addError('Esta solicitação de confirmação de cadastro já foi utilizada ou não está mais válida, favor solicitar novamente a sua confirmação.');
          }else{
            \Drupal::messenger()->addMessage('Sua solicitação foi cancelada com sucesso!');
            $redirect_path = "/";
            $url = url::fromUserInput($redirect_path);
            $form_state->setRedirectUrl($url);
          }


          
        } catch (SoapFault $exception) {
          // $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
          // \Drupal::messenger()->addError($response);
          // \Drupal::messenger()->addError($get_attributes);
          \Drupal::messenger()->addError('Login temporariamente indisponível');
        }
      
    return "Ok";
  }



}

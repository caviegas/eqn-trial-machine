<?php

namespace Drupal\module_nestle_connect\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Drupal\user\Entity\User;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Drupal\block_content\Controller\UserController;
use Drupal\module_nestle_connect\Controller\SessionController;


/**
 * Our custom ajax form.
 */
class formAtivarConta extends FormBase {

  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
     return "form_ativar_conta";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Campos do formulário
     */    
    $form['token'] = [
      '#type' => 'hidden',
      '#default_value' => $_GET['token'],
      '#attributes'    => [
        'window.onload' => 'this.form.submit();',
      ],
    ];

    $form['image'] = [
      '#type' => 'markup',
      '#markup' => '<img src="https://res.cloudinary.com/meeg-cloud/image/upload/v1630009145/TM/confirmation_gxxvne.svg" class="account-confirmation" alt="Ativar conta" title="Ativar conta" />'
    ];

    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => '<div><p class="opening">Ao clicar no botão abaixo, você confirma definitivamente a criação de uma conta em nosso site, garantindo a veracidade e a solidez dos seus dados conosco.</p></div>'
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Ativar minha conta',
      '#button_type' => 'primary',
      '#attributes'    => [
        'window.onload' => 'this.form.submit();',
      ],
    ];
    

     return $form;

   }

   public function validateForm(array &$form, FormStateInterface $form_state){



   }



   
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $sessionController = new SessionController;
    \Drupal::request()->query->remove('destination');
        try {
          $data = [
            'token' => $form_state->getValues()['token'],
          ];
          $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
              'trace' => 1,
          ));
          $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
          $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
          $client->__setSoapHeaders($header);
          $serviceStatus = $client->IsServiceAvailable();
          if ($serviceStatus != true) {
              $message = 'Serviço indisponível';
              return $message;
          }

      
          $res = $client->GetUserByConfirmationToken($data);

          if ($res->GetUserByConfirmationTokenResult == null){
            \Drupal::messenger()->addError('Esta solicitação de confirmação de cadastro já foi utilizada ou não está mais válida, favor solicitar novamente a sua confirmação.');
            // \Drupal::messenger()->addMessage('Sua conta foi ativada com sucesso!');
            // $form_state->disableRedirect();
            $redirect_path = "/user/solicitar_confirmacao?destination=";
            $url = url::fromUserInput($redirect_path);
            $form_state->setRedirectUrl($url);
          
          }else{
            

            $userId = user_load_by_mail($res->GetUserByConfirmationTokenResult->Email);
            // \Drupal::messenger()->addMessage($res->GetUserByConfirmationTokenResult->Email);
            $uid = $userId->id();

            $user = User::load($userId->id());

            $user->activate();
            $user->save();


            $message = '<script>var script = document.createElement("script"); script.innerHTML = "window.dataLayer = window.dataLayer || []; dataLayer.push({\'event\': \'Conta ativada\',\'visitorId\': \'' . $res->GetUserByConfirmationTokenResult->Codigo . '\'})"; var head = document.getElementsByTagName("head")[0]; head.insertBefore(script, head.firstChild)</script>';
            $rendered_message = \Drupal\Core\Render\Markup::create($message);
            \Drupal::messenger()->addWarning(\Drupal\Core\Render\Markup::create($rendered_message));

            \Drupal::messenger()->addMessage('Sua conta foi ativada com sucesso!');
            // $form_state->disableRedirect();
            \Drupal::request()->query->remove('destination');
            if(isset($_GET['destination'])){
              $redirect_path = "/user/entrar?destination=" . $_GET['destination'];
              
            }else{
              $redirect_path = "/user/entrar?destination=/"; // TODO redirect after login
            }
            
            $url = url::fromUserInput($redirect_path);
            $form_state->setRedirectUrl($url);
          }


          // $form_state->disableRedirect();
        } catch (SoapFault $exception) {
          // $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
          // \Drupal::messenger()->addError($response);
          // \Drupal::messenger()->addError($get_attributes);
          \Drupal::messenger()->addError('Login temporariamente indisponível');
        }
      
    return "Ok";
  }



}

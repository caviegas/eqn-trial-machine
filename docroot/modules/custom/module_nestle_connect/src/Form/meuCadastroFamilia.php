<?php

namespace Drupal\module_nestle_connect\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;

use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Drupal\module_nestle_connect\Controller\SessionController;


/**
 * Our custom ajax form.
 */
class meuCadastroFamilia extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "form_meu_cadastro_familia";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {
    // dd($_SESSION);
    /**
     * Meu campos
     */  

    
    if (\Drupal::currentUser()->isAuthenticated()) {
      $x = "";
      updateUserSession();
    } else {
      return new RedirectResponse('/user/entrar');   
      }

    if(isset($_SESSION["get_attributes"]["id_estado_civil"])){
      $civil = $_SESSION["get_attributes"]["id_estado_civil"];
    }else{
      $civil = "";
    }

      
    $form['opening'] = [
      '#type' => 'markup',
      '#markup' => '<div><p class="opening">Fale um pouco mais sobre você para que a gente possa lhe entender melhor.</p></div>'
    ];

    $form['tabmenu'] = [
      '#type' => 'markup',
      '#markup' => getUserDetailsTabMenu()
    ];

    $form['guidemessage'] = [
        '#type' => 'markup',
        '#markup' => '<div class="guide_message"><span class="family_icon"></span><p>Conhecendo melhor sua família, vamos entender melhor suas necessidades e demandas. :)</p></div><div>'
      ];

    
    $form['estado_civil'] = [
        '#type' => 'select',
        '#default_value' =>  $civil,
        '#wrapper' => 'edit-username',
        '#title' => $this->t('Estado civil'),
        '#options' => [
            '0' => $this->t('Selecionar'),
            '1' => $this->t('Casado'),
            '2' => $this->t('Divorciado'),
            '3' => $this->t('Solteiro'),
            '4' => $this->t('Viúvo'),
        ],
      ];

    $form['message2'] = [
      '#type' => 'markup',
      '#markup' => '<div class="interlabel"><p>VOCÊ TEM CRIANÇAS NA SUA FAMÍLIA?</p><span>Cadastre as crianças para que a gente mostre conteúdos especiais para as faixas de idade.</span></div>'
    ];

    // aqui monta os forms das crianças
    $criancaNaoCadastradaNr = 1;
    for($x = 1; $x <= 10; $x++){
      if($x < 10){
        $x = sprintf("%02d", $x);
      }

      isset($_SESSION["get_attributes"]["nm_filho_" . $x]) ? ${'nm_' . $x} = $_SESSION["get_attributes"]["nm_filho_" . $x] : ${'nm_' . $x} = "";
      isset($_SESSION["get_attributes"]["id_sexo_filho_" . $x]) ? ${'gn_' . $x} = $_SESSION["get_attributes"]["id_sexo_filho_" . $x] : ${'gn_' . $x} = 0;

      if(isset($_SESSION["get_attributes"]["dt_nascimento_filho_" . $x])){
        $dt = explode("/", $_SESSION["get_attributes"]["dt_nascimento_filho_" . $x]);
        ${'dt_' . $x} = $dt[2]."-".$dt[1]."-".$dt[0];
      }else{
       ${'dt_' . $x} = "";
      }



      if(${'nm_' . $x} !== "" || ${'dt_' . $x} !== ""){
        // aqui as crianças já cadastradas
        if(${'gn_' . $x} == 'M'){
          $genero = 'Menino';
        }else if(${'gn_' . $x} == 'F'){
          $genero = 'Menina';
        }else{
          $genero = 'Não declarado';
        }

        if(${'nm_' . $x} == ""){
          // se a criança não tem nome informado
          $form['nome_crianca_' . $x] = [
            '#type' => 'textfield',

            '#default_value' =>  ${'nm_' . $x},
            '#title' => $this->t('Nome completo criança'),
            '#prefix' => '<div class="box_crianca_cadastrada crianca_' . $x . '"><div class="crianca_cadastrada"><span></span><span>' . $_SESSION["get_attributes"]["dt_nascimento_filho_" . $x] . '</span><span>' . $genero . '</span></div>',
          ];
        }else{
          // se a criança tem todos os dados preenchidos
          $form['nome_crianca_' . $x] = [
            '#type' => 'textfield',

            '#default_value' =>  ${'nm_' . $x},
            '#title' => $this->t('Nome completo criança'),
            '#prefix' => '<div class="box_crianca_cadastrada crianca_' . $x . '"><div class="crianca_cadastrada"><span>' . ${'nm_' . $x} . '</span><span>' . $_SESSION["get_attributes"]["dt_nascimento_filho_" . $x] . '</span><span>' . $genero . '</span></div>',
          ];
        }

    
        $form['datanascimento_crianca_' . $x] = array(
          '#type' => 'date',

          '#default_value' => ${'dt_' . $x},
          '#title' => $this->t('Data de nascimento *'),
          '#suffix' => '<span class="dn-validate-message"></span>'
        );
    
        $form['genero_crianca_' . $x] = [
          '#type' => 'select',
          '#id' => 'genero_crianca_' . $x,
          '#default_value' =>  ${'gn_' . $x},

          '#title' => $this
            ->t('Gênero *'),
            '#options' => [
              '' => $this->t('Selecionar'),
              'F' => $this->t('Menina'),
              'M' => $this->t('Menino'),
              '0' => $this->t('Não declarado'),
            ],
          '#suffix' => '</div>',
        ];
      }else{
        // aqui as crianças em branco
        if($criancaNaoCadastradaNr == 1){
          $form['nome_crianca_' . $x] = [
            '#type' => 'textfield',
            '#default_value' =>  ${'nm_' . $x},
            '#title' => $this->t('Nome completo'),
            '#prefix' => '<div class="box_crianca_nao_cadastrada show crianca_' . $x . '"><div class="adicionar_crianca">Adicionar criança</div>',
          ];
      
          $form['datanascimento_crianca_' . $x] = array(
            '#type' => 'date',
  
            '#default_value' => ${'dt_' . $x},
            '#title' => $this->t('Data de nascimento *'),
            '#suffix' => '<span class="nome-valid-message"></span>'
          );
      
          $form['genero_crianca_' . $x] = [
            '#type' => 'select',
            '#id' => 'genero_crianca_' . $x,
            '#default_value' =>  '',
            '#title' => $this->t('Gênero *'),
            '#options' => [
              '' => $this->t('Selecionar'),
              'F' => $this->t('Menina'),
              'M' => $this->t('Menino'),
              '0' => $this->t('Não declarado'),
            ],
            '#suffix' => '</div>',
          ];
          $criancaNaoCadastradaNr++;
        }else{
          echo ${'nm_' . $x};
          $form['nome_crianca_' . $x] = [
            '#type' => 'textfield',
            '#default_value' =>  ${'nm_' . $x},
            '#title' => $this->t('Nome completo'),
            '#prefix' => '<div class="box_crianca_nao_cadastrada crianca_' . $x . '"><div class="adicionar_crianca">Adicionar outra criança</div>',
          ];
      
          $form['datanascimento_crianca_' . $x] = array(
            '#type' => 'date',
            '#default_value' => ${'dt_' . $x},
            '#title' => $this->t('Data de nascimento *'),
            '#suffix' => '<span class="nome-valid-message"></span>'
          );
      
          $form['genero_crianca_' . $x] = [
            '#type' => 'select',
            '#id' => 'genero_crianca_' . $x,
            '#default_value' =>  '',
            '#title' => $this->t('Gênero *'),
            '#options' => [
              '' => $this->t('Selecionar'),
              'F' => $this->t('Menina'),
              'M' => $this->t('Menino'),
              '0' => $this->t('Não declarado'),
            ],
            '#suffix' => '</div>',
          ];
          $criancaNaoCadastradaNr++;
        }
        
      }
    }

    $form['close'] = [
      '#type' => 'markup',
      '#markup' => '</div>'
    ];

    $form['message'] = ['#type' => 'markup','#markup' => '<div class="result_message"></div>']; 
  
    $form['submit_group_a'] = [
      '#type' => 'markup',
      '#markup' => '<div class="form-nav">
        <a href="basico" class="form-nav-left">Voltar</a>
        <a href="detalhamento" class="form-nav-right">Pular</a>
      </div>'
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SALVAR E AVANÇAR'),
      '#button_type' => 'primary',
    ];

     return $form;
   }

   public function validateForm(array &$form, FormStateInterface $form_state)
  {
    for($x = 1; $x <= 10; $x++){
      if($x < 10){
        $x = sprintf("%02d", $x);
      }
      $nascimentoCrianca = 'datanascimento_crianca_' . $x;
      $generoCrianca = 'genero_crianca_' . $x;
      $nomeCrianca = 'nome_crianca_' . $x;
      
      if($form_state->getValue($nascimentoCrianca) !== ''){
        if($form_state->getValues()[$generoCrianca] == ''){
          $form_state->setErrorByName($generoCrianca, $this->t('Você precisa sempre informar o gênero da criança.'));
        }
      }

      if($form_state->getValues()[$generoCrianca] !== ''){
        if($form_state->getValues()[$nascimentoCrianca] == ''){
          $form_state->setErrorByName($nascimentoCrianca, $this->t('Você precisa sempre informar a data de nascimento da criança.'));
        }else if($form_state->getValues()[$nascimentoCrianca] !== ''){
          $date = DrupalDateTime::createFromFormat('Y-m-d', $form_state->getValues()[$nascimentoCrianca])->getTimeStamp();
          $now = \Drupal::time()->getCurrentTime();
          if($date > $now){
            $form_state->setErrorByName($nascimentoCrianca, 'Você não pode informar uma data de nascimento no futuro.');
          }
        }
      }

      // if($form_state->getValues($nomeCrianca) !== ''){
      //   if($form_state->getValues()[$nascimentoCrianca] == ''){
      //     $form_state->setErrorByName($nascimentoCrianca, $this->t('Você precisa sempre informar a data de nascimento da criança.'));
      //   }else if($form_state->getValues()[$nascimentoCrianca] !== ''){
      //     $date = DrupalDateTime::createFromFormat('Y-m-d', $form_state->getValues()[$nascimentoCrianca])->getTimeStamp();
      //     $now = \Drupal::time()->getCurrentTime();
      //     if($date > $now){
      //       $form_state->setErrorByName($nascimentoCrianca, 'Você não pode informar uma data de nascimento no futuro.');
      //     }
      //   }
      //   if($form_state->getValues()[$generoCrianca] == ''){
      //     $form_state->setErrorByName($generoCrianca, $this->t('Você precisa sempre informar o gênero da criança.'));
      //   }
      //   // if($form_state->getValue($generoCrianca) == ''){
      //   //   $form_state->setErrorByName($generoCrianca, 'Você precisa sempre informar o gênero da criança.');
      //   // }
      // }
    }    


  }


   public function submitForm(array &$form, FormStateInterface $form_state){
    $response = new AjaxResponse();
    $sessionController = new SessionController;

    try {
      $client = new SoapClient($GLOBALS['CADU_API_URL'], array('trace' => 1,));
      
      // SET HEADERS
      $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
      $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
      $client->__setSoapHeaders($header);
      
      // Check if service is available
      $serviceStatus = $client->IsServiceAvailable();
      if ($serviceStatus != true) {
        $response->addCommand(
          new HtmlCommand(
            '.result_message',
            '<div class="my_top_message">Serviço indisponivel</div>'),
        );
      }

      $codeuser = $_SESSION["get_user"]['codigo'];
      $estado_civil = $form_state->getValues()['estado_civil'];

      
      $data_atributos['atributos'] = [
          [
              'CodigoVisitante' => $codeuser,
              'NomeAtributo' => 'id_estado_civil',
              'Valor' => $estado_civil,
              'Items' => [
                  'Items' => [
                      "Id" => 8347
                  ]
              ]
          ]
      ];
  
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;

      $redirect_path = "/";
      $url = url::fromUserInput($redirect_path);
      $form_state->setRedirectUrl($url);

  } catch (SoapFault $exception) {
      $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
      \Drupal::messenger()->addError($response);
  }



  $num_item = 0;
                
  for($x = 1; $x <= 10; $x++){
    if($x < 10){
      $num_item = "0".$x;
  }else{
      $num_item = $x;
  }
      if($form_state->getValues()['nome_crianca_'.$num_item] != "" || $form_state->getValues()['datanascimento_crianca_'.$num_item] !== ""){
          if($form_state->getValues()['datanascimento_crianca_'.$num_item] == "" || $form_state->getValues()['genero_crianca_'.$num_item] == ""){
             
              // \Drupal::messenger()->addError("Preencha todos os campos para o filho número ".$x);
              $x = 10;
              $response->addCommand(
                new HtmlCommand(
                  '.result_message',
                  '<div class="result_message">deu erro nesse troco</div>'),
              );
             
          }else{
              try {

                  $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
                  'trace' => 1,
                  ));
              
                  $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
                  $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
                  $client->__setSoapHeaders($header);
                  $serviceStatus = $client->IsServiceAvailable();
                  if ($serviceStatus != true) {
                      $message = 'Serviço indisponível';
                      return $message;
                  }

                  $codeuser = $_SESSION["get_user"]['codigo'];


                  if($x < 10){
                      $num_item = "0".$x;
                  }else{
                      $num_item = $x;
                  }

                  $data_atributos['atributos'] = [
                      [
                          'CodigoVisitante' => $codeuser,
                          'NomeAtributo' => 'nm_filho_'.$num_item,
                          'Valor' => $form_state->getValues()['nome_crianca_'.$num_item],
                          'Items' => [
                              'Items' => [
                                  "Id" => 8347
                              ]
                          ]
                      ],[
                          'CodigoVisitante' => $codeuser,
                          'NomeAtributo' => 'dt_nascimento_filho_'.$num_item,
                          'Valor' => date('d/m/Y', strtotime($form_state->getValues()['datanascimento_crianca_'.$num_item])),
                          'Items' => null
                      ],[
                          'CodigoVisitante' => $codeuser,
                          'NomeAtributo' => 'id_sexo_filho_'.$num_item,
                          'Valor' => $form_state->getValues()['genero_crianca_'.$num_item],
                          'Items' => [
                             'Items' => [
                                "Id" => 9751
                             ]
                          ]
                      ]
                  ];

                      
                  $res = $client->SaveAttributes($data_atributos);
                  $res_atributes = $res->SaveAttributesResult;

                  // $res_attr = $client->GetAttributes(['userName' => $_SESSION["get_user"]['email']]);
                  // $attributes = $res_attr->GetAttributesResult->Atributo;

                  // $attr = [];
                  // foreach ($attributes as $value) {
                
                  //   if($value->ValorMultivalorado == null && $value->Valor !== null){
                  //     $attr = array_merge($attr, [
                  //       $value->NomeAtributo => $value->Valor   
                  //     ]);
                  //   }else{
                  //     $attr = array_merge($attr, [
                  //       $value->NomeAtributo => $value->ValorMultivalorado   
                  //   ]);
                  //   }
                  // }


                  // $_SESSION["get_attributes"] = $attr;
                  // $sessionController->setChildren($attr);

                  /**
                  * Cria session do usuário
                  */
                  // $res = $client->GetUser(['username' => $_SESSION["get_user"]['email']]);

                  // $attr_user = [
                  //   "ativo" => $res->GetUserResult->Ativo,
                  //   "cpf" => $res->GetUserResult->CPF,
                  //   "cpf_responsavel" => $res->GetUserResult->CPFResponsavel,
                  //   "codigo" => $res->GetUserResult->Codigo,
                  //   "codigo_area_site_criacao" => $res->GetUserResult->CodigoAreaSiteCriacao,
                  //   "confirmacao_email" => $res->GetUserResult->ConfirmacaoEmail,
                  //   "data_acesso_bloquado" => $res->GetUserResult->DataAcessoBloqueado,
                  //   "data_alteracao" => $res->GetUserResult->DataAlteracao,
                  //   "data_criacao" => $res->GetUserResult->DataCriacao,
                  //   "data_nascimento" => $res->GetUserResult->DataNascimento,
                  //   "data_ultimo_login" => $res->GetUserResult->DataUltimoLogin,
                  //   "email" => $res->GetUserResult->Email,
                  //   "email_responsavel" => $res->GetUserResult->EmailResponsavel,
                  //   "expiration_date_token" => $res->GetUserResult->ExpirationDateToken,
                  //   "forgot_password_token" => $res->GetUserResult->ForgotPasswordToken,
                  //   "ip_criacao" => $res->GetUserResult->IPCriacao,
                  //   "login_result" => $res->GetUserResult->LoginResult,
                  //   "match_code_id" => $res->GetUserResult->MatchCodeId,
                  //   "nome" => $res->GetUserResult->Nome,
                  //   "nome_responsavel" => $res->GetUserResult->NomeResponsavel,
                  //   "quantidade_de_falhas_login" => $res->GetUserResult->QuantidadeDeFalhasLogin,
                  //   "questao_seguranca" => $res->GetUserResult->QuestaoSeguranca,
                  //   "resposta_questao_seguranca" => $res->GetUserResult->RespostaQuestaoSeguranca,
                  //   "senha" => $res->GetUserResult->Senha,
                  //   "tipo_cadastro" => $res->GetUserResult->Tipo_Cadastro,
                  //   "user_social_id" => $res->GetUserResult->UserSocialId

                  // ];

                  // $_SESSION["get_user"] = $attr_user;

                  updateUserSession();

                  $redirect_path = "/";
                  $url = url::fromUserInput($redirect_path);
                  $form_state->setRedirectUrl($url);
 
              } catch (SoapFault $exception) {
                  $response->addCommand(
                    new HtmlCommand(
                      '.result_message',
                      '<div class="result_message">deu erro em alguma coisa</div>'),
                  );
              }
          }
      }            
  }


  return $response;
   }





   
  // public function submitForm(array &$form, FormStateInterface $form_state) {
  //   return "Ok";
  // }
}

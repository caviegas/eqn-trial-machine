<?php

namespace Drupal\module_nestle_connect\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\user_cadu\Controller\UserCADUController;


/**
 * Our custom ajax form.
 */
class cadastrarUsuarioForm extends FormBase {
  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "form_cadastrar_usuario";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    /**
     * Campos do formulário
     */
    if (\Drupal::currentUser()->isAuthenticated()) {
      return new RedirectResponse('/');   
    }

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Seu e-mail *'),
    ];

    $form['nome'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seu nome completo *'),
    ];

    $form['document'] = [
      '#type' => 'textfield',
      '#id' => 'document',
      // '#default_value' => $cpf,
      // '#placeholder' => 'Digite seu CPF',
      '#title' => $this->t('Seu CPF'),
      '#mask' => [
        'value' => '999.999.999-99',
        'reverse' => FALSE,
        'selectonfocus' => TRUE,
        'clearifnotmatch' => FALSE,
      ],
    ];

    $form['telefone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Seu número de celular'),
      '#mask' => [
        'value' => '(99) 99999-9999',
        'reverse' => FALSE,
        'selectonfocus' => FALSE,
        'clearifnotmatch' => TRUE,
      ]
    ];

    $form['datanascimento'] = array(
      '#type' => 'date',
      '#title' => $this
      ->t('Sua data de nascimento *'),
    );

    $form['senha1'] = [
      '#type' => 'password',
      '#title' => $this->t('Digite uma senha *<span class="password_text"><strong>i</strong><p>Sua senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maíúscula, letra minúscula, números e caracteres especiais.</p> </span>'),
      // '#maxlength' => 10,
      // '#minlength' => 6,
    ];

    $form['senha2'] = [
      '#type' => 'password',
      '#title' => $this->t('Repita aqui a senha criada *'),
      // '#maxlength' => 10,
      // '#minlength' => 6,
    ];

    $form['politica'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Concordo que a Nestlé use meus dados de contato e interações para me mandar comunicações de marketing através do programa Com Você. Meu consentimento para este fim é voluntário e posso retirá-lo a qualquer momento. Mais informações disponíveis na <a class="link_black" href="/detalhes/politica-de-privacidade" target="_blank">Política de Privacidade de Eu Quero Nestlé</a>.'),
    ];

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
    ]; 

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#id' => 'btn_submit_create_user_form',
      '#value' => $this->t('CRIAR AGORA'),
      '#button_type' => 'primary',
    ];

     return $form;

   }

   public function validateForm(array &$form, FormStateInterface $form_state) {
    $count_errors = 0;
    $msg_erro = "";
    $msg_erroa = "";
    $msg_errob = "";

    if($form_state->getValues()['politica'] == 0){
      $form_state->setErrorByName('politica', $this->t('Você deve aceitar a política de privacidade.'));
    }


    if (!filter_var($form_state->getValues()['email'], FILTER_VALIDATE_EMAIL)) {
      $form_state->setErrorByName('email', 'Você deve informar um e-mail válido.');
    }


    if($form_state->getValues()['datanascimento'] == ''){
      $form_state->setErrorByName('datanascimento', 'Você precisa informar a sua data de nascimento.');
    }else if($form_state->getValues()['datanascimento'] !== ''){
      $date = DrupalDateTime::createFromFormat('Y-m-d', $form_state->getValues()['datanascimento'])->getTimeStamp();
      $now = \Drupal::time()->getCurrentTime();
      if($date > $now){
        $form_state->setErrorByName('datanascimento', 'Você não pode informar uma data de nascimento no futuro.');
      }
    }

    if($form_state->getValues()['datanascimento'] !== ""){
      $dataNasc = strtotime($form_state->getValues()['datanascimento']);
      $min = strtotime('+18 years', $dataNasc);
      if(time() < $min)  {
        $form_state->setErrorByName('datanascimento', 'Você precisa precisa ter mais de 18 anos para criar uma conta.');
       }
    }
     
    //  $form_state->setErrorByName('politica', $form_state->getValues()['datanascimento']);

    if($form_state->getValues()['senha1'] != $form_state->getValues()['senha2']){
      $form_state->setErrorByName('senha1', $this->t('As senhas que você informa precisam ser iguais.'));
    }else{
      $count_senha = 0;
      if (preg_match('/[\'^£$%&*()!}{@#~?><>,|=_+¬-]/', $form_state->getValues()['senha1']) )
        {
          $count_senha += 1;
        }
          
        if (preg_match("/[0-9]+/", $form_state->getValues()['senha1'])) {
          $count_senha += 1;
        } 

        if(preg_match('/[A-Z]/', $form_state->getValues()['senha1'])){
          $count_senha += 1;
        }

          if(preg_match('/[a-z]/',  $form_state->getValues()['senha1'])){
              $count_senha += 1;
          }

          if(strlen($form_state->getValues()['senha1']) <= 5|| strlen($form_state->getValues()['senha1']) > 10){
            $form_state->setErrorByName('senha1', $this->t('A senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maiúscula, letra minúscula, números e caracteres especiais.'));
          }

          if($count_senha < 3){
            $form_state->setErrorByName('senha1', $this->t('A senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maiúscula, letra minúscula, números e caracteres especiais.'));
          }
      }

      if($form_state->getValues()['nome'] == "" ){
        $form_state->setErrorByName('nome', $this->t('Você precisa informar o seu nome.'));
      }

      if($form_state->getValues()['email'] == ""){
        $form_state->setErrorByName('email', $this->t('Você deve informar um email válido.'));
      }

      if($count_errors > 0){
        \Drupal::messenger()->addError("Erro: ".$msg_erro);
        \Drupal::messenger()->addError("Erro: ".$msg_erroa);
        \Drupal::messenger()->addError("Erro: ".$msg_errob);
      }

      // $form_state->setErrorByName('email', 'erro pra não deixar submeter');
   }

   
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /**
     * Campos de criação de atributos do novo usuário
     */

    $email =  $form_state->getValues()['email'];
    $telefone = $form_state->getValues()['telefone'];
    $datanascimento = $form_state->getValues()['datanascimento'];

    /**
     * Iniciando variáveis
     */

    $get_attributes = "";
    $user_existe = 0;

    // }else{

        try {

          /**
           * Body do XML de criação de usuário.
           */

          $datanascimento = $form_state->getValues()['datanascimento'];

          $data = [
            'nome' => $form_state->getValues()['nome'],
            'email' => $form_state->getValues()['email'],
            'cpf' => $form_state->getValues()['document'],
            'dataNascimento' => $datanascimento,
            'dataCriacao' => date('Y-m-d'),
            'senha' => $form_state->getValues()['senha1'],
          ];

          $client = new SoapClient($GLOBALS['CADU_API_URL'], array('trace' => 1,));
          
          // SET HEADERS
          $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
          $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
          $client->__setSoapHeaders($header);
          
          $serviceStatus = $client->IsServiceAvailable();
          if ($serviceStatus != true) {
              $message = 'Serviço indisponível';
              return $message;
          }

          $res = $client->CreateUser($data);
          $createUserResult =  $res->CreateUserResult;

          /**
           * Testa se já existe usuario com mesmo email.
           */
          $form_state->disableRedirect();

          $emailJaCadastrado = false;
          $cpfJaCadastrado = false;
          $cpfInvalido = false;
          if(json_encode($createUserResult) != "{}"){
            if(is_array($createUserResult->ValidationError)){
              // quando a resposta ValidationError for um array
              foreach ($createUserResult->ValidationError as $validationError) {
                  if ($validationError->DescricaoErroCodigo == '04d86f4c') {
                      $cpfJaCadastrado = true;
                  } elseif ($validationError->DescricaoErroCodigo == '6cdd91fc') {
                      $emailJaCadastrado = true;
                  }else if($validationError->DescricaoErro == 'CPF Inválido. Digite novamente.'){
                      $cpfInvalido = true;
                  }
              }
            }else{
              // quando tiver só um objeto ValidationError
              if($createUserResult->ValidationError->DescricaoErroCodigo == '04d86f4c'){
                $cpfJaCadastrado = true;
              }else if($createUserResult->ValidationError->DescricaoErroCodigo == '6cdd91fc'){
                $emailJaCadastrado = true;
              }else if($createUserResult->ValidationError->DescricaoErro == 'CPF Inválido. Digite novamente.'){
                $cpfInvalido = true;
              }
            }

            if($emailJaCadastrado == true && $cpfJaCadastrado == false && $cpfInvalido == false){
              // quando o usuario tenta criar uma conta com um e-mail já existente na API 
              \Drupal::messenger()->addMessage("Seu cadastro foi concluído. Consulte a sua caixa de entrada de e-mail para novos passos.");
              
              $domainName = \Drupal::request()->getHost();
              $sendMail = $client->SendUserAlreadyExistsEmailWithDomain(['email' => $email,'domainName' => $domainName, 'username' => $email]);
            }

            if($cpfJaCadastrado == true && $cpfInvalido == false){
              \Drupal::messenger()->addMessage("Seu cadastro foi concluído. Consulte a sua caixa de entrada de e-mail para novos passos.");
              
              $domainName = \Drupal::request()->getHost();

              $user = $client->GetUser(['username' => $form_state->getValues()['document']],);
              $res_user_email = $user->GetUserResult->Email;
              $sendMail = $client->SendUserAlreadyExistsEmailWithDomain(['email' => $res_user_email,'domainName' => $domainName, 'username' => $res_user_email]);
            
            }
            if($createUserResult->ValidationError->NomeAtributo == "IdadeMinima"){
              \Drupal::messenger()->addError("O novo usuário deverá ser maior de 18 anos.");
            }
            if($cpfInvalido == true){
              \Drupal::messenger()->addError('CPF Inválido. Digite novamente.');
            }

          }else{
            
              try {

                $user_infos = $client->GetUser(['username' => $email],);
                $res_user_info = $user_infos->GetUserResult->Codigo;
                
                $data_atributos['atributos'] = [
                  
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'fg_politica_privacidade',
                    'Valor' => 'true',
                    'Items' => null
                  ],
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'id_receber_newsletter',
                    'Valor' => 'S',
                    'Items' => [
                        'Items' => [
                          "Id" => 11
                        ]
                    ]
                  ],
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'id_receber_newsletter_email',
                    'Valor' => 'S',
                    'Items' => [
                        'Items' => [
                          "Id" => 11
                        ]
                    ]
                  ],
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'id_receber_newsletter_sms',
                    'Valor' => 'S',
                    'Items' => [
                        'Items' => [
                          "Id" => 11
                        ]
                    ]
                  ],
                  [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'id_receber_newsletter_telefone',
                    'Valor' => 'S',
                    'Items' => [
                        'Items' => [
                          "Id" => 11
                        ]
                    ]
                  ]
                ];

                if($telefone !== ''){
                  $arrTelefone = [
                    'CodigoVisitante' => $res_user_info,
                    'NomeAtributo' => 'nu_celular',
                    'Valor' => $telefone,
                    'Items' => null
                  ];
                  array_push($data_atributos['atributos'], $arrTelefone);
                };

                $res_atributes = $client->SaveAttributes($data_atributos);
                
                $res = $client->GetUser(['username' => $email]);
    
              /**
               * Cria usuário no Drupal
               */
                
                // Pega o primeiro nome
                //Atribui um hexadecimal ao nome de usuário
                $nm_user_drupal = $form_state->getValue('nome');
                // $nm_array = explode(" ", $nm_user_drupal);
                $randon_user = bin2hex(openssl_random_pseudo_bytes(8));
                $nm_user_drupal = $nm_user_drupal."-".$randon_user;


                $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

                $user = user_load_by_mail($form_state->getValues()['email']);

                /** Set fields from CADU */
                $cadu_id = $res->GetUserResult->Codigo;
                $cadu_cpf = $res->GetUserResult->CPF;

                if($user){
                //  Caso o usuário já tenha cadastro no Drupal mas na API não, só atualiza o cadastro no drupal e cria na api
                  $user->setPassword($form_state->getValue('senha1'));
                  $user->set('status', 0);

                  /** Extra fields from CADU */
                  $user->set('field_cadu_id', $cadu_id);
                  $user->set('field_cpf', $cadu_cpf);

                  $user->save();
                }else{
                  // cria o usuario no drupal
                  $user = \Drupal\user\Entity\User::create();
                  $user->setPassword($form_state->getValue('senha1'));
                  $user->enforceIsNew();
                  $user->setEmail($email);
                  $user->setUsername($nm_user_drupal);//This username must be unique and accept only a-Z,0-9, - _ @ .
                  $user->set("init", 'email');
                  $user->set("langcode", $language);
                  $user->set("preferred_langcode", $language);
                  $user->set("preferred_admin_langcode", $language);

                  /** Extra fields from CADU */
                  $user->set('field_cadu_id', $cadu_id);
                  $user->set('field_cpf', $cadu_cpf);

                  // $user->activate();
                  $user->save();
                }

                // * Order - Checa se existe algum item na sessão para criar um Pedido congelado.
                $orderService = \Drupal::service('trialmachine_order.order');
                $orderService->createFrozenOrder($user->id());

                $message = '<script>var script = document.createElement("script"); script.innerHTML = "window.dataLayer = window.dataLayer || []; dataLayer.push({\'event\': \'Criação Cadastro\',\'visitorId\': \'' . $res->GetUserResult->Codigo . '\'})"; var head = document.getElementsByTagName("head")[0]; head.insertBefore(script, head.firstChild)</script>';
                $rendered_message = \Drupal\Core\Render\Markup::create($message);
                \Drupal::messenger()->addWarning($rendered_message);

                
                if(isset($_GET['destination'])){
                  $domainName = \Drupal::request()->getHost() . "/user/confirmacao?destination=" . $_GET['destination'] . "&";
                }else{
                  $domainName = \Drupal::request()->getHost() . "/user/confirmacao?";
                }

          

                $data = [
                  'email' => $email,
                  'domainName' => $domainName,
                ];
                $res = $client->SendConfirmationEmailWithDomain($data);
                $result = $res->SendConfirmationEmailWithDomainResult;

                \Drupal::messenger()->addMessage("Seu cadastro foi concluído. Consulte a sua caixa de entrada de e-mail para novos passos.");
                $form['email']['#value'] = '';
                $form['nome']['#value'] = '';
                $form['telefone']['#value'] = '';
                $form['datanascimento']['#value'] = '';
                $form_state->disableRedirect();
                
            } catch (SoapFault $exception) {
              $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
              \Drupal::messenger()->addError($response);
            }
          }

        } catch (SoapFault $exception) {
          // $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
          // \Drupal::messenger()->addError($response);
          // \Drupal::messenger()->addError($get_attributes);
          \Drupal::messenger()->addError('Login temporariamente indisponível');
        }

    // }  

    return "Ok";

  }
}
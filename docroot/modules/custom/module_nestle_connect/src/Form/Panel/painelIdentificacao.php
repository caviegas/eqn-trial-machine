<?php

namespace Drupal\module_nestle_connect\Form\Panel;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class painelIdentificacao extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "form_painel_identificacao";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    if (\Drupal::currentUser()->isAuthenticated()) {
      $x = "";
    } else {
      return new RedirectResponse('/user/entrar');   
      }

    updateUserSession();

    if(isset($_SESSION["get_attributes"]["nu_celular"])){
      $nu_celular = $_SESSION["get_attributes"]["nu_celular"];
    }else{
      $nu_celular = "";
    }

    $form['email'] = [
      '#type' => 'email',
      '#title' => 'Seu e-mail *',
      '#disabled' => TRUE,
      '#default_value' => $_SESSION["get_user"]['email']
    ];

    $form['document'] = [
      '#type' => 'textfield',
      '#id' => 'document',
      '#default_value' => $_SESSION["get_user"]['cpf'],
      // '#placeholder' => 'Digite seu CPF',
      '#disabled' => TRUE,
      '#title' => $this->t('Seu CPF'),
    ];

    $form['nome'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Seu nome completo *'),
      '#default_value' => $_SESSION["get_user"]['nome'],      
    ];

    $form['telefone'] = [
      '#type' => 'textfield',
      '#default_value' => $nu_celular,
      '#title' => $this->t('Seu número de celular'),
    ];

    $form['datanascimento'] = array(
      '#type' => 'date',
      '#title' => $this->t('Sua data de nascimento *'),
      '#disabled' => TRUE,
      '#default_value' => date("Y-m-d", strtotime($_SESSION["get_user"]['data_nascimento']))

    );

    $form['interlabel'] = [
      '#type' => 'markup',
      '#markup' => '<div class="interlabel">Preencha os campos abaixo se quiser alterar a sua senha</div>'
    ];

    $form['openpass'] = [
      '#type' => 'markup',
      '#markup' => '<div class="pass_fields">'
    ];

    $form['senha_atual'] = [
      '#type' => 'password',
      '#title' => $this->t('Senha atual')
    ];


    $form['senha1'] = [
      '#type' => 'password',
      '#title' => $this->t('Digite a senha nova')
    ];

    $form['senha2'] = [
      '#type' => 'password',
      '#title' => $this->t('Repita a senha nova'),
    ];

    $form['closepass'] = [
      '#type' => 'markup',
      '#markup' => '</div>'
    ];

    $form['close'] = [
      '#type' => 'markup',
      '#markup' => '</div>'
    ];

    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div class="result_message"></div>'
    ]; 

    $form['submit_group_a'] = [
      '#type' => 'markup',
      '#markup' => '<div class="ps-save">As informações alteradas em seu cadastro serão aplicadas em um próximo pedido. Caso você tenha pedidos em andamento, os dados alterados aqui não afetarão esses pedidos já abertos.</div>'
    ];

    $errors = $form_state->getErrors();

    if ($errors = $form_state->getErrors()) {
      $form['errors'] = [
        '#markup' => $errors['nome'],
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Salvar alterações'),
      '#button_type' => 'primary',
      // '#ajax' => [
      //   'callback' => '::myAjaxCallback', // don't forget :: when calling a class method.
      //   'effect' => 'fade',
      //   //'callback' => [$this, 'myAjaxCallback'], //alternative notation
      //   // 'disable-refocus' => FALSE, // Or TRUE to prevent re-focusing on the triggering element.
      //   // 'event' => 'change',
      //   // 'wrapper' => 'edit-output', // This element is updated with this AJAX callback.
      //   'progress' => [
      //     'type' => 'throbber',
      //     'message' => $this->t('Atualizando seus dados...'),
      //   ],
      // ]
    ];

     return $form;

   }


   public function validateForm(array &$form, FormStateInterface $form_state){

    if($form_state->getValues()['nome'] == ""){
      $form_state->setErrorByName('nome', 'Você precisa informar o seu nome.');
   }

    if($form_state->getValues()['datanascimento'] == ""){
        $form_state->setErrorByName('datanascimento', 'Você precisa informar a sua data de nascimento.');
     }

     if($form_state->getValues()['datanascimento'] !== ""){
      $dataNasc = strtotime($form_state->getValues()['datanascimento']);
      $min = strtotime('+18 years', $dataNasc);
      if(time() < $min)  {
        $form_state->setErrorByName('datanascimento', 'O usuário precisa ser maior de 18 anos');
       }
       $date = DrupalDateTime::createFromFormat('Y-m-d', $form_state->getValues()['datanascimento'])->getTimeStamp();
       $now = \Drupal::time()->getCurrentTime();
       if($date > $now){
         $form_state->setErrorByName('datanascimento', 'Você não pode informar uma data de nascimento no futuro.');
       }

     }

     if($form_state->getValues()['senha_atual'] !== "" && $form_state->getValues()['senha1'] !== "" && $form_state->getValues()['senha2'] !== ""){
      // entra aqui se todas as 3 senhas estiverem com algum valor
      if($form_state->getValues()['senha1'] !== $form_state->getValues()['senha2']){
        // se a senha 1 for diferente da 2
        $form_state->setErrorByName('senha1', 'A senha 1 e 2 precisam ser iguais');
        $form_state->setErrorByName('senha2', 'A senha 1 e 2 precisam ser iguais');
      }else{
        // valida a força das senhas
        $count_senha = 0;
        $count_senha_var = '';
        if (preg_match('/[\'^£$%&*()!}{@#~?><>,|=_+¬-]/', $form_state->getValues()['senha1']) )
          {
            $count_senha += 1;
          }
          
          // if (preg_match("/^\d+$/", $form_state->getValues()['senha1'])) {
          //   $count_senha += 1;
          // } 

          if (preg_match("/[0-9]+/", $form_state->getValues()['senha1'])) {
            $count_senha += 1;
          } 
          

          if(preg_match('/[A-Z]/', $form_state->getValues()['senha1'])){
              $count_senha += 1;
          }

          if(preg_match('/[a-z]/',  $form_state->getValues()['senha1'])){
              $count_senha += 1;
          }

          if(strlen($form_state->getValues()['senha1']) <= 5 || strlen($form_state->getValues()['senha1']) > 10){
            $form_state->setErrorByName('senha1', $this->t('A senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maiúscula, letra minúscula, números e caracteres especiais.'));
            $form_state->setErrorByName('senha2', $this->t('A senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maiúscula, letra minúscula, números e caracteres especiais.'));
          }

          if($count_senha < 3){
            $form_state->setErrorByName('senha1', $this->t('A senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maiúscula, letra minúscula, números e caracteres especiais.'));
            $form_state->setErrorByName('senha2', $this->t('A senha deve ter entre 6 e 10 caracteres contendo pelo menos três destes recursos: letra maiúscula, letra minúscula, números e caracteres especiais.'));
          }
      }
     }

     if($form_state->getValues()['telefone'] == ""){
        $form_state->setErrorByName('telefone', 'Favor informar o telefone');
     }
   }


   public function submitForm(array &$form, FormStateInterface $form_state){
    $response = new AjaxResponse();

    // $telefone = $form_state->getValues()['telefone'];
    // $datanascimento = $form_state->getValues()['datanascimento'];
    // $nome = $form_state->getValues()['nome'];

    // if($form_state->getValue('nome') == ""){
    //   $response->addCommand(new HtmlCommand('.nome-valid-message', 'Preencher o nome'));
    // }else{
    //   $response->addCommand(
    //     new HtmlCommand(
    //       '.result_message',
    //       '<div class="my_top_message">Usuario  atualizado</div>'),
    //   );
    // }
    try{
      $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
        'trace' => 1,
      ));
  
      // SET HEADERS
      $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
      $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
      $client->__setSoapHeaders($header);
    
      // Check if service is available
      $serviceStatus = $client->IsServiceAvailable();
      if ($serviceStatus != true) {
          $message = 'Serviço indisponível';
          return $message;
      }

      if ($serviceStatus != true) {
        $message = 'Serviço indisponível';
        return $message;
    }

      $codeuser = $_SESSION["get_user"]['codigo'];
      
      $data_atributos['atributos'] = [
          [
            'CodigoVisitante' => $codeuser,
            'NomeAtributo' => 'nu_celular',
            'Valor' => $form_state->getValues()['telefone'],
            'Items' => null
        ],
        [
          'CodigoVisitante' => $codeuser,
          'NomeAtributo' => 'fg_politica_privacidade',
          'Valor' => 'true',
          'Items' => null
        ],
        [
          'CodigoVisitante' => $codeuser,
          'NomeAtributo' => 'id_receber_newsletter',
          'Valor' => 'S',
          'Items' => [
              'Items' => [
                "Id" => 11
              ]
          ]
        ],
        [
          'CodigoVisitante' => $codeuser,
          'NomeAtributo' => 'id_receber_newsletter_email',
          'Valor' => 'S',
          'Items' => [
              'Items' => [
                "Id" => 11
              ]
          ]
        ],
        [
          'CodigoVisitante' => $codeuser,
          'NomeAtributo' => 'id_receber_newsletter_sms',
          'Valor' => 'S',
          'Items' => [
              'Items' => [
                "Id" => 11
              ]
          ]
        ],
        [
          'CodigoVisitante' => $codeuser,
          'NomeAtributo' => 'id_receber_newsletter_telefone',
          'Valor' => 'S',
          'Items' => [
              'Items' => [
                "Id" => 11
              ]
          ]
        ]
      ];

              
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;

      $res = $client->GetUser(['username' => $form_state->getValues()['email']]);




      $codeuser = $res->GetUserResult->Codigo;

      $data_atributos2['user'] = [
        'Codigo' => $codeuser,
        'Nome' => $form_state->getValues()['nome'],
        'dataNascimento' => $res->GetUserResult->DataNascimento,
        'Email' => $form_state->getValues()['email']
      ];

      $res2 = $client->UpdateUser($data_atributos2);


      // update drupal user
      $user = user_load_by_mail($form_state->getValues()['email']);
      $uid = $user->id();
      $user = \Drupal\user\Entity\User::load($uid);
      $user->setUsername($form_state->getValues()['nome']);



      if($form_state->getValues()['senha_atual'] !== '' && $form_state->getValues()['senha1'] !== '' && $form_state->getValues()['senha2'] !== ''){
        $data_atributos3 = [
          'username' => $form_state->getValues()['email'],
          'oldPassword' => $form_state->getValues()['senha_atual'],
          'newPassword' =>  $form_state->getValues()['senha1']
        ];
        $res3 = $client->ChangePassword($data_atributos3);
        $res_atributes3 = $res3->ChangePasswordResult;

        if($res_atributes3 == false){
          \Drupal::messenger()->addError('A senha atual informada não é válida.');
          return false;
        }else{
          $user->setPassword($form_state->getValues()['senha1']);
          \Drupal::messenger()->addMessage('Senha alterada com sucesso.');
        }
      }
      \Drupal::messenger()->addMessage('Dados atualizados.');
      // $user->save();
      // $redirect_path = "/user/detalhamento";
      // $url = url::fromUserInput($redirect_path);
      // $form_state->setRedirectUrl($url);
    }catch (SoapFault $exception) {
      \Drupal::messenger()->addError($response);
    }
    return $response;
   }

}

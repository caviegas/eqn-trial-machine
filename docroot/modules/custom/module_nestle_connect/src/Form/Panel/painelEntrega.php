<?php

namespace Drupal\module_nestle_connect\Form\Panel;
Use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\meeg_content\Controller\UserController;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class painelEntrega extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "form_painel_entrega";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {
    // creating UserController Instance
    $userController = new UserController;

    if (\Drupal::currentUser()->isAuthenticated()) {
      $x = "";
      updateUserSession();
    } else {
      return new RedirectResponse('/user/entrar?destination=/user/interesses');   
    }

    isset($_SESSION["get_attributes"]["id_sexo"]) ? $gn_user = $_SESSION["get_attributes"]["id_sexo"] : $gn_user = "";
    isset($_SESSION["get_attributes"]["nu_cep"]) ? $nu_cep = $_SESSION["get_attributes"]["nu_cep"] : $nu_cep = "";
    isset($_SESSION["get_attributes"]["dc_endereco"]) ? $dc_endereco = $_SESSION["get_attributes"]["dc_endereco"] : $dc_endereco = "";
    isset($_SESSION["get_attributes"]["sg_estado"]) ? $dc_estado = $_SESSION["get_attributes"]["sg_estado"] : $dc_estado = "";
    isset($_SESSION["get_attributes"]["dc_cidade"]) ? $dc_cidade = $_SESSION["get_attributes"]["dc_cidade"] : $dc_cidade = "";
    isset($_SESSION["get_attributes"]["dc_bairro"]) ? $dc_bairro = $_SESSION["get_attributes"]["dc_bairro"] : $dc_bairro = "";
    isset($_SESSION["get_attributes"]["dc_numero"]) ? $dc_numero = $_SESSION["get_attributes"]["dc_numero"] : $dc_numero = "";
    isset($_SESSION["get_attributes"]["dc_complemento"]) ? $dc_complemento = $_SESSION["get_attributes"]["dc_complemento"] : $dc_complemento = "";

    $form['open_cep'] = [
      '#type' => 'markup',
      '#markup' => '<div class="cep_infos">'
    ];

    $form['cep'] = [
      '#type' => 'textfield',
      '#id' => 'cep',
      '#default_value' => $nu_cep,
      '#title' => $this->t('Seu CEP'),
      '#mask' => [
        'value' => '99999-999',
        'reverse' => FALSE,
        'selectonfocus' => TRUE,
        'clearifnotmatch' => FALSE,
      ],
      '#suffix' => '<span class="dn-validate-message"></span>'
    ];


    $form['cepwarning'] = [
      '#type' => 'markup',
      '#markup' => "<div class='qual_cep'><p>Não sabe o seu CEP? <a target='_blank' href='https://www2.correios.com.br/sistemas/buscacep/buscaCep.cfm'>Descubra aqui</a> <span class='line'></span></p></div>"
    ];

    $form['close_cep'] = [
      '#type' => 'markup',
      '#markup' => '</div>'
    ];

    $form['estado'] = [
      '#type' => 'select',
      '#default_value' => $dc_estado,
      '#validated' => TRUE,
      '#title' => $this
        ->t('Estado'),
      
    ];

    $form['cidade'] = [
      '#type' => 'select',
      '#default_value' => $dc_cidade,
      '#validated' => TRUE,
      '#title' => $this
          ->t('Cidade'),
          '#options' => [
          '0' => $this->t('Selecionar'),
          '1' => $this->t('POA'),
      ],
    ];


    $form['bairro'] = [
      '#type' => 'textfield',
      '#id' => 'bairro',
      '#default_value' => $dc_bairro,
      '#title' => $this->t('Bairro'),
      // '#placeholder' => $this->t('digite aqui')
    ];

    $form['endereco'] = [
      '#type' => 'textfield',
      '#id' => 'endereco',
      '#default_value' => $dc_endereco,
      '#title' => $this->t('Rua'),
      
    ];

    $form['numero'] = [
      '#type' => 'textfield',
      '#default_value' => $dc_numero,
      '#title' => $this->t('Número'),
      // '#placeholder' => $this->t('digite aqui')
    ];

    $form['complemento'] = [
      '#type' => 'textfield',
      '#default_value' => $dc_complemento,
      '#title' => $this->t('Complemento'),
      // '#placeholder' => $this->t('digite aqui')
    ];

    $form['gridclose'] = [
      '#type' => 'markup',
      '#markup' => "</div>"
    ];

    $form['close_fieldset'] = [
      '#type' => 'markup',
      '#markup' => '</fieldset>'
    ];
    
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SALVAR E AVANÇAR'),
      '#button_type' => 'primary',
      '#suffix' => '<div class="result_message"></div>'
    ];

    return $form;
  }


     /**
   * {@inheritdoc}
   */
   public function validateForm(array &$form, FormStateInterface $form_state) {
 
    $get_attributes = $_SESSION['get_attributes'];
    // if(isset($get_attributes['nu_cep'])){
      if($form_state->getValues()['cep'] == ''){
        $form['cep']['#value'] = $get_attributes['nu_cep']; // caso o campo cep tenha erro, retorna para primeiro valor
        $form_state->setErrorByName('cep', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    // }

    if(isset($get_attributes['sg_estado'])){
      if($form_state->getValues()['estado'] == ''){
        $form['estado']['#value'] = $get_attributes['sg_estado'];
        $form_state->setErrorByName('estado', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    }

    if(isset($get_attributes['dc_cidade'])){
      if($form_state->getValues()['cidade'] == ''){
        $form['cidade']['#value'] = $get_attributes['dc_cidade'];
        $form_state->setErrorByName('cidade', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    }

    // if(isset($get_attributes['dc_bairro'])){
      if($form_state->getValues()['bairro'] == ''){ 
        $form['bairro']['#value'] = $get_attributes['dc_bairro'];
        $form_state->setErrorByName('bairro', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    // }

    // if(isset($get_attributes['dc_endereco'])){
      if($form_state->getValues()['endereco'] == ''){
        $form['endereco']['#value'] = $get_attributes['dc_endereco'];
        $form_state->setErrorByName('endereco', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    // }

    // if(isset($get_attributes['dc_numero'])){

      if($form_state->getValues()['numero'] == ''){
        dd('1');
        $form['numero']['#value'] = $get_attributes['dc_numero'];
        $form_state->setErrorByName('numero', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    // }

  }



   public function submitForm(array &$form, FormStateInterface $form_state){
    $response = new AjaxResponse();
    $count_errors = 0;
    $count_interesses = 0;

    // Colocar aqui o numero de atributos existentes no Form.
    $num_atributos = 12;

    $msg_erro = "";
    $msg_erroa = "";
    $get_attributes = "";

    try {

      $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
        'trace' => 1,
      ));
    
      // SET HEADERS
      $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
      $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
      $client->__setSoapHeaders($header);
      
      // Check if service is available
      $serviceStatus = $client->IsServiceAvailable();
      if ($serviceStatus != true) {
          $message = 'Serviço indisponível';
          return $message;
      }

      $codeuser = $_SESSION["get_user"]['codigo'];

      if ($form_state->getValues()['estado'] !== '') {
          $data_atributos['atributos'] = [
          [
            'CodigoVisitante' => $codeuser,
            'NomeAtributo' => 'sg_estado',
            'Valor' => $form_state->getValues()['estado'],
            'Items' => [
              'Items' => [
                  "Id" => 32
              ]
            ]
          ]
        ];
      }

      if($form_state->getValues()['cep'] !== ''){
        $data_atributos['atributos'] = [
          [
            'CodigoVisitante' => $codeuser,
            'NomeAtributo' => 'nu_cep',
            'Valor' => $form_state->getValues()['cep'],
            'Items' => null
          ]
        ];

      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }

      if($form_state->getValues()['endereco'] !== ''){
        $data_atributos['atributos'] = [
          [
            'CodigoVisitante' => $codeuser,
            'NomeAtributo' => 'dc_endereco',
            'Valor' => $form_state->getValues()['endereco'],
            'Items' => null
          ]
        ];

      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }

      if($form_state->getValues()['numero'] !== ''){
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_numero','Valor' => $form_state->getValues()['numero'],'Items' => null]];
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }

      if($form_state->getValues()['complemento'] !== ''){
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_complemento','Valor' => $form_state->getValues()['complemento'],'Items' => null]];
        $res = $client->SaveAttributes($data_atributos);
        $res_atributes = $res->SaveAttributesResult;
      }else{
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_complemento','Valor' => '','Items' => null]];
        $res = $client->SaveAttributes($data_atributos);
        $res_atributes = $res->SaveAttributesResult;
      }

      if($form_state->getValues()['bairro'] !== ''){
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_bairro','Valor' => $form_state->getValues()['bairro'],'Items' => null]];
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }else if($form_state->getValues()['bairro'] == ''){
 
      }

      if($form_state->getValues()['cidade'] !== ''){
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_cidade','Valor' => $form_state->getValues()['cidade'],'Items' => null]];
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }
      
      updateUserSession();
      // Criar uma entidade endereço

      $addressData = [
        'state' => $form_state->getValues()['estado'],
        'city' => $form_state->getValues()['cidade'],
        'cep' => $form_state->getValues()['cep'],
        'district' => $form_state->getValues()['bairro'],
        'street' => $form_state->getValues()['endereco'],
        'number' => $form_state->getValues()['numero'],
        'complement' => $form_state->getValues()['complemento'],
        'user_id' => \Drupal::currentUser()->id(),
      ];

      $address = \Drupal::entityTypeManager()->getStorage('trialmachine_address')
      ->create($addressData);
      $address->save();

      // Atribuir o endereço recém criado ao usuário:
      $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
      $user->set('field_address_id', $address->id());
      $user->save();

      \Drupal::messenger()->addMessage('Dados salvos.');




    } catch (SoapFault $exception) {
      $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
      \Drupal::messenger()->addError($response);
    }   

    return $response;
   }


}

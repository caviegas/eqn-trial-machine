<?php

namespace Drupal\module_nestle_connect\Form;
Use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class meuCadastroInteresses extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "form_meu_cadastro_interesses";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {

    if (\Drupal::currentUser()->isAuthenticated()) {
      $x = "";
      updateUserSession();
    } else {
      return new RedirectResponse('/user/entrar?destination=/user/interesses');   
      }




      $form['opening'] = [
        '#type' => 'markup',
        '#markup' => '<div><p class="opening">Diga aqui quais são os tipos de conteúdo que lhe interessam.</p></div>'
      ];

      $form['tabmenu'] = [
        '#type' => 'markup',
        '#markup' => getUserDetailsTabMenu()
      ];

      $form['guidemessage'] = [
        '#type' => 'markup',
        '#markup' => '<div class="guide_message"><span class="interests_icon"></span><p>A partir dos seus interesses, conseguimos criar um conteúdo cada vez mais personalizado!</p></div>'
      ];

      $form['gridopen'] = [
        '#type' => 'markup',
        '#markup' => '<div class="grid-options">'
      ];


      $termObjects = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => 'interesses']);
      
      $terms = [];
      $selectedTerms = array();
      $unSelectedTerms = array();

      $selectedTags = array();
      $unSelectedTags = array();
      foreach ($termObjects as $term) {
        $terms[$term->id()] = $term->label();
      }


      if(isset($_SESSION["get_attributes"]["nm_tags"]) && $_SESSION["get_attributes"]["nm_tags"] != 'null'){
        $nm_tags = json_decode($_SESSION["get_attributes"]["nm_tags"])->interesses;
        
        foreach($nm_tags AS $key => $value){
          array_push($selectedTags, $value->id);
          // unset($unSelectedTags[$value->id]);
        }

      }


      $form['selectedTags'] = array(
        '#type' => 'checkboxes',
        '#id' => 'selectedTags',
        '#title' => 'Seus interesses',
        '#options' => $terms,
        '#default_value' => $selectedTags
       );


      

      $form['gridclose'] = [
        '#type' => 'markup',
        '#markup' => "</div>"
      ];

    $form['submit_group_a'] = [
      '#type' => 'markup',
      '#markup' => '<div class="form-nav">
        <a href="detalhamento" class="form-nav-left">Voltar</a>
        <a href="/" class="form-nav-right">Pular</a>
      </div>'
    ];
    
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SALVAR E AVANÇAR'),
      '#button_type' => 'primary',
      '#suffix' => '<div class="result_message"></div>'

    ];

     return $form;

   }


   public function submitForm(array &$form, FormStateInterface $form_state){
    $response = new AjaxResponse();
    $count_errors = 0;
    $count_interesses = 0;

    // Colocar aqui o numero de atributos existentes no Form.
    $num_atributos = 12;

    $msg_erro = "";
    $msg_erroa = "";
    $get_attributes = "";

    try {

      $client = new SoapClient($GLOBALS['CADU_API_URL'], array(
        'trace' => 1,
      ));
    
      // SET HEADERS
      $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
      $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
      $client->__setSoapHeaders($header);
      
      // Check if service is available
      $serviceStatus = $client->IsServiceAvailable();
      if ($serviceStatus != true) {
          $message = 'Serviço indisponível';
          return $message;
      }

      $codeuser = $_SESSION["get_user"]['codigo'];
   
      $checkedInteresses = array_filter($form_state->getValues()['selectedTags'], function ($value) {
        return ($value !== 0);
      });
      $checkedInteresses2 = array();
      foreach($checkedInteresses AS $key => $value){
        $checkedInteresses2[] = $value;
      }
      
      $selectedInteresses = [];
      $termObjects = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => 'interesses']);

      foreach ($termObjects as $term) {
        // $newInteresse = new \stdClass();
        if(in_array($term->id(), $checkedInteresses2)){
          // echo $term->id();
          $newInteresse = new \stdClass();
          $newInteresse->id = intval($term->id());
          $newInteresse->label = $term->label();
          $selectedInteresses[] = $newInteresse;
        }
                
      }
      $nm_tags = [ 'interesses' => $selectedInteresses, 'consumidor' => $selectedInteresses];
      // $nm_tags = [ 'consumidor' => $selectedInteresses];

      // Se nenhum interesse tiver check, salva como 'null'
      if ($checkedInteresses == []) {
        $nm_tags = 'null';
      } else {
        $nm_tags = json_encode($nm_tags, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
      }
      
      $data_atributos['atributos'] = [
        [
          'CodigoVisitante' => $codeuser,
          'NomeAtributo' => 'nm_tags',
          'Valor' => $nm_tags,
          'Items' => null
        ]
      ];

    
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;

      updateUserSession();


      // redireciona pra /user/completo se o usuario estiver com todos os dados completos
      $userAttributes = $_SESSION['get_attributes'];
      if(isset($userAttributes['id_estado_civil']) && isset($userAttributes['sg_estado']) && isset($userAttributes['nu_cep']) && isset($userAttributes['nm_tags']) && !empty(json_decode($userAttributes['nm_tags'])->interesses) && $userAttributes['id_estado_civil'] !== '0'){
        $redirect_path = "/user/completo";
        $url = url::fromUserInput($redirect_path);
        $form_state->setRedirectUrl($url);
      }else{
        $redirect_path = "/";
        $url = url::fromUserInput($redirect_path);
        $form_state->setRedirectUrl($url);
        $response->addCommand(
          new HtmlCommand(
            '.result_message',
            '<div class="result_message">Salvo</div>'),
        );
      }




    } catch (SoapFault $exception) {
      $response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
      \Drupal::messenger()->addError($response);
    }   

    return $response;
   }


}

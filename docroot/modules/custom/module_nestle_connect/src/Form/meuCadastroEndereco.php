<?php

namespace Drupal\module_nestle_connect\Form;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
// use Drupal\Core\Ajax\RedirectResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\user_location\Controller\UserLocationController;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class meuCadastroEndereco extends FormBase {

  /**
   * {@inheritdoc}
   */
   public function getFormId() {
     return "form_meu_cadastro_endereco";
   }

   /**
    * {@inheritdoc}
    */

  public function buildForm(array $form, FormStateInterface $form_state) {
    /**
     * Meu campos
     */
    
    $x = "";
    // dd($_SESSION);

    if (\Drupal::currentUser()->isAuthenticated()) {
      $x = "";
      updateUserSession();
    } else {
      return new RedirectResponse('/user/entrar');   
      }

    isset($_SESSION["get_attributes"]["id_sexo"]) ? $gn_user = $_SESSION["get_attributes"]["id_sexo"] : $gn_user = "";
    isset($_SESSION["get_attributes"]["nu_cep"]) ? $nu_cep = $_SESSION["get_attributes"]["nu_cep"] : $nu_cep = "";
    isset($_SESSION["get_attributes"]["dc_endereco"]) ? $dc_endereco = $_SESSION["get_attributes"]["dc_endereco"] : $dc_endereco = "";
    isset($_SESSION["get_attributes"]["sg_estado"]) ? $dc_estado = $_SESSION["get_attributes"]["sg_estado"] : $dc_estado = "";
    isset($_SESSION["get_attributes"]["dc_cidade"]) ? $dc_cidade = $_SESSION["get_attributes"]["dc_cidade"] : $dc_cidade = "";
    isset($_SESSION["get_attributes"]["dc_bairro"]) ? $dc_bairro = $_SESSION["get_attributes"]["dc_bairro"] : $dc_bairro = "";
    isset($_SESSION["get_attributes"]["dc_numero"]) ? $dc_numero = $_SESSION["get_attributes"]["dc_numero"] : $dc_numero = "";
    isset($_SESSION["get_attributes"]["dc_complemento"]) ? $dc_complemento = $_SESSION["get_attributes"]["dc_complemento"] : $dc_complemento = "";
    
    
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => '<div><p class="opening">Fale um pouco mais sobre você para que a gente possa lhe entender melhor.</p></div>'
    ];

    $form['tabmenu'] = [
      '#type' => 'markup',
      '#markup' => getUserDetailsTabMenu()
    ];

    $form['guidemessage'] = ['#type' => 'markup', '#markup' => '<div class="guide_message"><span class="detail_icon"></span><p>Com os dados demográficos, conseguimos entender de que parte do Brasil você é. Isso nos ajuda a criar novos conteúdos pensados para você!</p></div><div>'];
    
    $form['cpf'] = [
      '#type' => 'textfield',
      '#id' => 'cpf',
      '#default_value' => $_SESSION["get_user"]["cpf"],
      '#title' => $this->t('Seu CPF'),
      '#mask' => [
        'value' => '999.999.999-99',
        'reverse' => FALSE,
        'selectonfocus' => TRUE,
        'clearifnotmatch' => FALSE,
      ],
      '#suffix' => '<span class="dn-validate-message"></span>'
    ];



    $form['genero_options'] = array(
      '#type' => 'value',
      '#default_value' =>  $gn_user,
      '#value' => array('' => t('Selecionar'),
                        'F' => t('Feminino'),
                        'M' => t('Masculino'),
                        '0' => t('Outro')
                      
                      ));



    $form['genero'] = [
        '#type' => 'select',
        '#default_value' => $gn_user,
        '#validated' => TRUE,
        '#title' => $this->t('Seu gênero'),
          '#options' => [
            '' => $this->t('Selecionar'),
            'F' => $this->t('Feminino'),
            'M' => $this->t('Masculino'),
            '0' => $this->t('Não declarado'),
          ],
      ];

      $form['open_cep'] = [
        '#type' => 'markup',
        '#markup' => '<div class="cep_infos">'
      ];

      $form['cep'] = [
        '#type' => 'textfield',
        '#id' => 'cep',
        '#default_value' => $nu_cep,
        '#title' => $this->t('Seu CEP'),
        '#mask' => [
          'value' => '99999-999',
          'reverse' => FALSE,
          'selectonfocus' => TRUE,
          'clearifnotmatch' => FALSE,
        ],
        '#suffix' => '<span class="dn-validate-message"></span>'
      ];
    
      $form['cepwarning'] = [
        '#type' => 'markup',
        '#markup' => "<div class='qual_cep'><p>Não sabe o seu CEP? <a target='_blank' href='http://www.buscacep.correios.com.br/sistemas/buscacep/default.cfm'>Descubra aqui</a> <span class='line'></span></p></div>"
      ];

      $form['close_cep'] = [
        '#type' => 'markup',
        '#markup' => '</div>'
      ];

      $form['estado'] = [
        '#type' => 'select',
        '#default_value' => $dc_estado,
        '#validated' => TRUE,
        '#title' => $this
          ->t('Estado'),
        
      ];

      $form['cidade'] = [
        '#type' => 'select',
        '#default_value' => $dc_cidade,
        '#validated' => TRUE,
        '#title' => $this
            ->t('Cidade'),
            '#options' => [
            '0' => $this->t('Selecionar'),
            '1' => $this->t('POA'),
            
        ],
      ];


      $form['bairro'] = [
        '#type' => 'textfield',
        '#id' => 'bairro',
        '#default_value' => $dc_bairro,
        '#title' => $this->t('Bairro'),
        // '#placeholder' => $this->t('digite aqui')
      ];

      $form['endereco'] = [
        '#type' => 'textfield',
        '#id' => 'endereco',
        '#default_value' => $dc_endereco,
        '#title' => $this->t('Rua'),
      ];

      $form['numero'] = [
        '#type' => 'textfield',
        '#default_value' => $dc_numero,
        '#title' => $this->t('Número'),
        // '#placeholder' => $this->t('digite aqui')
      ];


      $form['complemento'] = [
        '#type' => 'textfield',
        '#default_value' => $dc_complemento,
        '#title' => $this->t('Complemento'),
        // '#placeholder' => $this->t('digite aqui')
      ];

    $form['close'] = [
      '#type' => 'markup',
      '#markup' => '</div>'
    ];

    $form['submit_group_a'] = [
      '#type' => 'markup',
      '#markup' => '<div class="form-nav">
        <a href="basico" class="form-nav-left">Voltar</a>
        <a href="interesses" class="form-nav-right">Pular</a>
      </div>'
    ];
    
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SALVAR E AVANÇAR'),
      '#button_type' => 'primary',
      // '#ajax' => [
      //   'callback' => '::submitFormAjax', 
      //   'effect' => 'fade',
      //   "wrapper" => "test-ajax",
      //   'progress' => [
      //     'type' => 'throbber',
      //     'message' => $this->t('Atualizando seus dados...'),
      //   ],
      // ]
    ];

     return $form;

   }


  public function validateForm(array &$form, FormStateInterface $form_state) {

    $get_attributes = $_SESSION['get_attributes'];
    if(isset($get_attributes['nu_cep'])){
      if($form_state->getValues()['cep'] == ''){
        $form['cep']['#value'] = $get_attributes['nu_cep']; // caso o campo cep tenha erro, retorna para primeiro valor
        $form_state->setErrorByName('cep', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    }

    if(isset($get_attributes['sg_estado'])){
      if($form_state->getValues()['estado'] == ''){
        $form['estado']['#value'] = $get_attributes['sg_estado'];
        $form_state->setErrorByName('estado', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    }

    if(isset($get_attributes['dc_cidade'])){
      if($form_state->getValues()['cidade'] == ''){
        $form['cidade']['#value'] = $get_attributes['dc_cidade'];
        $form_state->setErrorByName('cidade', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    }

    if(isset($get_attributes['dc_bairro'])){
      if($form_state->getValues()['bairro'] == ''){ 
        $form['bairro']['#value'] = $get_attributes['dc_bairro'];
        $form_state->setErrorByName('bairro', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    }

    if(isset($get_attributes['dc_endereco'])){
      if($form_state->getValues()['endereco'] == ''){
        $form['endereco']['#value'] = $get_attributes['dc_endereco'];
        $form_state->setErrorByName('endereco', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    }

    if(isset($get_attributes['dc_numero'])){
      if($form_state->getValues()['numero'] == ''){
        $form['numero']['#value'] = $get_attributes['dc_numero'];
        $form_state->setErrorByName('numero', 'Você pode editar, mas não pode apagar as suas informações de endereço.');
      }
    }

  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $response = new AjaxResponse();

    $userLocationController = new UserLocationController();
    
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $uuid = $user->uuid();
    $data = [
      'uuid' => $uuid,
      'city' => $form_state->getValues()['cidade'],
      'state' => $form_state->getValues()['estado']
    ];
    $userLocationController->setUserLocation($data);

    try {

      $client = new SoapClient($GLOBALS['CADU_API_URL'], array('trace' => 1,));
      $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
      $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
      $client->__setSoapHeaders($header);
      $serviceStatus = $client->IsServiceAvailable();
      if ($serviceStatus != true) {
          $message = 'Serviço indisponível';
          return $message;
      }
      $codeuser = $_SESSION["get_user"]['codigo'];
  

      // $data_atributos['atributos'] = [
      //     // [
      //     //       'CodigoVisitante' => $codeuser,
      //     //       'NomeAtributo' => 'nu_cep',
      //     //       'Valor' => $form_state->getValues()['cep'],
      //     //       'Items' => null
      //     //   ],
      //       // [
      //       //     'CodigoVisitante' => $codeuser,
      //       //     'NomeAtributo' => 'dc_endereco',
      //       //     'Valor' => $form_state->getValues()['endereco'],
      //       //     'Items' => null
      //       // ],
      //       // [
      //       //     'CodigoVisitante' => $codeuser,
      //       //     'NomeAtributo' => 'dc_numero',
      //       //     'Valor' => $form_state->getValues()['numero'],
      //       //     'Items' => null
      //       // ],
      //       // [
      //       //     'CodigoVisitante' => $codeuser,
      //       //     'NomeAtributo' => 'dc_complemento',
      //       //     'Valor' => $form_state->getValues()['complemento'],
      //       //     'Items' => null
      //       // ],
      //       // [
      //       //     'CodigoVisitante' => $codeuser,
      //       //     'NomeAtributo' => 'dc_bairro',
      //       //     'Valor' => $form_state->getValues()['bairro'],
      //       //     'Items' => null
      //       // ],
      //       // [
      //       //     'CodigoVisitante' => $codeuser,
      //       //     'NomeAtributo' => 'dc_cidade',
      //       //     'Valor' => $form_state->getValues()['cidade'],
      //       //     'Items' => null
      //       // ],
      //       [
      //           'CodigoVisitante' => $codeuser,
      //           'NomeAtributo' => 'sg_estado',
      //           'Valor' => $form_state->getValues()['estado'],
      //           'Items' => [
      //             'Items' => [
      //                 "Id" => 32
      //             ]
      //           ]
      //       ]
      // ];

      // dd($data_atributos);
      // $res = $client->SaveAttributes($data_atributos);
      // $res_atributes = $res->SaveAttributesResult;



      if($form_state->getValues()['genero'] != ''){
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'id_sexo','Valor' => $form_state->getValues()['genero'],'Items' => ['Items' => ["Id" => 5]]]];
        $res = $client->SaveAttributes($data_atributos);
        $res_atributes = $res->SaveAttributesResult;
      }else if($form_state->getValues()['genero'] == ''){
      $data_atributos['atributos'] = [
        ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'id_sexo','Valor' => '','Items' => ['Items' => ["Id" => 5]]]];
        $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }

        if($form_state->getValues()['estado'] !== ''){
          $data_atributos['atributos'] = [
            [
              'CodigoVisitante' => $codeuser,
              'NomeAtributo' => 'sg_estado',
              'Valor' => $form_state->getValues()['estado'],
              'Items' => [
                'Items' => [
                    "Id" => 32
                ]
              ]
            ]
          ];

      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }


      if($form_state->getValues()['cep'] !== ''){
        $data_atributos['atributos'] = [
          [
            'CodigoVisitante' => $codeuser,
            'NomeAtributo' => 'nu_cep',
            'Valor' => $form_state->getValues()['cep'],
            'Items' => null
          ]
        ];

      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }


      if($form_state->getValues()['endereco'] !== ''){
        $data_atributos['atributos'] = [
          [
            'CodigoVisitante' => $codeuser,
            'NomeAtributo' => 'dc_endereco',
            'Valor' => $form_state->getValues()['endereco'],
            'Items' => null
          ]
        ];

      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }


      if($form_state->getValues()['numero'] !== ''){
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_numero','Valor' => $form_state->getValues()['numero'],'Items' => null]];
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }

      if($form_state->getValues()['complemento'] !== ''){
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_complemento','Valor' => $form_state->getValues()['complemento'],'Items' => null]];
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }

      if($form_state->getValues()['bairro'] !== ''){
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_bairro','Valor' => $form_state->getValues()['bairro'],'Items' => null]];
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }else if($form_state->getValues()['bairro'] == ''){
        // $data_atributos['atributos'] = [
        //   ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_bairro','Valor' => '','Items' => null]];
        // $res = $client->SaveAttributes($data_atributos);
        // if($res->SaveAttributesResult->ValidationError){
        //   echo $res->SaveAttributesResult->ValidationError->DescricaoErro;
        //   // var_dump($res);
        //   die();
        // }
      // $res_atributes = $res->SaveAttributesResult;
 
      }

      if($form_state->getValues()['cidade'] !== ''){
        $data_atributos['atributos'] = [
          ['CodigoVisitante' => $codeuser,'NomeAtributo' => 'dc_cidade','Valor' => $form_state->getValues()['cidade'],'Items' => null]];
      $res = $client->SaveAttributes($data_atributos);
      $res_atributes = $res->SaveAttributesResult;
      }

      if($form_state->getValues()['cpf'] !== ''){
        $data_atributos['atributos'] = [
          ['Codigo' => $codeuser,'NomeAtributo' => 'CPF','Valor' => $form_state->getValues()['cpf']]];
      $res = $client->UpdateUser($data_atributos);
      // $res_atributes = $res->SaveAttributesResult;
      }


      /**
       * Cria seesion com atributos do usuário
       */

      // $res_attr = $client->GetAttributes(['userName' => $_SESSION["get_user"]['email']]);
      // $attributes = $res_attr->GetAttributesResult->Atributo;

      // $attr = [];
      
      // foreach ($attributes as $value) {
      //   if($value->ValorMultivalorado == null && $value->Valor !== null){
      //     $attr = array_merge($attr, [
      //       $value->NomeAtributo => $value->Valor   
      //     ]);
      //   }else{
      //     $attr = array_merge($attr, [
      //       $value->NomeAtributo => $value->ValorMultivalorado   
      //   ]);
      //   }
      // }
      // $_SESSION["get_attributes"] = $attr;

      /**
      * Cria seesion do usuário
      */
      // $res = $client->GetUser(['username' => $_SESSION["get_user"]['email']]);

      // $attr_user = [
      // "ativo" => $res->GetUserResult->Ativo,
      // "cpf" => $res->GetUserResult->CPF,
      // "cpf_responsavel" => $res->GetUserResult->CPFResponsavel,
      // "codigo" => $res->GetUserResult->Codigo,
      // "codigo_area_site_criacao" => $res->GetUserResult->CodigoAreaSiteCriacao,
      // "confirmacao_email" => $res->GetUserResult->ConfirmacaoEmail,
      // "data_acesso_bloquado" => $res->GetUserResult->DataAcessoBloqueado,
      // "data_alteracao" => $res->GetUserResult->DataAlteracao,
      // "data_criacao" => $res->GetUserResult->DataCriacao,
      // "data_nascimento" => $res->GetUserResult->DataNascimento,
      // "data_ultimo_login" => $res->GetUserResult->DataUltimoLogin,
      // "email" => $res->GetUserResult->Email,
      // "email_responsavel" => $res->GetUserResult->EmailResponsavel,
      // "expiration_date_token" => $res->GetUserResult->ExpirationDateToken,
      // "forgot_password_token" => $res->GetUserResult->ForgotPasswordToken,
      // "ip_criacao" => $res->GetUserResult->IPCriacao,
      // "login_result" => $res->GetUserResult->LoginResult,
      // "match_code_id" => $res->GetUserResult->MatchCodeId,
      // "nome" => $res->GetUserResult->Nome,
      // "nome_responsavel" => $res->GetUserResult->NomeResponsavel,
      // "quantidade_de_falhas_login" => $res->GetUserResult->QuantidadeDeFalhasLogin,
      // "questao_seguranca" => $res->GetUserResult->QuestaoSeguranca,
      // "resposta_questao_seguranca" => $res->GetUserResult->RespostaQuestaoSeguranca,
      // "senha" => $res->GetUserResult->Senha,
      // "tipo_cadastro" => $res->GetUserResult->Tipo_Cadastro,
      // "user_social_id" => $res->GetUserResult->UserSocialId

      // ];

      // $_SESSION["get_user"] = $attr_user;

      // \Drupal::messenger()->addMessage("Salvar atributo:".json_encode($res_atributes));
      
      updateUserSession();
      $redirect_path = "/user/interesses";
      $url = url::fromUserInput($redirect_path);
      $form_state->setRedirectUrl($url);
      // $command = new RedirectCommand('/user/interesses');
      // $response->addCommand($command);

    } catch (SoapFault $exception) {

      $response->addCommand(
        new HtmlCommand(
          '.result_message',
          '<div class="result_message">deu erro em alguma coisa</div>'),
      );
    }
  return $response;

  }
   
  
}

(function ($, Drupal, drupalSettings) {

var checkPolitica = $('#edit-politica')
var buttonSubmit = $('#btn_submit_create_user_form')

if(checkPolitica.prop('checked')){
  buttonSubmit.prop("disabled",false);
  buttonSubmit.removeClass("disabled")
}else{
  buttonSubmit.prop("disabled",true);
  buttonSubmit.addClass("disabled")
}


checkPolitica.on('click', function(){
  if(checkPolitica.prop('checked')){
    buttonSubmit.prop("disabled",false);
    buttonSubmit.removeClass("disabled")
  }else{
    buttonSubmit.prop("disabled",true);
    buttonSubmit.addClass("disabled")
  }
})

})(jQuery, Drupal, drupalSettings);
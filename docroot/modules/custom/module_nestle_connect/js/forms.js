(function ($, Drupal, drupalSettings) {
    console.log
    getAttribute = drupalSettings.hero.php_session.get_attributes
    console.log(drupalSettings.hero.php_session)
      
    $('#edit-genero option[value=""]').attr('disabled', true)

    $( "#edit-telefone" ).mask('(00) 00000-0000');
    $('#edit-email').attr('autocomplete', 'off');
    $('#edit-senha1').attr('autocomplete', 'off');
    $( "#cep" ).mask('00000-000');
    $('#document').mask('000.000.000-00');

    // form endereço
    var cep = $('#cep')
    var estado = $('#edit-estado')


    estado.val(getAttribute.sg_estado)

    setaEstadoECidade(getAttribute.sg_estado, getAttribute.dc_cidade)


    $('#cep').on('change', function() {
        if(cep.val().length > 8){
            buscaEndereco(cep.val());
        }
    })

    function buscaEndereco(cep){
        toggleLoaderOnAddressFields(true)
        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
            if (!("erro" in dados)) {
                $("#endereco").val(dados.logradouro)
                $("#bairro").val(dados.bairro)
                setaEstadoECidade(dados.uf, dados.localidade)            
            } 
            else {
                alert("CEP não encontrado.");
                toggleLoaderOnAddressFields()  
            }
        });

        // var soapServiceURL = 'https://nestle2-internet.ef5.com.br/cadastro/WebServices/wsCadU.svc?wsdl';
        // var soapMessage = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:nes="http://schemas.datacontract.org/2004/07/Nestle.Web.CadastroUnico.DTO">' +
        // '<soap:Header>' +
        // '<PartnerCode xmlns="ns">16</PartnerCode>' +
        // '<CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>' +
        // '</soap:Header><soap:Body><tem:BuscarEnderecoPorCep>' +
        // '<tem:cep>' + cep + '</tem:cep></tem:BuscarEnderecoPorCep></soap:Body></soap:Envelope>'
    }

    $("#edit-estado").on('change', function() {         
        $.getJSON('/modules/custom/module_nestle_connect/js/estados_cidades.json', (data) => {
            let options_cidades = '<option selected disabled hidden>Selecione a cidade</option>';
            let str = $("#edit-estado").val();                  
            
            for (val of data) {
                if(val.sigla == str) {                          
                    for (val_city of val.cidades) {
                        if(getAttribute.dc_cidade == val_city){
                            options_cidades += '<option value="' + val_city + '" selected>' + val_city + '</option>';
                        }else{
                            options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                        }
                    }                           
                }
            }

            $("#edit-cidade").html(options_cidades);        
        })
    })      
    
    function setaEstadoECidade(estado, cidade){
        var jsonFile = '/modules/custom/module_nestle_connect/js/estados_cidades.json'
        $.getJSON(jsonFile, (data) => {
            let items = [];
            let options = 'Selecione a UF';
                
            for (val of data) {
                if(estado == val.sigla){
                    options += '<option value="' + val.sigla + '" selected>' + val.sigla + " - " + val.nome + '</option>';
                }else{
                    options += '<option value="' + val.sigla + '">' + val.sigla + " - " + val.nome + '</option>';
                }
                
            }           
            
            $("#edit-estado").html(options);        
        })


        $("#edit-estado").trigger('change')

        $.getJSON(jsonFile, (data) => {
            let options_cidades = '<option selected disabled hidden>Selecione a cidade</option>';
            let str = $("#edit-estado").val();                  

            for (val of data) {
                if(val.sigla == str) {                          
                    for (val_city of val.cidades) {
                        if(cidade == val_city){
                            options_cidades += '<option value="' + val_city + '" selected>' + val_city + '</option>';
                        }else{
                            options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                        }
                        
                    }                           
                }
            }

            $("#edit-cidade").html(options_cidades);        
        })
    }

    function toggleLoaderOnAddressFields(loader = false){


        if(loader){
            $("#endereco").val('');
            $("#bairro").val('');
            $("#edit-numero").val('');
            $("#edit-complemento").val('');
            $("#edit-estado").val('')
            $("#edit-cidade").val('')
            $("#endereco").attr("placeholder", "Buscando...");
            $("#bairro").attr("placeholder", "Buscando...");

        }else{

            $("#endereco").removeAttr('placeholder');
            $("#bairro").removeAttr('placeholder');
        }

    }

})(jQuery, Drupal, drupalSettings);

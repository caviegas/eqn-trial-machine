<?php

namespace Drupal\modulo_eqn_questoes\Form;


use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DrupalDateTime;

use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Symfony\Component\HttpFoundation\RedirectResponse;



// namespace Drupal\Core\Database\Query;

use Drupal\Core\Database\Connection;



/**
 * Our custom ajax form.
 */
class formAdminQuestions extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return "form_eqn_admin_questions";
    }

    /**
     * {@inheritdoc} 
     */

    public function buildForm(array $form, FormStateInterface $form_state) {

        
        $database = \Drupal::database();
        $questions = $database->query("SELECT * from trial_machine_questions");
            foreach ($questions as $question) {


                $form['question_' . $question->id . '_question'] = array(
                    '#type' => 'textfield',
                    '#default_value' => $question->question,

                    );

                    $form['question_' . $question->id . '_type'] = array(
                    '#type' => 'radios',
                    '#default_value' => $question->field_type,
                    '#options' => array('radio' => 'radio de 1 a 5', 'text' => 'text', 'bool' => 'Sim e não'
                    
                    ));

                    $form['interlabel_' . $question->id] = [
                        '#type' => 'markup',
                        '#markup' => '<hr><br>'
                      ];
                
            };


            $form['actions']['#type'] = 'actions';
            $form['actions']['submit'] = [
              '#type' => 'submit',
              '#value' => $this->t('ATUALIZAR'),
              '#button_type' => 'primary',
            ];


 
         return $form;


    }

    public function submitForm(array &$form, FormStateInterface $form_state){


        // $answers = [];
        // foreach($form_state->getValues()['question'] AS $key => $value){
        //     $answers[$key] = $value;
        //     echo $key;
        // }



        // $answers[1]['name'] = $form_state->getValues()['question_1_name'];
        // $answers[1]['type'] = $form_state->getValues()['question_1_type'];
        // $answers[2] = $form_state->getValues()['question_2'];
        // $answers[3] = $form_state->getValues()['question_3'];
        // $answers[4] = $form_state->getValues()['question_4'];
        // $answers[5] = $form_state->getValues()['question_5'];

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 1,
            ':question' => $form_state->getValues()['question_1_question'],
            ':field_type' => $form_state->getValues()['question_1_type']
        ]);

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 2,
            ':question' => $form_state->getValues()['question_2_question'],
            ':field_type' => $form_state->getValues()['question_2_type']
        ]);

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 3,
            ':question' => $form_state->getValues()['question_3_question'],
            ':field_type' => $form_state->getValues()['question_3_type']
        ]);

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 4,
            ':question' => $form_state->getValues()['question_4_question'],
            ':field_type' => $form_state->getValues()['question_4_type']
        ]);

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 5,
            ':question' => $form_state->getValues()['question_5_question'],
            ':field_type' => $form_state->getValues()['question_5_type']
        ]);

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 6,
            ':question' => $form_state->getValues()['question_6_question'],
            ':field_type' => $form_state->getValues()['question_6_type']
        ]);

        return true;

    }
}
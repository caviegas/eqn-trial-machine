<?php

namespace Drupal\modulo_eqn_questoes\Form;


use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DrupalDateTime;

use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\trialmachine_product\Entity\Product;
use Drupal\trialmachine_coupon\Entity\Coupon;
use Drupal\trialmachine_order\Entity\Order;


// namespace Drupal\Core\Database\Query;

use Drupal\Core\Database\Connection;



/**
 * Our custom ajax form.
 */
class formGetCoupon extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return "form_eqn_get_coupon";
    }

    /**
     * {@inheritdoc} 
     */

    public function buildForm(array $form, FormStateInterface $form_state) {
        // verifica se o user tá logado
        if (\Drupal::currentUser()->isAuthenticated()) {
            $x = "";
        } else {
            // return new RedirectResponse('/nestle_trial_machine?campaign=' . $_GET['campaign']);   
        }


        // valida se tem o parametro campaign na url
        if(!isset($_GET['order'])){
            die('CHEGOU NA URL SEM O PARAMETRO ORDER');
        }

        // verifica se o usuario já tem cupom para essa campanha
        $order = Order::load($_GET['order']);
        $userHasCoupon = \Drupal::entityQuery('trialmachine_coupon')
            ->condition('campaign_id', $order->campaign_id->entity->id())
            ->condition('user_id', \Drupal::currentUser()->id())
            ->execute();
        if($userHasCoupon){
            return new RedirectResponse('/');   
            // die('Usuário já tem cupom para essa campanha. Aqui tem que fazer alguma coisa');
        }

        $couponId = \Drupal::entityQuery('trialmachine_coupon')
        ->condition('campaign_id', $order->campaign_id->entity->id())
        ->notExists('user_id')
        ->range(0,1)
        ->execute();
        // TODO ver como buscar um id só. Agora tá trazendo um array mesmo com o range, por isso o foreach aqui embaixo

        $coupons = Coupon::loadMultiple($couponId);
        foreach($coupons AS $coupon){
            $form['couponCode'] = [
                '#type' => 'markup',
                '#markup' => '<h1>Esse é o teu cupom: ' . $coupon->code->value . '</h1>' . 
                '<h2>Total do desconto: ' . $coupon->discount->value . '%</h2>'
            ];
            $coupon->user_id = \Drupal::currentUser()->id(); // invalida o cupom
            $coupon->save();
        }
      

 
         return $form;


    }

    public function submitForm(array &$form, FormStateInterface $form_state){



    }
}
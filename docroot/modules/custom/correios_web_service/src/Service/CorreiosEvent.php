<?php

/**
 * @file providing the service that matches the event from Correios Brazil to Trial Machine
 * Contains \Drupal\correios_web_service\Service\CorreiosEvent
 */

namespace Drupal\correios_web_service\Service;

//! Esta classe será alterada depois de criada o OrderService (nome provisório).
class CorreiosEvent
{
    /**
     * Constructs a CorreiosEvent object.
     */
    public function __construct()
    {
        $this->orderService = \Drupal::service('trialmachine_order.order');
    }

    /**
     * Format the event code and message from Correios Web Service for the Trial Machine Orders.
     */
    public function formatEvent($event)
    {
        if (!is_object($event)) {
            throw new \Exception("Objeto inválido.");
        }

        if (empty($event->codigo) || empty($event->tipo)) {
            throw new \Exception("Código errado do evento.");
        }

        $status = $this->matchEvent($event->codigo, $event->tipo);

        if ($status === false) {
            throw new \Exception("Evento não encontrado.");
        }

        return $status;
    }

    /**
     * Match the main code from Correios Web Service to discover its status.
     */
    public function matchEvent($code, $type)
    {
        if (strcasecmp($code, 'PO') === 0) {
            return $this->matchTypePO($type);
        }

        if (strcasecmp($code, 'BDE') === 0) {
            return $this->matchTypeBDE($type);
        }

        if (strcasecmp($code, 'BDI') === 0) {
            return $this->matchTypeBDI($type);
        }

        if (strcasecmp($code, 'BDR') === 0) {
            return $this->matchTypeBDR($type);
        }

        if (strcasecmp($code, 'BLQ') === 0) {
            return $this->matchTypeBLQ($type);
        }

        if (strcasecmp($code, 'DO') === 0) {
            return $this->matchTypeDO($type);
        }

        if (strcasecmp($code, 'CO') === 0) {
            return $this->matchTypeCO($type);
        }

        if (strcasecmp($code, 'RO') === 0) {
            return $this->matchTypeRO($type);
        }

        if (strcasecmp($code, 'PAR') === 0) {
            return $this->matchTypePAR($type);
        }

        if (strcasecmp($code, 'FC') === 0) {
            return $this->matchTypeFC($type);
        }

        if (strcasecmp($code, 'LDI') === 0) {
            return $this->matchTypeLDI($type);
        }

        if (strcasecmp($code, 'EST') === 0) {
            return $this->matchTypeEST($type);
        }

        if (strcasecmp($code, 'OEC') === 0) {
            return $this->matchTypeOEC($type);
        }

        return false;
    }

    /**
     * Match Correios type "PO".
     */
    private function matchTypePO($type)
    {
        /**
         * Pedidos que ainda não saíram para a entrega ou que estão prontos para envio.
         */
        if (preg_match('/\b(01|09)\b/', $type)) {
            return $this->orderService->getStatusByAlias('aguardando');
        }

        return false;
    }

    /**
     * Match Correios type "BDE".
     */
    private function matchTypeBDE($type)
    {
        /**
         * Pedidos que foram entregues ao destinatário.
         */
        if (preg_match('/\b(01|67|70)\b/', $type)) {
            return $this->orderService->getStatusByAlias('entregue');
        }

        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b(09|15|18|20|25|30|32|35|39|4[5-7]|53|54|5[7-9]|60|66|68|69|84|85|87)\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b(0[2-8]|10|1[2-4]|19|2[1-4]|26|28|29|33|34|3[6-8]|4[0-4]|48|49|5[0-2]|55|56|61|62|7[1-7]|79|8[0-3]|86|89|98)\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }

    /**
     * Match Correios type "BDI".
     */
    private function matchTypeBDI($type)
    {
        /**
         * Pedidos que foram entregues ao destinatário.
         */
        if (preg_match('/\b(01|67|70)\b/', $type)) {
            return $this->orderService->getStatusByAlias('entregue');
        }

        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b(09|15|18|19|20|22|25|30|32|35|39|4[5-7]|5[3-5]|5[7-9]|66|68|69|80|84|85|87)\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b(0[2-8]|10|1[2-4]|21|23|24|26|2[8-9]|3[3-4]|3[6-8]|4[0-4]|4[8-9]|5[0-2]|56|6[0-2]|7[1-7]|8[1-3]|86|89|98)\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }

    /**
     * Match Correios type "BDR".
     */
    private function matchTypeBDR($type)
    {
        /**
         * Pedidos que foram entregues ao destinatário.
         */
        if (preg_match('/\b(01|67|70)\b/', $type)) {
            return $this->orderService->getStatusByAlias('entregue');
        }

        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b(09|12|15|18|19|20|25|3[0-2]|35|41|4[5-7]|5[7-9]|60|63|66|68|69|84|85|87)\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b(0[2-8]|10|13|14|2[1-4]|26|28|29|33|34|3[6-9]|40|4[2-4]|48|49|5[0-6]|61|62|64|7[1-7]|79|8[0-3]|86|89)\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }

    /**
     * Match Correios type "BLQ".
     */
    private function matchTypeBLQ($type)
    {
        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b(24|44|54|61)\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b(01|04|05|11|21|22|30|31|41|42|51)\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }

    /**
     * Match Correios type "CO".
     */
    private function matchTypeCO($type)
    {
        /**
         * Pedidos que ainda não saíram para a entrega ou que estão prontos para envio.
         */
        if (preg_match('/\b12\b/', $type)) {
            return $this->orderService->getStatusByAlias('aguardando');
        }

        /**
         * Pedidos que foram entregues ao destinatário.
         */
        if (preg_match('/\b08\b/', $type)) {
            return $this->orderService->getStatusByAlias('entregue');
        }

        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b(0[1-7])\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b(09|10|11|13|14)\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }

    /**
     * Match Correios type "DO".
     */
    private function matchTypeDO($type)
    {
        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b(01\b|02)\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        return false;
    }

    /**
     * Match Correios type "RO".
     */
    private function matchTypeRO($type)
    {
        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b01\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        return false;
    }

    /**
     * Match Correios type "FC".
     */
    private function matchTypeFC($type)
    {
        /**
         * Pedidos que ainda não saíram para a entrega ou que estão prontos para envio.
         */
        if (preg_match('/\b0[1-4]\b/', $type)) {
            return $this->orderService->getStatusByAlias('aguardando');
        }

        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b(07|08|10)\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b(05|09|1[1-3]|19|24|26|27|29|3[1-5]|37|41|42|47|51|55)\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }

    /**
     * Match Correios type "PAR".
     */
    private function matchTypePAR($type)
    {
        /**
         * Pedidos que ainda não saíram para a entrega ou que estão prontos para envio.
         */
        if (preg_match('/\b(10|12|21|22|34|42)\b/', $type)) {
            return $this->orderService->getStatusByAlias('aguardando');
        }

        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b(15|16|18|19|20)\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b(11|13|14|17|2[3-9]|3[0-3]|40|41)\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }

    /**
     * Match Correios type "LDI".
     */
    private function matchTypeLDI($type)
    {
        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b(0[1-4]|11|13|14)\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }

    /**
     * Match Correios type "EST".
     */
    private function matchTypeEST($type)
    {
        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b(0[1-6]|09)\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }

    /**
     * Match Correios type "OEC".
     */
    private function matchTypeOEC($type)
    {
        /**
         * Pedidos ainda à caminho do destino e quaiquer outros estados que não forem de 'problema na entrega'.
         */
        if (preg_match('/\b01\b/', $type)) {
            return $this->orderService->getStatusByAlias('caminho');
        }

        /**
         * Pedidos que possuem algum problema e devem ser verificados com os Correios.
         */
        if (preg_match('/\b09\b/', $type)) {
            return $this->orderService->getStatusByAlias('problema');
        }

        return false;
    }
}
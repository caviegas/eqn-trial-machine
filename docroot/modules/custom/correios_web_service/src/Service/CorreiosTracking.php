<?php

/**
 * @file providing the service that tracks objects from Correios Brazil
 * Contains \Drupal\correios_web_service\Service\CorreiosTracking
 */

namespace Drupal\correios_web_service\Service;

class CorreiosTracking
{
    /**
     * Base URL using the Correios api SRO RASTRO from proxyapp.correios.com.br
     * 
     * @var string $base_url
     */
    private $base_url = 'https://proxyapp.correios.com.br/v1/sro-rastro/';

    /**
     * Create an error object to standardize the returns of this class.
     * 
     * @param string $message
     *  Mensagem de erro.
     * 
     * @return object $error
     *  The object created from error message.
     */
    private function buildError($message)
    {
        $error = new \stdClass;
        $error->error = $message;
        return $error;
    }

    /**
     * Make a GET request for the tracked object using cURL.
     * 
     * @param string $url
     *  URL from the Correios Web Service API.
     * 
     * @return object $response
     *  The object decoded from cURL response.
     */
    private function getConnection(String $url)
    {
        $curl = curl_init();

        if ($curl === false) {
            throw new \Exception('Falha em obter a conexão com os Correios.');
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        
        //! Descomente esta linha apenas para realizar testes em LOCALHOST.
        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);

        if ($response === false) {
            throw new \Exception(curl_error($curl), curl_errno($curl));
        }

        curl_close($curl);

        return json_decode($response);
    }

    /**
     * Get the last registered event for the tracked object.
     * 
     * @param object $object
     *  The Object result from the tracked object got from Correios Web Service API.
     * 
     * @return array
     *  The object eventos from Correios Web Service.
     */
    public function getLastEvent($object)
    {
        if (!is_object($object)) {
            throw new \Exception("Objeto inválido.");
        }

        if (is_array($object->eventos)) {
            return reset($object->eventos);
        }

        return $object->eventos;
    }

    /**
     * Get object from response.
     * 
     * @param object $response_object
     *  Result object from the API response from Correios Web Service.
     * 
     * @return object
     *  The object from response object.
     */
    private function getObjectFromResponse($response_object)
    {
        if (isset($response_object->objetos) && count($response_object->objetos) == 1) {
            return reset($response_object->objetos);
        }

        if (isset($response_object->msgs)) {
            $object = new \stdClass;
            $object->mensagem = $response_object->causa;
            return $object;
        }
    }

    /**
     * Search for the Orders ready to be tracked by the Correios Web Service.
     * 
     * @return array $orders
     *  The array of entities Order.
     */
    public function searchOrdersToTrack()
    {
        // @var \Drupal\trialmachine_order\Service\OrderService $orderService
        $orderService = \Drupal::service('trialmachine_order.order');

        // @var \Drupal\trialmachine_order\Entity\Order $order
        $order = \Drupal::entityTypeManager()->getStorage('trialmachine_order');

        /**
         * Get only the oldest 50 orders every iteration.
         */
        $tbl_order = $order->getBaseTable();
        $query = \Drupal::database()->select($tbl_order);
        $query->fields($tbl_order, ['id']);
        $query->condition("{$tbl_order}.status", $orderService->getStatusByAlias('entregue', 1), '<>');
        $query->condition("{$tbl_order}.status", $orderService->getStatusByAlias('avaliado', 1), '<>');
        $query->condition("{$tbl_order}.status", $orderService->getStatusByAlias('cancelado', 1), '<>');
        $query->condition("{$tbl_order}.tracking_code", '', '<>');
        $query->orderBy('changed', 'ASC');
        $query->range(0,50);

        $order_ids = $query->execute()->fetchAllKeyed(0,0);

        $orders = $order->loadMultiple($order_ids);
        
        return $orders;
    }

    /**
     * Check if an object exists in Correios database using the tracking code.
     * 
     * @param string $tracking_code
     * The tracking code created in Correios.
     * 
     * @return Object
     * Custom object with "ok" variable to set TRUE or FALSE.
     */
    public function checkObjectExists(string $tracking_code)
    {
        $object = $this->trackOneObject($tracking_code);

        $object->ok = true;

        if (!empty($object->error)) {
            $object->ok = false;
        }

        return $object;
    }

    /**
     * Main method for tracking objects from Correios Web Service and integrating with Trial Machine.
     * 
     * @param string $tracking_code
     *  The tracking code from Correios used to find the Object events.
     * 
     * @return array $response
     *  The array of data collected from Correios Web Service.
     */
    public function trackObjectFormatted(String $tracking_code)
    {
        $EventService = \Drupal::service('correios_web_service.event');

        $Object = $this->trackOneObject($tracking_code);

        if (!empty($Object->error)) {
            throw new \Exception($Object->error);
        }

        $Event = $this->getLastEvent($Object);

        $status = $EventService->formatEvent($Event);

        $response = [
            'status_id' => $status['id'],
            'description' => $status['description'],
            'tracking_code' => $Object->codObjeto,
            'delivery_date' => $Object->dtPrevista,
            'event_code' => $Event->codigo . $Event->tipo,
            'event_message' => $Event->descricao,
            'event_created_date' => $Event->dtHrCriado,
        ];

        return $response;
    }

    /**
     * Track only one object from Correios Web Service.
     * 
     * @param string $tracking_code
     *  The tracking code from Correios used to find the Object events.
     * 
     * @return object $object
     *  The object from cURL response.
     */
    public function trackOneObject(String $tracking_code)
    {
        $url = $this->base_url . $tracking_code;

        try {
            $response = $this->getConnection($url);

            $object = $this->getObjectFromResponse($response);

            if (isset($object->mensagem)) {
                throw new \Exception($object->mensagem);
            }
        }
        catch (\Throwable $e) {
            return $this->buildError($e->getMessage());
        }

        return $object;
    }
}
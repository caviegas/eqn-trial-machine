<?php
/**
 * @file
 * Contains \Drupal\correios_web_service\Controller\CorreiosWebService
 */

namespace Drupal\correios_web_service\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;

class CorreiosWebServiceController extends ControllerBase
{
    /**
     * Apenas usado para testes da Cron em browser.
     */
    public function correios()
    {
        //$order = \Drupal::entityTypeManager()->getStorage('commerce_order');
        //$tracking_code = \Drupal::entityTypeManager()->getStorage('commerce_order.field_tracking_code');

        $correiosTracking = \Drupal::service('correios_web_service.tracking');
        $orders = $correiosTracking->searchOrdersToTrack();

        //dd($order);
        //dd($order->getEntityTypeId());
        //dd($order->getBaseTable());

        foreach ($orders as $order) {
            dd(DrupalDateTime::createFromTimestamp($order->getChangedTime())->format('d/m/Y H:i:s'));
        }

        /*
        $orders = \Drupal::entityTypeManager()
            ->getStorage('commerce_order')
            ->loadByProperties([
                'field_order_status' => [
                    $eventService->getStatusByAlias('aguardando', 2),
                    $eventService->getStatusByAlias('caminho', 2),
                    $eventService->getStatusByAlias('problema', 2),
                ],
            ]);

        dd($orders);
        */

        /*

        $trackingService = \Drupal::service('correios_web_service.tracking');
        $eventService = \Drupal::service('correios_web_service.event');

        $error = null;

        try {
            $tbl_commerce_order = 'commerce_order';
            $tbl_tracking_code = "{$tbl_commerce_order}__field_tracking_code";
            $tbl_order_status = "{$tbl_commerce_order}__field_order_status";

            $query = \Drupal::database()->select($tbl_commerce_order);
            $query->fields($tbl_commerce_order, ['order_id']);
            $query->join($tbl_tracking_code, NULL, "{$tbl_tracking_code}.entity_id = {$tbl_commerce_order}.order_id");
            $query->join($tbl_order_status, NULL, "{$tbl_order_status}.entity_id = {$tbl_commerce_order}.order_id");
            $query->condition("{$tbl_commerce_order}.type", 'campaign');
            $query->condition("{$tbl_order_status}.field_order_status_value", $eventService->getStatusByAlias('entregue')['description'], '<>');
            $query->condition("{$tbl_order_status}.field_order_status_value", $eventService->getStatusByAlias('avaliado')['description'], '<>');
            $query->condition("{$tbl_order_status}.field_order_status_value", $eventService->getStatusByAlias('cancelado')['description'], '<>');
            $query->condition("{$tbl_tracking_code}.field_tracking_code_value", '', '<>');
            
            $order_ids = $query->execute()->fetchAllKeyed(0,0);
            
            $Orders = \Drupal::entityTypeManager()->getStorage('commerce_order')->loadMultiple($order_ids);
        }
        catch (\Throwable $e) {
            $error = $e->getMessage();
        }

        if (empty($error) && !empty($Orders)) {
            foreach ($Orders as $Order) {
                $tracking_code = $Order->get('field_tracking_code')->value;
                
                try {
                    $response = $trackingService->trackObjectFormatted($tracking_code);
    
                    $Order->set('state', $response['status_id']);
                    $Order->set('field_order_status', $response['description']);
                    $Order->set('field_tracking_event_code', $response['event_code']);
                    $Order->set('field_tracking_event_message', $response['event_message']);
                    $Order->set('field_tracking_delivery_date', $response['delivery_date']);
    
                    //$Order->save();

                    var_dump($tracking_code);
                    var_dump($Order->get('state')->value);
                    var_dump($Order->get('field_order_status')->value);
                    var_dump($Order->get('field_tracking_event_code')->value);
                    var_dump($Order->get('field_tracking_event_message')->value);
                    var_dump($Order->get('field_tracking_delivery_date')->value);
                }
                catch (\Throwable $e) {
                    var_dump($e->getMessage());
                }
            }

            $view = [
                '#markup' => 'teste',
            ];
        }

        if (!empty($error)) {
            $view = [
                '#markup' => $error,
            ];
        }

        return $view;
        */
    }

    public function ordersbulk()
    {

        // NOVO CÓDIGO APENAS PARA TESTES DA NOVA ENTIDADE ORDER

        $user_id = \Drupal::currentUser()->id();
        $campaign_id = 3;
        $address_id = NULL; //! Tem que criar a entidade para poder executar corretamente.

        $data = [
            'user_id' => $user_id,
            'campaign_id' => $campaign_id,
            'address_id' => $address_id,
        ];

        // @var \Drupal\trialmachine_order\Entity\Order
        $order = \Drupal::entityTypeManager()->getStorage('trialmachine_order')
            ->create($data);

        // Save Order.
        try {
            // @var \Drupal\trialmachine_campaign\Entity\Campaign
            $campaign = \Drupal::entityTypeManager()->getStorage('trialmachine_campaign')
                ->load($campaign_id);
            
            if (!$campaign->takeOneFromQuantity()) {
                throw new Exception("Ocorreu um erro ao tentar remover 1 produto da campanha (id {$campaign_id})");
            }

            $order->save();
        }
        catch (\Throwable $e) {
            \Drupal::logger('trialmachine_order')->error($e->getMessage());
        }

        return $view = [
            '#markup' => 'inseriu a order com sucesso',
        ];

        /*

        //dd(DrupalDateTime::createFromFormat('d/m/Y'));

        $CorreiosEvent = \Drupal::service('correios_web_service.event');

        $campaign_id = 2;

        $type = 'campaign';

        $store_id = ['1'];

        $state = $CorreiosEvent->getStatusByAlias('aguardando', 1);

        $placed = new DrupalDateTime(); $placed = $placed->getTimestamp();

        $order_status = $CorreiosEvent->getStatusByAlias('aguardando', 2);

        $tracking_event_code = null;

        $tracking_event_message = null;

        $tracking_event_delivery_date = null;

        $data = [
            'type' => $type,
            'store_id' => $store_id,
            'state' => $state,
            'placed' => $placed,
        ];

        $orderStorage = \Drupal::entityTypeManager()->getStorage('commerce_order');

        $userNicolauber = \Drupal::entityTypeManager()->getStorage('user')->load(14);
        $userNicolas = \Drupal::entityTypeManager()->getStorage('user')->load(21);
        $userJean = \Drupal::entityTypeManager()->getStorage('user')->load(18);

        $tracking_codes = [
            'QG125677815BR',
            'OP594445146BR',
            'QG011299055BR',
            'PQ306291582BR',
            'QG011299268BR',
            'QG293536854BR',
            //'OP726041987BR',
            //'QG279471525BR',
            //'OP726041987BR',
            'PQ306291582BR',
            'PQ306291582BR',
            'PQ306291582BR',
            'PQ306291582BR',
        ];

        for ($i = 1; $i <= 10; $i++) {
            $order = $orderStorage->create($data);
            $order->set('uid', $userNicolas);
            $order->set('mail', $userNicolas->getEmail());
            $order->set('field_campaign', $campaign_id);
            $order->set('field_order_status', $order_status);
            $order->set('field_tracking_event_code', $tracking_event_code);
            $order->set('field_tracking_event_message', $tracking_event_message);
            $order->set('field_tracking_delivery_date', $tracking_event_delivery_date);
            $order->set('field_tracking_code', $tracking_codes[($i-1)]);
            $order->save();
        }

        for ($i = 1; $i <= 10; $i++) {
            $order = $orderStorage->create($data);
            $order->set('uid', $userJean);
            $order->set('mail', $userJean->getEmail());
            $order->set('field_campaign', $campaign_id);
            $order->set('field_order_status', $order_status);
            $order->set('field_tracking_event_code', $tracking_event_code);
            $order->set('field_tracking_event_message', $tracking_event_message);
            $order->set('field_tracking_delivery_date', $tracking_event_delivery_date);
            $order->set('field_tracking_code', $tracking_codes[($i-1)]);
            $order->save();
        }

        for ($i = 1; $i <= 10; $i++) {
            $order = $orderStorage->create($data);
            $order->set('uid', $userNicolauber);
            $order->set('mail', $userNicolauber->getEmail());
            $order->set('field_campaign', $campaign_id);
            $order->set('field_order_status', $order_status);
            $order->set('field_tracking_event_code', $tracking_event_code);
            $order->set('field_tracking_event_message', $tracking_event_message);
            $order->set('field_tracking_delivery_date', $tracking_event_delivery_date);
            $order->set('field_tracking_code', $tracking_codes[($i-1)]);
            $order->save();
        }

        return $view = [
            '#markup' => 'inseriu 30 orders.',
        ];
        */
    }

    public function correiosmanual()
    {
        // @var \Drupal\correios_web_service\Service\CorreiosTracking $correiosTracking
        $correiosTracking = \Drupal::service('correios_web_service.tracking');

        // @var \Drupal\commerce_order\Entity\Order $orders
        $orders = $correiosTracking->searchOrdersToTrack();

        if (empty($orders)) {
            return;
        }

        // @var QueueFactory $queueFactory
        $queueFactory = \Drupal::service('queue');

        // @var QueueInterface $queue
        $queue = $queueFactory->get('correios_object_tracker');

        $queueManager = \Drupal::service('plugin.manager.queue_worker');

        $queueWorker = $this->queueManager->createInstance('manual_correios_object_tracker');

        foreach ($orders as $order) {
            $queue->createItem($order);
        }

        return $view = [
            '#markup' => "teste",
        ];
    }
}
<?php

/**
 * @file
 * Contains Drupal\correios_web_service\Plugin\QueueWorker\ManualCorreiosObjectTracker.php
 */

namespace Drupal\correios_web_service\Plugin\QueueWorker;

/**
 * A tracker that uses the tracking code of an order to search the status of an object from Correios.
 * 
 * @QueueWorker(
 *  id = "manual_correios_object_tracker",
 *  title = "Rastreador de Objetos dos Correios",
 * )
 */
class ManualCorreiosObjectTracker extends CorreiosObjectTrackerBase {}
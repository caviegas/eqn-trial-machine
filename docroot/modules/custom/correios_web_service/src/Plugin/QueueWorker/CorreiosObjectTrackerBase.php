<?php

/**
 * @file
 * Contains Drupal\correios_web_service\Plugin\QueueWorker\CorreiosObjectTrackerBase.php
 */

namespace Drupal\correios_web_service\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base functionality for the CorreiosObjectTracker Queue Workers.
 */
abstract class CorreiosObjectTrackerBase extends QueueWorkerBase implements ContainerFactoryPluginInterface
{
    /**
     * EntityTypeManagerInterface definition.
     * 
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface
     */
    protected $entityTypeManager;

    /**
     * LoggerChannelFactoryInterface definition.
     * 
     * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
     */
    protected $loggerChannelFactory;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $configuration,
                                $plugin_id,
                                $plugin_definition,
                                EntityTypeManagerInterface $entityTypeManager,
                                LoggerChannelFactoryInterface $loggerChannelFactory)
    {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->entityTypeManager = $entityTypeManager;
        $this->loggerChannelFactory = $loggerChannelFactory;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->get('entity_type.manager'),
            $container->get('logger.factory')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function processItem($order)
    {
        // @var CorreiosTracking $trackingService
        $correiosTracking = \Drupal::service('correios_web_service.tracking');

        $tracking_code = $order->get('field_tracking_code')->value;

        /*
        \Drupal::logger('correios_web_service_mail')->notice('Order (%id) / Tracking code (%tc)', [
            '%id' => $order->get('order_id')->value,
            '%tc' => $tracking_code
        ]);
        */

        try {

            $response = $correiosTracking->trackObjectFormatted($tracking_code);

            \Drupal::logger('correios_object_tracker')->notice('order (%id) code (%code): %response', [
                '%id' => $order->get('order_id')->value,
                '%code' => $tracking_code,
                '%response' => $response['description'],
            ]);
            
            //catch (\Exception $e) {
            //    throw new SuspendQueueException("Problema na consulta no Correios: %order_id.", [
            //        'order_id' => $order->get('id')->value
            //    ]);
            //}

            /*
            $response['status_id'] = '2';
            $response['description'] = 'A caminho';
            $response['event_code'] = 'aaa';
            $response['event_message'] = 'teste';
            $response['delivery_date'] = 'teste';
            */

            /*
            if (isset($order->original) && !$order->isNew() && $order->original->getVersion() > $order->getVersion()) {
                $order_id = $order->get('order_id')->value;
                $order = \Drupal::entityTypeManager()->getStorage('commerce_order')->load($order_id);
            }

            $order->set('state', $response['status_id']);
            $order->set('field_order_status', $response['description']);
            $order->set('field_tracking_event_code', $response['event_code']);
            $order->set('field_tracking_event_message', $response['event_message']);
            $order->set('field_tracking_delivery_date', $response['delivery_date']);

            $order->save();
            */

            /*
            // @var CorreiosEvent $correiosEvent
            $correiosEvent = \Drupal::service('correios_web_service.event');

            if ($order->get('field_order_status')->value == $correiosEvent->getStatusByAlias('entregue', 2)) {
                $params = [
                    'customer' => $order->get('uid')->entity,
                    'order' => $order,
                    'product' => $order->get('field_campaign')->entity->get('product_id')->entity,
                    'email' => $order->getEmail(),
                ];

                $correiosTracking->mailOrderDelivered($params);
            }
            */
        }
        catch (\Exception $e) {
            \Drupal::logger('correios_object_tracker')->error('order (%id) code (%code): %error', [
                '%id' => $order->get('order_id')->value,
                '%code' => $tracking_code,
                '%error' => $e->getMessage()
            ]);
        }
        
        // Log in the watchdog for debugging purposes.
        /*
        $this->loggerChannelFactory->get('debug')
            ->debug('Atualização da entidade @id da queue %item',
                [
                    '@id' => $order->get('order_id')->value,
                    '%item' => print_r($order, TRUE)
                ]);
        */
    }
}
<?php

/**
 * @file
 * Contains Drupal\correios_web_service\Plugin\QueueWorker\CronCorreiosObjectTracker.php
 */

namespace Drupal\correios_web_service\Plugin\QueueWorker;

/**
 * A tracker that uses the tracking code of an order to search the status of an object from Correios.
 * 
 * @QueueWorker(
 *  id = "cron_correios_object_tracker",
 *  title = "Cron do Rastreador de Objetos dos Correios",
 *  cron = {"time" = 60}
 * )
 */
class CronCorreiosObjectTracker extends CorreiosObjectTrackerBase {}
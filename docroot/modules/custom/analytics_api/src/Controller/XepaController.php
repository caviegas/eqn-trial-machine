<?php

namespace Drupal\analytics_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Database\Database;


/**
 * Defines ConnectController class.
 */
class XepaController extends ControllerBase {
    
    public function pageview() {
        
        $database = \Drupal::database();

        $query = $database->select('leftover__pageview', 'lp');
        // $query->condition('created_at', $currentTime, '>=');
        $query->where('created_at >= DATE_SUB(NOW(), INTERVAL 1 DAY)');
        $query->fields('lp', ['host', 'url', 'nid', 'email', 'ip', 'created_at', 'updated_at']);
        $result = $query->execute();
        $obj = $result->fetchAll();

        return new JsonResponse($obj);
    }

    public function logger() {
        
        $database = \Drupal::database();

        $query = $database->select('leftover__logger', 'll');
        // $query->condition('created_at', $currentTime, '>=');
        $query->where('created_at >= DATE_SUB(NOW(), INTERVAL 1 DAY)');
        $query->fields('ll', ['email', 'ip', 'created_at', 'updated_at']);
        $result = $query->execute();
        $obj = $result->fetchAll();

        return new JsonResponse($obj);
    }
}
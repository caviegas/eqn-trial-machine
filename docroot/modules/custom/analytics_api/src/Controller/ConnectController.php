<?php

namespace Drupal\analytics_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines ConnectController class.
 */
class ConnectController extends ControllerBase {
    
    public function pageview() {
        $ip_address = \Drupal::request()->getClientIp();
        $currentUser = \Drupal::currentUser();
        /*
            Quando o usuário é anônimo ou apenas autênticado, possui apenas uma role.
            Caso ele tenha alguma outra função (admin, autor), ele terá mais de uma
            Neste caso, queremos apenas usuários normais logados ou deslogados. 
        */ 
        if(count($currentUser->getRoles()) != 1) {
            return;
        }
        /* 
            Captura o e-mail se estiver logado. Este é o parâmetro 
            que usaremos para saber se o acesso era de um usuário autênticado ou não.
        */
        $email = ($currentUser->isAuthenticated()) ? $currentUser->getEmail() : null;

        // Limpando a URL acessada
        $current_uri = \Drupal::request()->getRequestUri();
        $url = $this->urlCleaning($current_uri);

        $node = \Drupal::routeMatch()->getParameter('node');
        $nid = ($node instanceof \Drupal\node\NodeInterface) ? $node->id() : null;

        $client = \Drupal::httpClient();

        try {
            $request = $client->request('POST', 'https://ninho.meeg.app/pagevie', [
    //   $request = $client->request('POST', 'https://google.com', [

            'json' => [
                'url'     => $url,
                'nid'     => $nid,
                'email'   => $email,
                "ip"      => $ip_address
                ]
            ]);
        } 
        catch (\Throwable $e) {
            return new JsonResponse('error');
        } 

        $response = json_decode($request->getBody());
        return new JsonResponse($response);
    }


    private function urlCleaning($url) {
        $url = "https://www.ninhosdobrasil.com.br".$url;

        // Remover "/" final
        $url = preg_replace('/\/$/', '', $url);
        // Remover "pt-br" 
        $url = preg_replace('/\/pt-br/', '', $url);
        // Remover /node que as vezes aparece na home (remove apenas que a url terminar em /node)
        $url = preg_replace('/\/node$/', '', $url);
      
        // Remover string ?destination= em diante
        if(strpos($url, "?destination=") == true){
          $url = preg_replace('/\?destination=.*$/', '', $url);
        }
      
        // Remover paginação do fim
        $url = preg_replace('/[\&|\?]page\=([0-9])*$/', '', $url);

        return $url;
    }

    public function logger() {
        $apiService = \Drupal::service('analytics_api.connection');
        
        $response = $apiService->logger();
        
        return new JsonResponse($response);
    }
}
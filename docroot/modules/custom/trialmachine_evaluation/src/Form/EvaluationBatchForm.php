<?php
/**
 * @file
 * Contains \Drupal\trialmachine_campaign\Form\TermForm.
 */

namespace Drupal\trialmachine_evaluation\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;


/**
 * Form controller for the content_entity_example entity edit forms.
 *
 * @ingroup content_entity_example
 */
class EvaluationBatchForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\trialmachine_evaluation\Entity\Evaluation */
    $form = parent::buildForm($form, $form_state);
    $form['code'] = array_merge($form['code'], [
      '#type' => 'textarea',
      '#title' => t('Códigos'),
    ]);  

    // dd($form);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Redirect to term list after save.
    $form_state->setRedirect('entity.trialmachine_evaluation.collection');
    $code = $form_state->getValue('code_wrapper');
    $form_state->unsetValue('code_wrapper');
    $codeArr = preg_split('/\n|\r\n?/', $code);
    foreach ($codeArr as $code) {
      if (!empty($code)) {
        $form_state->setValue('code', [['value' => $code]]);
        $entity = $this->buildEntity($form, $form_state);
        $uuidService = \Drupal::service('uuid');
        $uuid = $uuidService->generate();
        $entity->set('uuid', $uuid);
        $entity->save();
      }
    }

    return;
  }

}

<?php
/**
 * @file
 * Contains \Drupal\content_entity_example\Entity\ContentEntityExample.
 */

namespace Drupal\trialmachine_evaluation\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup evaluation
 *
 *
 * @ContentEntityType(
 *   id = "trialmachine_evaluation",
 *   label = @Translation("Evaluation entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\trialmachine_evaluation\Entity\Controller\EvaluationListBuilder",
 *     "form" = {
 *       "add" = "Drupal\trialmachine_evaluation\Form\EvaluationForm",
 *       "edit" = "Drupal\trialmachine_evaluation\Form\EvaluationForm",
 *       "delete" = "Drupal\trialmachine_evaluation\Form\EvaluationDeleteForm",
 *       "batch" = "Drupal\trialmachine_evaluation\Form\EvaluationBatchForm"
 *     },
 *     "access" = "Drupal\trialmachine_evaluation\EvaluationAccessControlHandler",
 *   },
 *   list_cache_contexts = { "user" },
 *   base_table = "trial_machine_evaluations",
 *   admin_permission = "administer trialmachine_evaluation entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "campaign_id" = "campaign_id",
 *     "answers" = "answers",
 *     "user_id" = "user_id",
 *     "order_id" = "order_id",
 *     "created_at" = "created_at"
 *    },
 *   field_ui_base_route = "entity.evaluation.settings",
 *   common_reference_target = TRUE,
 * )
 */
class Evaluation extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    // Default author to current user.
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  public function setValue($prop, $value) {
    $this->set($prop, $value);
  }

  public function searchAllQuestionsFromAnswers()
  {
    $answers = json_decode($this->get('answers')->value, true);

    if ($answers === false || $answers === NULL) {
      return false;
    }

    $questions_ids = array_keys($answers);

    $tbl_questions = 'trial_machine_questions';

    $query = \Drupal::database()->select($tbl_questions);

    $or_group = $query->orConditionGroup();

    foreach ($questions_ids as $id) {
      $or_group->condition("{$tbl_questions}.id", $id);
    }

    $query->fields($tbl_questions);

    $query->condition($or_group);

    $questions = $query->execute()->fetchAll();

    return $questions;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Evaluation entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Autor'))
      ->setDescription(t('Usuário vinculado ao cupom.'))
      ->setSetting('target_type', 'user')
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['campaign_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Vai pertencer à qual Campanha?'))
      ->setDescription(t('À qual campanha estão relacionados'))
      ->setSetting('handler', 'default')
      ->setSetting('target_type', 'trialmachine_campaign')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'campaign',
        'weight' => 4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ),
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Vai pertencer à qual Campanha?'))
      ->setDescription(t('À qual campanha estão relacionados'))
      ->setSetting('handler', 'default')
      ->setSetting('target_type', 'trialmachine_order')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'campaign',
        'weight' => 4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ),
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);



      $fields['answers'] = BaseFieldDefinition::create('string')
      ->setLabel(t('As respostas'))
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'campaign',
        'weight' => 4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ),
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['created_at'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Created At'))
      ->setDefaultValue([
        'default_date_type' => 'now',
        'default_date'      => 'now',
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => -3,
      ]);





    return $fields;
  }

}

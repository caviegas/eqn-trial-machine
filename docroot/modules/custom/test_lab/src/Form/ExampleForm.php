<?php

namespace Drupal\test_lab\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ExampleForm extends FormBase {
  
  public function getFormId() {

    // Unique ID of the form.
    return 'example_form';
  }
  public function buildForm(array $form, FormStateInterface $form_state) {

    // $arr = [
    //   0 => 'Four',
    //   1 => 'Five',
    //   2 => 'Six',
    // ];
    // foreach ($arr as $key => $value) {
      // $form['checkbox__container'] = [
      //   '#markup' => '<div class="checkbox__container">'
      // ];
      // $form['image'] = [
      //   '#markup' => '<img src="https://image.shutterstock.com/image-vector/ui-image-placeholder-wireframes-apps-260nw-1037719204.jpg"/>'
      // ];
      // $form[$key] = [
        // '#type' => 'checkbox',
        // '#title' => '<img src="https://image.shutterstock.com/image-vector/ui-image-placeholder-wireframes-apps-260nw-1037719204.jpg"/>',
        // '#attributes' => ['checked' => 'checked'],
      // ];
      // $form['checkbox__container'] = [
      //   '#markup' => '</div>'
      // ];
    // }
    // Create a $form API array.

    $termObjects = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['vid' => 'tags']);
    
    // $terms = ['Marca 1', 'Marca 2', 'Marca 3', 'Marca 4', 'Marca 5', 'Marca 6'];
    $selectedTerms = array();
    $unSelectedTerms = array();

    $selectedTags = array();
    $unSelectedTags = array();
    foreach ($termObjects as $term) {
      $terms[$term->id()] = '<img src="'.file_create_url($term->field_image->entity->getFileUri()).'"/>';
    }


    $form['selectedTags'] = array(
      '#type' => 'checkboxes',
      '#id' => 'selectedTags',
      //'#title' => 'Seus interesses',
      '#options' => $terms,
      // '#field_prefix' => '<img src="show" />',
      '#default_value' => $selectedTags
     );


    $form['save'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );

    return $form;
  }
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Validate submitted form data.
  }
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Handle submitted form data.
  }

}
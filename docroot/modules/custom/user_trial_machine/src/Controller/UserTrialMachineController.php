<?php
/**
 * @file
 * Contains \Drupal\user_trial_machine\Controller\UserTrialMachineController
 */

namespace Drupal\user_trial_machine\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

class UserTrialMachineController extends ControllerBase
{
    public function orders(Request $request)
    {
        $user_id = \Drupal::currentUser()->id();

        // @var \Drupal\trialmachine_order\Entity\Order $orders
        $orders = \Drupal::entityTypeManager()->getStorage('trialmachine_order')
            ->loadByProperties(['user_id' => $user_id]);

        // @var \Drupal\trialmachine_order\Service\OrderService
        $orderService = \Drupal::service('trialmachine_order.order');

        /**
         * Data array to the view
         */
        $data = [
            'orders' => $orders,
            'orderService' => $orderService,
        ];

        return [
            '#theme' => 'orders_list',
            '#data' => $data,
        ];
    }
}
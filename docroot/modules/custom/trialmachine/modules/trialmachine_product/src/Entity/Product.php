<?php
/**
 * @file
 * Contains \Drupal\content_entity_example\Entity\ContentEntityExample.
 */

namespace Drupal\trialmachine_product\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup product
 *
 *
 * @ContentEntityType(
 *   id = "trialmachine_product",
 *   label = @Translation("Product entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\trialmachine_product\Entity\Controller\ProductListBuilder",
 *     "form" = {
 *       "add" = "Drupal\trialmachine_product\Form\ProductForm",
 *       "edit" = "Drupal\trialmachine_product\Form\ProductForm",
 *       "delete" = "Drupal\trialmachine_product\Form\ProductDeleteForm",
 *     },
 *     "access" = "Drupal\trialmachine_product\ProductAccessControlHandler",
 *   },
 *   list_cache_contexts = { "user" },
 *   base_table = "trialmachine_product",
 *   admin_permission = "administer trialmachine_product entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "title" = "title",
 *     "author_id" = "author_id",
 *     "created" = "created",
 *     "changed" = "changed",
 *     "image__target_id" = "image__target_id",
 *     "image__alt" = "image__alt",
 *     "image__title" = "image__title",
 *     "image__width" = "image__width",
 *     "image__height" = "image__height",
 *    },
 *   links = {
 *     "canonical" = "/trialmachine_product/{trialmachine_product}",
 *     "edit-form" = "/trialmachine_product/{trialmachine_product}/edit",
 *     "delete-form" = "/trialmachine_product/{trialmachine_product}/delete",
 *     "collection" = "/trialmachine_product/list"
 *   },
 *   field_ui_base_route = "entity.product.settings",
 *   common_reference_target = TRUE,
 * )
 */
class Product extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * * GETTERS
  */

  /**
   * GET the author.
   * 
   * @return \Drupal\user\Entity\User
   * The related entity.
   */
  public function getAuthor()
  {
    return $this->get('author_id')->entity;
  }

  /**
   * GET the image.
   * 
   * @return \Drupal\file\Entity\File
   * The related entity.
   */
  public function getImage()
  {
    return $this->get('image')->entity;
  }

  /**
   * GET the image alt property
   * 
   * @return string
   */
  public function getImageAlt()
  {
    return $this->get('image')->alt;
  }

  /**
   * GET the image title property
   * 
   * @return string
   */
  public function getImageTitle()
  {
    return $this->get('image')->title;
  }

  /**
   * GET the image width property
   * 
   * @return int
   */
  public function getImageWidth()
  {
    return (int) $this->get('image')->width;
  }

  /**
   * GET the image height property
   * 
   * @return int
   */
  public function getImageHeight()
  {
    return (int) $this->get('image')->height;
  }

  /**
   * GET the SKU.
   * 
   * @return string
   */
  public function getSKU()
  {
    return $this->get('sku')->value;
  }

  /**
   * GET the title.
   * 
   * @return string
   */
  public function getTitle()
  {
    return $this->get('title')->value;
  }

  /**
   * GET the weight.
   * 
   * @return string
   */
  public function getWeight()
  {
    return $this->get('weight')->value;
  }

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    // Default author to current user.
    $values += array(
      'author_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Product entity.'))
      ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Título'))
    ->setDescription(t('Título do produto.'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => -1,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -1,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Descrição'))
    ->setDescription(t('Descrição do produto.'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'text_textarea_with_summary',
      'weight' => 1,
    ])
    ->setDisplayOptions('form', [
      'type' => 'text_textarea_with_summary',
      'settings' => array(
        'rows' => 4,
      ),
      'weight' => 1,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Imagem'))
      ->setDescription(t('Imagem da campanha.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'image',
        'weight' => 2,
        'label' => 'hidden',
        'settings' => [
          'image_style' => 'large',
        ],
      ])
      ->setDisplayOptions('form',  [
        'type' => 'image',
        'weight' => 2,
        'label' => 'hidden',
        'settings' => [
          'image_style' => 'thumbnail',
        ]
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setReadOnly(TRUE);

    $fields['sku'] = BaseFieldDefinition::create('string')
    ->setLabel(t('SKU do Produto'))
    ->setDescription(t('Stock Keeping Unit.'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => -1,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -1,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Peso do produto'))
    ->setDescription(t('Peso'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => -1,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -1,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    // The form presents a auto complete field for the user name.
    $fields['author_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Autor'))
      ->setDescription(t('O criador do produto.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 3,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ),
        'weight' => 3,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}

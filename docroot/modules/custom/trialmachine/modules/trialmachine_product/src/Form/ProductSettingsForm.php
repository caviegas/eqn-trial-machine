<?php
/**
 * @file
 * Contains \Drupal\trialmachine_product\Form\TermSettingsForm.
 */

namespace Drupal\trialmachine_product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContentEntityExampleSettingsForm.
 *
 * @package Drupal\trialmachine_product\Form
 *
 * @ingroup product
 */
class ProductSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'trialmachine_product_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['trialmachine_product_settings']['#markup'] = 'Settings form for product Term. Manage field settings here.';
    return $form;
  }

}

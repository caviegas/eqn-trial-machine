<?php

/**
 * @file providing axuliar methods for responses in the datalab endpoints.
 * Contains \Drupal\datalab_endpoints\Service\DatalabResponseManager
 */

namespace Drupal\datalab_endpoints\Service;

use Symfony\Component\HttpFoundation\JsonResponse;

class DatalabResponseManager
{
    /**
     * Array that handle any errors and its data arranged with an alias.
     * @var array
     */
    private $errors = [
        'no_content' => [
            'status' => 204,
            'type' => 'No Content',
            'message' => 'No data found in this request.',
        ],
    ];

    /**
     * Get the errors by its alias.
     * @param string $alias.
     * The friendly name for the error.
     * 
     * @return array
     * The array with data from the error.
     */
    public function getErrorByName($alias)
    {
        return $this->errors[$alias];
    }

    /**
     * Create the response when data must be sent as JSON.
     * 
     * @param array $data
     * The data from the endpoint to send as JSON.
     * @param array $headers
     * If the header is needed it can be sent from the endpoint.
     * It is also possible for any internal logic to override the header.
     * 
     * @return JsonResponse
     */
    public function sendDataResponse($data, $headers = [])
    {
        $response = [
            'status' => 'success',
            'data' => $data,
        ];

        return new JsonResponse($response, 200, $headers);
    }

    /**
     * Create the error response.
     * 
     * @param string $alias
     * The friendly name for the error.
     * @param array $headers
     * If the header is needed it can be sent from the endpoint.
     * It is also possible for any internal logic to override the header.
     * 
     * @return JsonResponse
     * It will return the $response params only if it is needed or permitted.
     */
    public function sendError($alias = 'no_content', $headers = [])
    {
        $error_data = $this->getErrorByName($alias);

        $status = $error_data['status'];

        $response = [
            'status' => 'error',
            'error' => $error_data,
        ];

        return new JsonResponse($response, $status, $headers);
    }
}
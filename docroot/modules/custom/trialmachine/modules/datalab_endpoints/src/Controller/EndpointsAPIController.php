<?php
/**
 * @file
 * Contains \Drupal\datalab_endpoints\Controller\EndpointsAPIController
 */

namespace Drupal\datalab_endpoints\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

class EndpointsAPIController extends ControllerBase
{
    /**
     * Service to manage the JSON response for the endpoints.
     * @var \Drupal\datalab_endpoints\Service\DatalabResponseManager
     */
    private $responseManager;

    public function __construct()
    {
        $this->responseManager = \Drupal::service('trialmachine.datalab_response_manager');
    }

    /**
     * Callback endpoint 'api/campaigns'
     * 
     * @return JsonResponse
     * The JsonResponse object with the formatted data or with the error data.
     */
    public function campaigns()
    {
        /** @var \Drupal\trialmachine_campaign\Entity\Campaign $campaignStorage */
        $campaignStorage = \Drupal::entityTypeManager()->getStorage('trialmachine_campaign');

        /** IDs of all campaigns */
        $ids = $campaignStorage->getQuery()->execute();

        /** @var \Drupal\trialmachine_campaign\Entity\Campaign $campaigns */
        $campaigns = $campaignStorage->loadMultiple($ids);

        if (empty($campaigns)) {
            return $this->responseManager->sendError();
        }

        $data = [];

        foreach ($campaigns as $campaign) {
            /** Set Main Data fields */
            $id = $campaign->id();
            $name = $campaign->getTitle();
            $start_date = date_format(date_create($campaign->getStartDate()), 'd-m-Y');
            $end_date = date_format(date_create($campaign->getEndDate()), 'd-m-Y');
            $duration = $campaign->getDurationInDays();

            /** Set Product fields */
            /** @var \Drupal\trialmachine_product\Entity\Product $product */
            $product = $campaign->getProduct();
            $product_id = $product->id();
            $product_name = $product->getTitle();
            $product_sku = $product->getSKU();
            $product_quantity = $campaign->getQuantity();
            $product_requested = $campaign->getTotalOfRequestedProducts();
            $product_total = $product_quantity + $product_requested;
            $brand_name = $product->getAuthor()->getDisplayName();

            /** Response data */
            $data[] = [
                'id' => $id,
                'name' => $name,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'duration' => $duration,
                'brand_name' => $brand_name,
                'product' => [
                    'id' => $product_id,
                    'name' => $product_name,
                    'SKU' => $product_sku,
                    'quantity' => [
                        'total' => $product_total,
                        'requested' => $product_requested,
                        'stock' => $product_quantity,
                    ],
                ],
            ];
        }

        return $this->responseManager->sendDataResponse($data);
    }

    /**
     * Callback endpoint 'api/coupons'
     * Only allowed GET requests.
     * 
     * @return JsonResponse
     * The JsonResponse object with the formatted data or with the error data.
     */
    public function coupons()
    {
        /** @var \Drupal\trialmachine_coupon\Entity\Coupon $couponStorage */
        $couponStorage = \Drupal::entityTypeManager()->getStorage('trialmachine_coupon');

        /** IDs of coupon entity */
        $coupon_ids = $couponStorage->getQuery()->execute();

        /** @var \Drupal\trialmachine_coupon\Entity\Coupon $coupons */
        $coupons = $couponStorage->loadMultiple($coupon_ids);

        if (empty($coupons)) {
            return $this->responseManager->sendError();
        }

        $data = [];

        foreach ($coupons as $coupon) {
            /** Set Main Data fields */
            $id = $coupon->get('code')->value;
            $name = $coupon->get('name')->value;
            $discount = "{$coupon->get('discount')->value}%";
            $sku = $coupon->get('sku')->value;
            $start_date = date_format(date_create($coupon->get('start_date')->value), 'd-m-Y');
            $end_date = date_format(date_create($coupon->get('end_date')->value), 'd-m-Y');
            $duration = $coupon->getDurationInDays();
            $user_id = NULL;
            $available = $coupon->checkAvailable();

            /** Coupons don't need an user_id so it needs to check if there is one. */
            /** @var \Drupal\Core\Entity\User $customer */
            $customer = $coupon->get('user_id')->entity;

            if (!empty($customer) && $customer->hasField('field_cadu_id')) {
                $user_id = $customer->get('field_cadu_id')->value;
            }

            /** Response data */
            $data[] = [
                'id' => $id,
                'name' => $name,
                'discount' => $discount,
                'sku' => $sku,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'duration' => $duration,
                'user_id' => $user_id,
                'available' => $available,
            ];
        }

        return $this->responseManager->sendDataResponse($data);
    }

    /**
     * Callback endpoint 'api/orders'
     * 
     * @return JsonResponse
     * The JsonResponse object with the formatted data or with the error data.
     */
    public function orders(Request $request)
    {
        /** @var \Drupal\trialmachine_order\Entity\Order $orderStorage */
        $orderStorage = \Drupal::entityTypeManager()->getStorage('trialmachine_order');

        /** IDs of order entity */
        $order_ids = $orderStorage->getQuery()->execute();

        /** @var \Drupal\trialmachine_order\Entity\Order $orders */
        $orders = $orderStorage->loadMultiple($order_ids);

        if (empty($orders)) {
            return $this->responseManager->sendError();
        }

        $data = [];

        foreach ($orders as $order) {
            /** Set Main Data fields */
            $id = $order->getId();
            $status = $order->getStatusName();
            $tracking_code = $order->getTrackingCode();
            $created_at = DrupalDateTime::createFromTimestamp($order->get('created')->value)->format('d-m-Y');

            /** Set Campaign fields */
            /** @var \Drupal\trialmachine_campaign\Entity\Campaign $campaign */
            $campaign = $order->getCampaign();
            $campaign_id = $campaign->id();
            $campaign_name = $campaign->getTitle();

            /** Set User fields */
            /** @var \Drupal\user\Entity\User $user */
            $user = $order->getCustomer();
            $user_id = $user->hasField('field_cadu_id') ? $user->get('field_cadu_id')->value : NULL;
            $user_email = $user->getEmail();
            $user_address = NULL;

            /** @var \Drupal\trialmachine_address\Entity\Address $address */
            $address = $order->getAddress();

            if (!empty($address)) {
                $user_address = [
                    'cep' => $address->getCEP(),
                    'street' => $address->getStreet(),
                    'number' => $address->getNumber(),
                    'complement' => $address->getComplement(),
                    'district' => $address->getDistrict(),
                    'city' => $address->getCity(),
                    'state' => $address->getState(),
                ];
            }

            /** Response data */
            $data[] = [
                'id' => $id,
                'status' => $status,
                'tracking_code' => $tracking_code,
                'created_at' => $created_at,
                'campaign' => [
                    'id' => $campaign_id,
                    'name' => $campaign_name,
                ],
                'user' => [
                    'user_id' => $user_id,
                    'email' => $user_email,
                    'address' => $user_address,
                ],
            ];
        }

        return $this->responseManager->sendDataResponse($data);
    }

    /**
     * Callback endpoint 'api/ratings'
     * 
     * @return JsonResponse
     * The JsonResponse object with the formatted data or with the error data.
     */
    public function ratings(Request $request)
    {
        /** @var \Drupal\trialmachine_evaluation\Entity\Evaluation $evaluationStorage */
        $evaluationStorage = \Drupal::entityTypeManager()->getStorage('trialmachine_evaluation');

        /** IDs from evaluation entity */
        $evaluation_ids = $evaluationStorage->getQuery()->execute();

        /** @var \Drupal\trialmachine_evaluation\Entity\Evaluation $evaluations */
        $evaluations = $evaluationStorage->loadMultiple($evaluation_ids);

        if (empty($evaluations)) {
            return $this->responseManager->sendError();
        }

        $data = [];

        foreach ($evaluations as $evaluation) {
            /** Set Main Data fields */
            $id = $evaluation->id();

            /** Set Information fields */
            /** @var \Drupal\trialmachine_campaign\Entity\Campaign $campaign */
            $campaign = $evaluation->get('campaign_id')->entity;
            /** @var \Drupal\trialmachine_order\Entity\Order $order */
            $order = $evaluation->get('order_id')->entity;
            /** @var \Drupal\user\Entity\User $customer */
            $customer = $evaluation->get('user_id')->entity;
            
            $campaign_id = $campaign->get('id')->value;
            $campaign_name = $campaign->get('title')->value;
            $order_id = empty($order) ? NULL : $order->id();
            $user_id = $customer->hasField('field_cadu_id') ? $customer->get('field_cadu_id')->value : NULL;
            $created_at = empty($evaluation->get('created_at')->value) ? NULL : date_format(date_create($evaluation->get('created_at')->value), 'd-m-Y');
            
            /** Set Evaluations fields */
            $answers = json_decode($evaluation->get('answers')->value, true);
            $questions = $evaluation->searchAllQuestionsFromAnswers();

            $data_evaluations = [];

            if (!empty($questions)) {
                foreach ($questions as $question) {
                    $id = $question->id;

                    $data_evaluations[$id] = [
                        'question' => $question->question,
                        'answer' => $answers[$id],
                    ];
                }
            }
            
            /** Response data */
            $data[] = [
                'id' => $id,
                'information' => [
                    'campaign_id' => $campaign_id,
                    'campaign_name' => $campaign_name,
                    'order_id' => $order_id,
                    'user_id' => $user_id,
                    'created_at' => $created_at,
                ],
                'evaluations' => $data_evaluations,
            ];
        }

        return $this->responseManager->sendDataResponse($data);
    }
}
<?php
/**
 * @file
 * Contains \Drupal\trialmachine_coupon\Form\CouponSettingsForm.
 */

namespace Drupal\trialmachine_coupon\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContentEntityExampleSettingsForm.
 *
 * @package Drupal\trialmachine_coupon\Form
 *
 * @ingroup coupon
 */
class CouponSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'trialmachine_coupon_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['trialmachine_coupon_settings']['#markup'] = 'Settings form for coupon Term. Manage field settings here.';
    return $form;
  }

}

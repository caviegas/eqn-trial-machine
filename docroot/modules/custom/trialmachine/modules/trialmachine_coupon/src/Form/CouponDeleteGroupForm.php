<?php

/**
 * @file
 * Contains \Drupal\trialmachine_coupon\Form\CouponGroupDeleteForm
 */

namespace Drupal\trialmachine_coupon\Form;

//use Symfony\Component\DependencyInjection\ContainerInterface;
//use Drupal\Core\Entity\ContentEntityConfirmFormBase;
//use Drupal\Core\Routing\CurrentRouteMatch;
//use Drupal\Core\Form\FormBase;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\trialmachine_coupon\Entity\Coupon;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Form for deleting a coupon and its group.
 *
 */
class CouponDeleteGroupForm extends ConfirmFormBase
{
    /**
     * Array of coupon entities to delete.
     * 
     * @var array \Drupal\trialmachine_coupon\Entity\Coupon
     */
    protected $coupons;

    /**
     * Name of the coupon.
     * 
     * @var string
     */
    protected $name;

    /**
     * {@inheritDoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, Coupon $trialmachine_coupon = NULL)
    {
      $coupon = $trialmachine_coupon;

      $this->name = $coupon->getName();

      /** @var \Drupal\trialmachine_coupon\Entity\Coupon $couponStorage */
      $couponStorage = \Drupal::entityTypeManager()->getStorage('trialmachine_coupon');

      $query = $couponStorage->getQuery()->condition('name', $this->name);
      $queryNoUser = $query;
      $queryUser = $query;

      $id_no_user = $couponStorage->getQuery()
          ->condition('name', $this->name)
          ->condition('user_id', NULL, 'IS NULL')
          ->execute();
      
      $this->coupons = $couponStorage->loadMultiple($id_no_user);

      $id_user = $couponStorage->getQuery()
          ->condition('name', $this->name)
          ->condition('user_id', NULL, 'IS NOT NULL')
          ->execute();

      $couponsUser = $couponStorage->loadMultiple($id_user);

      $form = parent::buildForm($form, $form_state);

      if (!empty($couponsUser)) {

        $form['user_coupons'] = [
            '#type' => 'table',
            '#caption' => $this->t('Os cupons adiante já foram tomados e não podem ser apagados.'),
            '#header' => [
                $this->t('Código'),
                $this->t('Solicitante'),
            ],
        ];

        foreach ($couponsUser as $coupon) {
            $form['user_coupons'][] = [
                'code' => [
                    '#type' => 'textfield',
                    '#title' => $this->t('Código'),
                    '#default_value' => $coupon->getCode(),
                    '#title_display' => 'invisible',
                    '#disabled' => TRUE,
                ],
                'user' => [
                    '#type' => 'textfield',
                    '#title' => $this->t('Solicitante'),
                    '#default_value' => $coupon->getUser()->getEmail(),
                    '#title_display' => 'invisible',
                    '#disabled' => TRUE,
                ],
            ];
        }
      }

      return $form;
    }

    /**
     * {@inheritDoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        if (empty($this->coupons)) {
            \Drupal::messenger()->addMessage(t('Não existem cupons para apagar.'));
        }

        if (!empty($this->coupons)) {
            $codes = [];

            foreach ($this->coupons as $coupon) {
                $codes[] = $coupon->getCode();
                $coupon->delete();
            }

            \Drupal::messenger()->addMessage(t('Cupons apagados de nome %name e códigos: %codes', [
                '%name' => $this->name,
                '%codes' => trim(implode(', ', $codes)),
            ]));
        }

        $form_state->setRedirect('entity.trialmachine_coupon.collection');
    }

    /**
     * {@inheritDoc}
     */
    public function getFormId() : string
    {
        return 'trialmachine_coupon_delete_group';
    }

    /**
     * {@inheritdoc}
     */
    public function getCancelUrl() : Url
    {
        return new Url('entity.trialmachine_coupon.collection');
    }

    /**
     * {@inheritdoc}
     */
    public function getQuestion()
    {
        return $this->t('Tem certeza que deseja deletar o cupom e seu grupo semelhante?');
    }
}
<?php
/**
 * @file
 * Contains \Drupal\content_entity_example\Entity\ContentEntityExample.
 */

namespace Drupal\trialmachine_coupon\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup coupon
 *
 *
 * @ContentEntityType(
 *   id = "trialmachine_coupon",
 *   label = @Translation("Coupon entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\trialmachine_coupon\Entity\Controller\CouponListBuilder",
 *     "form" = {
 *       "add" = "Drupal\trialmachine_coupon\Form\CouponForm",
 *       "edit" = "Drupal\trialmachine_coupon\Form\CouponForm",
 *       "delete" = "Drupal\trialmachine_coupon\Form\CouponDeleteForm",
 *       "batch" = "Drupal\trialmachine_coupon\Form\CouponBatchForm"
 *     },
 *     "access" = "Drupal\trialmachine_coupon\CouponAccessControlHandler",
 *   },
 *   list_cache_contexts = { "user" },
 *   base_table = "trialmachine_coupon",
 *   admin_permission = "administer trialmachine_coupon entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "code" = "code",
 *     "name" = "name",
 *     "campaign_id" = "campaign_id",
 *     "start_date" = "start_date", 
 *     "end_start" = "end_start", 
 *     "discount" = "discount",
 *     "created" = "created",
 *     "changed" = "changed",
 *    },
 *   links = {
 *     "canonical" = "/trialmachine_coupon/{trialmachine_coupon}",
 *     "collection" = "/trialmachine_coupon/list",
 *     "edit-form" = "/trialmachine_coupon/{trialmachine_coupon}/edit",
 *     "delete-form" = "/trialmachine_coupon/{trialmachine_coupon}/delete",
 *     "delete-group" = "/trialmachine_coupon/{trialmachine_coupon}/delete-group"
 *   },
 *   field_ui_base_route = "entity.coupon.settings",
 *   common_reference_target = TRUE,
 * )
 */
class Coupon extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  // public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
  //   parent::preCreate($storage_controller, $values);
  //   // Default author to current user.
  //   $values += array(
  //     'user_id' => \Drupal::currentUser()->id(),
  //   );
  // }

  public function setValue($prop, $value) {
    $this->set($prop, $value);
  }

  /**
   * GET code
   * 
   * @return string
   */
  public function getCode()
  {
    return $this->get('code')->value;
  }

  /**
   * Gets the coupon's duration in days.
   * 
   * @return int
   * The difference number in days between start_date and end_date.
   * 
   * @return boolean
   * FALSE if date_diff is FALSE.
   */
  public function getDurationInDays()
  {
    $start_date = date_create($this->get('start_date')->value);
    $end_date = date_create($this->get('end_date')->value);

    $date_diff = date_diff($start_date, $end_date);

    if ($date_diff === false) {
      return false;
    }

    return $date_diff->days;
  }

  /**
   * GET name
   * 
   * @return string
   */
  public function getName()
  {
    return $this->get('name')->value;
  }

  /**
   * GET User
   * 
   * @return \Drupal\user\Entity\User
   */
  public function getUser()
  {
    return $this->get('user_id')->entity;
  }

  /**
   * Check if the coupon is available.
   * 
   * @return boolean
   * Returns TRUE if the coupon is available or FALSE if it is not.
   */
  public function checkAvailable()
  {
    $today = new DrupalDateTime();
    $start_date = new DrupalDateTime($this->get('start_date')->value);
    $end_date = new DrupalDateTime($this->get('end_date')->value);

    if (!empty($this->get('user_id')->value)) {
      return false;
    }

    if ($today->getTimestamp() < $start_date->getTimestamp()) {
      return false;
    }

    if ($today->getTimestamp() > $end_date->getTimestamp()) {
      return false;
    }

    return true;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Coupon entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('UUID da entidade Coupon.'))
      ->setReadOnly(TRUE);

    $fields['code'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Códigos'))
    ->setDescription(t('Códigos.'))
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string_textarea',
      'weight' => 5,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textarea',
      'weight' => 5,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
    ->setLabel(t('O nome do cupom'))
    ->setDescription(t('Nome do cupom.'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => 0,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['sku'] = BaseFieldDefinition::create('string')
    ->setLabel(t('SKU do produto à receber o desconto do cupom'))
    ->setDescription(t('Código SKU.'))
    ->setSettings([
      'default_value' => null,
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'description' => 'inline',
      'type' => 'string',
      'weight' => 0,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

  
    // Owner field of the COUPON.
    // Entity reference field, holds the reference to the user object.
    // The view shows the user name field of the user.
    // The form presents a auto complete field for the user name.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Autor'))
      ->setDescription(t('Usuário vinculado ao cupom.'))
      ->setSetting('target_type', 'user')
      ->setRequired(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['campaign_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Vai pertencer à qual Campanha?'))
      ->setDescription(t('À qual campanha estão relacionados'))
      ->setSetting('handler', 'default')
      ->setSetting('target_type', 'trialmachine_campaign')
      ->setRequired(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'campaign',
        'weight' => 4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ),
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['discount'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Qual a porcentagem de desconto deste cupom?'))
      ->setDescription(t('Quantidade de produtos disponíveis nesta campanha.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'integer',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'integer',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


      $fields['start_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Início da validade do cupom.'))
      ->setDescription(t('Inicio do cupom.'))
      ->setRequired(TRUE)
      ->setSetting('datetime_type', 'datetime')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'datetime',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime',
        'weight' => 6,
      ])
      ->setDefaultValue(DrupalDateTime::createFromTimestamp(time()))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['end_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Expiração do cupom'))
      ->setDescription(t('Término do cupom.'))
      ->setDefaultValue(DrupalDateTime::createFromTimestamp(time()))
      ->setSettings([
        'datetime_type' => 'datetime',
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'datetime',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);


    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}

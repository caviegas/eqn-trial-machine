<?php

namespace Drupal\trialmachine_coupon\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * create custom action
 * 
 * @action(
 *  id = "trialmachine_coupon_delete_action",
 *  label = @translation("Deletar Cupom"),
 *  type = "trialmachine_coupon"
 * )
 */
class DeleteAction extends ActionBase
{
    /**
     * {@inheritDoc}
     */
    public function execute($coupon = NULL)
    {
        if ($coupon) {
            //TODO: lógica do delete aqui.
            \Drupal::messenger()->addStatus(t('O cupom foi deletado.'));
        }
    }

    /**
     * {@inheritDoc}
     */
    public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE)
    {
        /** @var \Drupal\trialmachine_coupon\Entity\Coupon $coupon */
        //TODO: escrever as permissões aqui.
        $result = $object->access('create', $account, TRUE);
        return $return_as_object ? $result : $result->isAllowed();
    }
}
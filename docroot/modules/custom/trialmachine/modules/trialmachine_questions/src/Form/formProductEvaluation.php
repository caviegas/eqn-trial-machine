<?php

namespace Drupal\modulo_eqn_questoes\Form;



use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DrupalDateTime;

use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Drupal\trialmachine_product\Entity\Product;
use Drupal\trialmachine_campaign\Entity\Campaign;
use Drupal\trialmachine_order\Entity\Order;
use Drupal\user\Entity\User;

use Drupal\trialmachine_evaluation\Entity\Evaluation;




use Drupal\Core\Database\Connection;



/**
 * Our custom ajax form.
 */
class formProductEvaluation extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return "form_eqn_evaluation";
    }

    /**
     * {@inheritdoc} 
     */

    public function buildForm(array $form, FormStateInterface $form_state) {
        // verifica se o user tá logado
        if (\Drupal::currentUser()->isAuthenticated()) {
            $x = "";
        } else {
            return new RedirectResponse('/user/entrar?destination=/eqn/evaluate?order=' . $_GET['order']);   
        }


        if(!isset($_GET['order'])){
            // redireciona se não tem o parametro order na url
            return new RedirectResponse('/'); 
        }
        
        $order = Order::load($_GET['order']);
        if(!$order){
            // redireciona pra home se o id não for de uma order
            return new RedirectResponse('/');
           
        }

        if(\Drupal::currentUser()->getEmail() != $order->user_id->entity->mail->value){
            // redireciona pra home se a order não é do usuario logado
            return new RedirectResponse('/');   
            
        }

        if($order->status->value != '3'){
            // redireciona pra home se o status da order não foi recebido (status 3)
            return new RedirectResponse('/');   
        }
        if($order->status->value == '4'){
            // redireciona pra home se a order tem status avaliada (4)
            return new RedirectResponse('/');   
        
        }
        // if(!$order->hasField('field_campaign')){
        //     // entra aqui se a order não tem campanha
        //     return new RedirectResponse('/'); 
            
        // }

        $form['markup0'] = [
            '#markup' => '<div class="rating_bar"><span></span></div>' 
        ];

        
        $form['markup1'] = [
            '#markup' => '<div class="rating_reminder"><img src="https://dev-dig0031080-nestle-corporate-brazil.pantheonsite.io/sites/default/files/pictures/2021-08/dummy-image-square.jpeg"><div>' 
        ];        
            
        $form['markup2'] = [
            '#markup' => '<span>Você está avaliando</span> <strong>'. $order->campaign_id->entity->product_id->entity->title->value . '</strong></div></div>' 
        ];
        
        // mostra as perguntas
        $database = \Drupal::database();
        $questions = $database->query("SELECT * from trial_machine_questions");
            foreach ($questions as $question) {
                if ($question->field_type == 'radio') {
                    $form['question_' . $question->id] = array(
                        '#type' => 'radios',
                        '#title' => $question->question,
                        '#options' => array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5'),
                    );
                }else if($question->field_type == 'bool') {
                    $form['question_' . $question->id] = array(
                        '#type' => 'radios',
                        '#title' => $question->question,
                        '#options' => array('SIM' => 'Sim', 'NAO' => 'Não'),
                    );
                } else {
                    $form['question_' . $question->id] = [
                        '#type' => 'textarea',
                        '#title' => $question->question,
                        '#legend' => '<legend>TESTE</legend>'

                    ];
                }
            }
            $form['actions']['#type'] = 'actions';
            $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('SALVAR E AVANÇAR'),
          '#button_type' => 'primary',
        ];
 
         return $form;


    }

    public function submitForm(array &$form, FormStateInterface $form_state){

        $answers[1] = $form_state->getValues()['question_1'];
        $answers[2] = $form_state->getValues()['question_2'];
        $answers[3] = $form_state->getValues()['question_3'];
        $answers[4] = $form_state->getValues()['question_4'];
        $answers[5] = $form_state->getValues()['question_5'];
        $answers[6] = $form_state->getValues()['question_6'];

        $order = Order::load($_GET['order']);

        $evaluation = Evaluation::create([
            ':user_id' => \Drupal::currentUser()->id(),
            'campaign_id' => $order->campaign_id->entity->id(),
            'order_id' => $_GET['order'],
            'answers' => \json_encode($answers),
            // 'created_at' => date("Y-m-d H:i:s")
        ]);
        $evaluation->save();


        $order->status = 4;
        $order->save();

        // TODO alterar status da order para 5 
        $redirect_path = '/eqn/getcoupon?order=' . $_GET['order']; // TODO redirect after login
        $url = url::fromUserInput($redirect_path);
        $form_state->setRedirect('machine_name');
        $form_state->setRedirectUrl($url);
        return true;

    }
}
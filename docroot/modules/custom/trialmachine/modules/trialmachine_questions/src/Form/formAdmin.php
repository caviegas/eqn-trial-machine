<?php

namespace Drupal\modulo_eqn_questoes\Form;


use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Datetime\DrupalDateTime;

use Drupal\Core\Url;
use Drupal\Core\Messenger;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;
use Symfony\Component\HttpFoundation\RedirectResponse;



// namespace Drupal\Core\Database\Query;

use Drupal\Core\Database\Connection;



/**
 * Our custom ajax form.
 */
class formAdmin extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return "form_eqn_admin";
    }

    /**
     * {@inheritdoc} 
     */

    public function buildForm(array $form, FormStateInterface $form_state) {


     
        $form['submit_group_a'] = [
            '#type' => 'markup',
            '#markup' => '<div class="form-nav">
              <a href="questions" ><button class="primary">Editar Perguntas</button></a>

            </div>'
          ];

 
         return $form;


    }

    public function submitForm(array &$form, FormStateInterface $form_state){




        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 1,
            ':question' => $form_state->getValues()['question_1_question'],
            ':field_type' => $form_state->getValues()['question_1_type']
        ]);

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 2,
            ':question' => $form_state->getValues()['question_2_question'],
            ':field_type' => $form_state->getValues()['question_2_type']
        ]);

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 3,
            ':question' => $form_state->getValues()['question_3_question'],
            ':field_type' => $form_state->getValues()['question_3_type']
        ]);

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 4,
            ':question' => $form_state->getValues()['question_4_question'],
            ':field_type' => $form_state->getValues()['question_4_type']
        ]);

        $database = \Drupal::database();
        $questions = $database->query("UPDATE trial_machine_questions SET question = :question, field_type = :field_type where id = :id", [
            ':id' => 5,
            ':question' => $form_state->getValues()['question_5_question'],
            ':field_type' => $form_state->getValues()['question_5_type']
        ]);

        return true;

    }
}
<?php
/**
 * @file
 * Contains \Drupal\content_entity_example\Entity\ContentEntityExample.
 */

namespace Drupal\trialmachine_address\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup address
 *
 *
 * @ContentEntityType(
 *   id = "trialmachine_address",
 *   label = @Translation("Address entity"),
 *   list_cache_contexts = { "user" },
 *   base_table = "trialmachine_address",
 *   admin_permission = "administer trialmachine_address entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "user_id" = "user_id",
 *     "state" = "state",
 *     "city" = "city",
 *     "district" = "district", 
 *     "street" = "street", 
 *     "number" = "number",
 *     "created" = "created",
 *     "changed" = "changed",
 *    },
 *   links = {
 *     "canonical" = "/trialmachine_address/{trialmachine_address}",
 *     "edit-form" = "/trialmachine_address/{trialmachine_address}/edit",
 *     "delete-form" = "/trialmachine_address/{trialmachine_address}/delete",
 *     "collection" = "/trialmachine_address/list"
 *   },
 *   field_ui_base_route = "entity.address.settings",
 *   common_reference_target = TRUE,
 * )
 */
class Address extends ContentEntityBase {

  use EntityChangedTrait;

  // public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
  //   parent::preCreate($storage_controller, $values);
  //   // Default author to current user.
  //   $values += array(
  //     'user_id' => \Drupal::currentUser()->id(),
  //   );
  // }

  /**
   * GET the CEP.
   * 
   * @return string
   */
  public function getCEP()
  {
    return $this->get('cep')->value;
  }

  /**
   * GET the street's complement.
   * 
   * @return string
   */
  public function getComplement()
  {
    return $this->get('complement')->value;
  }

  /**
   * GET the city.
   * 
   * @return string
   */
  public function getCity()
  {
    return $this->get('city')->value;
  }

  /**
   * GET the district.
   * 
   * @return string
   */
  public function getDistrict()
  {
    return $this->get('district')->value;
  }

  /**
   * GET the number.
   * 
   * @return int
   */
  public function getNumber()
  {
    return $this->get('number')->value;
  }

  /**
   * GET the state.
   * 
   * @return string
   */
  public function getState()
  {
    return $this->get('state')->value;
  }

  /**
   * GET the street.
   * 
   * @return string
   */
  public function getStreet()
  {
    return $this->get('street')->value;
  }

  /**
   * GET the user owner of this address.
   * 
   * @return \Drupal\user\entity\User
   * The entity reference.
   */
  public function getUser()
  {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('ID do endereço.'))
      ->setReadOnly(TRUE);

    $fields['state'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Estado'))
    ->setDescription(t('Estado do endereço.'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => -1,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -1,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['city'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Cidade'))
    ->setDescription(t('Cidade do endereço.'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => -1,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -1,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['cep'] = BaseFieldDefinition::create('string')
    ->setLabel(t('CEP'))
    ->setDescription(t('CEP do endereço.'))
    ->setRequired(TRUE)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['district'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Bairro'))
    ->setDescription(t('Bairro do endereço.'))
    ->setRequired(TRUE)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['street'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Rua'))
    ->setDescription(t('Rua do endereço.'))
    ->setRequired(TRUE)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['number'] = BaseFieldDefinition::create('integer')
    ->setLabel(t('Número'))
    ->setDescription(t('Número do endereço.'))
    ->setRequired(TRUE)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['complement'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Complemento'))
    ->setDescription(t('Complemento do endereço.'))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);
    
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Usuário'))
      ->setDescription(t('Usuário dono do endereço.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 3,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ),
        'weight' => 3,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}

<?php

namespace Drupal\trialmachine_mail\Service;

/**
 * Manage the emails from Trial Machine.
 */
class TrialmachineMailService
{
    /**
     * @var $route
     * Default: \Drupal\Core\Mail\MailManager
     */
    protected $mailer;

    /**
     * @var \Drupal\user\Entity\User
     */
    protected $current_user;

    /**
     * Constructor
     * 
     * @param $mailer
     * @param $current_user
     */
    public function __construct($mailer, $current_user)
    {
        $this->mailer = $mailer;
        $this->current_user = $current_user;
    }

    /**
     * Sends the mails using MailSystem.
     * 
     * @param array $params
     * There is a format of associative keys for this param.
     * 'data' => Any data that must be sent to the email template.
     * 'users' => Array of entity Users to send the email. If it is empty, the email will be sent to current_user.
     * @param string $key
     * The key to format params for the email template.
     * @param string $module
     * The module where hook_mail is located
     */
    public function sendMail(array $params = [], $key = 'trialmachine_mail', $module = 'trialmachine_mail')
    {
        $users = empty($params['users']) ? NULL : $params['users'];
        $data = empty($params['data']) ? NULL : $params['data'];

        if (empty($users)) {
            $to_email = $this->current_user->getEmail();
            $langcode = $this->current_user->getPreferredLangCode();
            $result = $this->mailer->mail($module, $key, $to_email, $langcode, $data, NULL, TRUE);
        }

        if (!empty($users)) {
            foreach ($users as $user) {
                $to_email = $user->getEmail();
                $langcode = $user->getPreferredLangcode();
                $result = $this->mailer->mail($module, $key, $to_email, $langcode, $data, NULL, TRUE);
            }
        }

        if (empty($result)) {
            return FALSE;
        }

        return $result['result'];
    }
}
<?php

namespace Drupal\trialmachine_order\Plugin\EntityReferenceSelection;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * 
 */
class OrderSelection extends DefaultSelection
{
    /**
     * @var \Drupal\trialmachine_order\Entity\Order
     */
    private $entity;

    /**
     * Constructs a OrderSelection object.
     */
    public function __construct()
    {
        $this->entity = $this->entityTypeManager->getStorage('trialmachine_order');
    }

    /**
     * 
     */
/*
    protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS')
    {
        $configuration = $this->getConfiguration();

        $query = $this->entity->getQuery();

        var_dump($configuration);
        */
        /*
        if (!empty($configuration['target_bundles'])) {
            $query->condition('type', $configuration['target_bundles'], 'IN');
        }
*/

        /*
        if (isset($match)) {
            $match_condition = $query->orConditionGroup()
                ->condition('title', $match, $match_operator);
            
            $query->condition($match_condition);
        }
        */

        /** Add the Selection handler for system_query_entity_reference_alter(). */
        //$query->addTag('entity_reference');
        //$query->addMetaData('entity_reference_selection_handler', $this);

        /** Add the sort option. */
        /*
        if ($configuration['sort']['field'] !== '_none') {
            $query->sort($configuration['sort']['field'], $configuration['sort']['direction']);
        }
        */
/*
        return $query;
    }
*/

    /**
     * 
     */
/*
    public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0)
    {
        $query = $this->buildEntityQuery($match, $match_operator);
        
        if ($limit > 0) {
            $query->range(0, $limit);
        }

        $result = $query->execute();

        if (empty($result)) {
            return [];
        }

        $options = [];
*/
        /** @var \Drupal\trialmachine_order\Entity\Order $orders */
/*
        $orders = $this->entity->loadMultiple($result);
        
        foreach ($orders as $id => $order) {
            $bundle = $order->bundle();
            $options[$bundle][$id] = $this->entityRepository->getTranslationFromContext($order)->label();
        }

        return $options;
    }
*/
}
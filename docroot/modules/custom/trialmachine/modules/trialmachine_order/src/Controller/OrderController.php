<?php

/**
 * @file
 * Contains \Drupal\trialmachine_order\Controller\OrderController
 */

namespace Drupal\trialmachine_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Datetime\DrupalDateTime;


class OrderController extends ControllerBase
{
    /**
     * Action for route trialmachine_order.waiting_orders
     */
    public function waitingOrders(Request $request)
    {
        /** @var \Drupal\trialmachine_order\Service\OrderService */
        $orderService = \Drupal::service('trialmachine_order.order');

        /** @var \Drupal\trialmachine_order\Entity\Order $orders */
        $orders = \Drupal::entityTypeManager()->getStorage('trialmachine_order')
            ->loadMultiple($orderService->searchUserOpenOrders());

        /**
         * Data array to the view
         */
        $data = [
            'orders' => $orders,
        ];

        return [
            '#theme' => 'aguarde_seu_pedido',
            '#data' => $data,
        ];
    }

    /**
     * Action for route trialmachine_order.rating_orders
     */
    public function ratingOrders(Request $request)
    {
        /** @var \Drupal\trialmachine_order\Service\OrderService */
        $orderService = \Drupal::service('trialmachine_order.order');

        /** @var \Drupal\trialmachine_order\Entity\Order $orders */
        $orders = \Drupal::entityTypeManager()->getStorage('trialmachine_order')
            ->loadMultiple($orderService->searchUserDeliveredOrders());
        
        /**
         * Data array to the view
         */
        $data = [
            'orders' => $orders,
        ];

        return [
            '#theme' => 'avalie_seus_pedidos',
            '#data' => $data,
        ];
    }

    public function alreadyOrdered(Request $request)
    {
        /** @var \Drupal\trialmachine_order\Entity\Order $order */
        $order = $request->attributes->get('trialmachine_order');

        /**
         * Data array to the view
         */
        $data = [
            'order' => $order,
        ];

        return [
            '#theme' => 'voce_ja_participou',
            '#data' => $data,
        ];
    }

    /**
     * Resend emails to customers with the tracking code.
     */
    public function resendEmail(Request $request)
    {
        $order = $request->attributes->get('trialmachine_order');

        $params = [
            'data' => ['order' => $order],
            'users' => [$order->getCustomer()],
        ];

        $mailManager = \Drupal::service('trialmachine_mail.mail');

        $to_email = $order->getCustomer()->getEmail();

        if ($mailManager->sendMail($params, 'tracking_code_ok') === TRUE) {
            \Drupal::messenger()->addMessage("Email enviado com sucesso para: {$to_email}");
        } else {
            \Drupal::messenger()->addError("Ocorreu um problema ao enviar o email para: {$to_email}");
        }

        $response = new RedirectResponse($order->toUrl('collection')->toString());

        return $response->send();
    }
}
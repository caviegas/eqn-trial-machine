<?php

namespace Drupal\trialmachine_order\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class AutocompleteController extends ControllerBase
{
    /**
     * Handler for the customer_email autocomplete request.
     * @return JsonResponse
     */
    public function customerEmail(Request $request)
    {
        $results = [];
        $input = $request->query->get('q'); //TODO: Verificar o 'q'

        /** Get the typed string from the URL, if it exists. */
        //TODO: Ver se é necessário
        if (!$input) {
            return new JsonResponse($results);
        }

        $input = Xss::filter($input);

        /** @var \Drupal\Core\Entity\User $userStorage */
        $userStorage = \Drupal::entityTypeManager()->getStorage('user');

        $query = $userStorage->getQuery()
            ->condition('mail', $input, 'CONTAINS')
            ->groupBy('uid')
            ->sort('mail', 'ASC');

        $ids = $query->execute();

        $users = $ids ? $userStorage->loadMultiple($ids) : [];

        foreach ($users as $user) {
            $results[] = [
                'value' => "{$user->getEmail()} ({$user->id()})",
                'label' => "{$user->getEmail()} <small>({$user->id()})</small>"
            ];
        }

        return new JsonResponse($results);
    }
}
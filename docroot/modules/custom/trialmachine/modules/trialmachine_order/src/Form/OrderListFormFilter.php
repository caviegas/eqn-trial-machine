<?php

namespace Drupal\trialmachine_order\Form;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

//use Drupal\Core\Ajax\AjaxResponse;
//use Drupal\Core\Ajax\HtmlCommand;
//use Drupal\Core\Messenger\MessengerInterface;
//use Drupal\Core\Datetime\DrupalDateTime;

//use Drupal\Core\Url;
//use Drupal\Core\Messenger;
//use \SoapClient;
//use \SoapVar;
//use \SoapHeader;
//use \SoapFault;
//use Symfony\Component\HttpFoundation\RedirectResponse;

//use Drupal\Core\Database\Connection;

class OrderListFormFilter extends FormBase
{
    /**
     * @var \Drupal\trialmachine_order\Service\OrderService
     */
    private $orderService;

    /**
     * Constructs a OrderListFormBuilderFilter object.
     */
    public function __construct()
    {
        $this->orderService = \Drupal::service('trialmachine_order.order');
    }

    /**
     * {@inheritdoc}
     */
    public static function createInstance(ContainerInterface $container) {
        return new static(
            $container->get('order.service')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return "form_order_filter";
    }

    /**
     * 
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $request = \Drupal::request();

        $form['filter'] = [
            '#type' => 'container',
            '#attributes' => [
                'class' => ['form--inline', 'clearfix'],
            ],
        ];

        /** Field - Campaign (entities) */
        if (!empty($request->get('campaign_id'))) {
            $campaign_ids = [];

            if (is_array($request->get('campaign_id'))) {
                foreach ($request->get('campaign_id') as $campaign) {
                    $campaign_ids[] = $campaign['target_id'];
                }

                /** @var \Drupal\trialmachine_campaign\Entity\Campaign $campaigns */
                $campaigns = \Drupal::entityTypeManager()->getStorage('trialmachine_campaign')->loadMultiple($campaign_ids);
            }
        }

        $form['filter']['campaign_id'] = [
            '#type' => 'entity_autocomplete',
            '#target_type' => 'trialmachine_campaign',
            '#title' => $this->t('Campanha'),
            '#description' => $this->t('Separe os nomes das campanhas com vírgula para filtrar mais de uma'),
            '#default_value' => empty($campaigns) ? NULL : $campaigns,
            '#tags' => TRUE,
            '#weight' => '0',
        ];

        /** Field - Customer Emails */
        if (!empty($request->get('user_id'))) {
            $user_ids = [];

            if (is_array($request->get('user_id'))) {
                foreach ($request->get('user_id') as $user) {
                    $user_ids[] = $user['target_id'];
                }

                /** @var \Drupal\Core\Entity\User $users */
                $users = \Drupal::entityTypeManager()->getStorage('user')->loadMultiple($user_ids);

                $user_filter = [];

                foreach ($users as $user) {
                    $user_filter[] = "{$user->getEmail()} ({$user->id()})";
                }

                $user_filter = implode(', ', $user_filter);
            }
        }

        $form['filter']['user_id'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Solicitante'),
            '#autocomplete_route_name' => 'autocomplete.trialmachine_order.customer_email',
            '#description' => $this->t('Separe os emails com vírgula para filtrar mais de um'),
            '#default_value' => empty($user_filter) ? NULL : $user_filter,
        ];

        /** Field - Status */
        $status_list = $this->orderService->getStatusKeyed();

        $form['filter']['status'] = [
            '#type' => 'select',
            '#title' => $this->t('Situação'),
            '#options' => $status_list,
            '#sort_options' => true,
            '#empty_option' => $this->t('- Nenhum -'),
            '#empty_value' => '',
            '#default_value' => $request->get('status') ?? 0,
        ];

        /** Field - Tracking Code */
        $form['filter']['tracking_code'] = [
            '#type' => 'select',
            '#title' => $this->t('Código de Rastreio?'),
            '#options' => [
                'yes' => $this->t('Sim'),
                'no' => $this->t('Não'),
            ],
            '#sort_options' => true,
            '#empty_option' => $this->t('- Ambos -'),
            '#empty_value' => '',
            '#default_value' => !empty($request->get('tracking_code')) ? $request->get('tracking_code') : ''
        ];

        /** Field - Order Date (Created) */
        $form['filter']['created'] = [
            '#type' => 'date',
            '#title' => $this->t('DT Solicitação'),
            '#default_value' => !empty($request->get('created')) ? $request->get('created') : ''
        ];

        /** Form Actions */
        $form['actions']['wrapper'] = [
            '#type' => 'container',
            '#attributes' => ['class' => ['form-item']],
        ];

        $form['actions']['wrapper']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Filtrar'),
        ];

        if ($request->getQueryString()) {
            $form['actions']['wrapper']['reset'] = [
                '#type' => 'submit',
                '#value' => $this->t('Resetar'),
                '#submit' => ['::resetForm'],
            ];
        }

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $query = [];

        if (!empty($form_state->getValue('campaign_id'))) {
            $query['campaign_id'] = $form_state->getValue('campaign_id');
        }

        if (!empty($form_state->getValue('user_id'))) {
            $user_ids = array_map('trim', explode(',', $form_state->getValue('user_id')));

            foreach ($user_ids as $user_id) {
                $query['user_id'][] = [
                    'target_id' => EntityAutocomplete::extractEntityIdFromAutocompleteInput($user_id)
                ];
            }
        }

        if (!empty($form_state->getValue('status'))) {
            $query['status'] = $form_state->getValue('status');
        }

        if (!empty($form_state->getValue('tracking_code'))) {
            $query['tracking_code'] = $form_state->getValue('tracking_code');
        }

        if (!empty($form_state->getValue('created'))) {
            $query['created'] = $form_state->getValue('created');
        }

        $form_state->setRedirect('entity.trialmachine_order.collection', $query);
    }

    /**
     * {@inheritdoc}
     */
    public function resetForm(array $form, FormStateInterface $form_state)
    {
        $form_state->setRedirect('entity.trialmachine_order.collection');
    }
}
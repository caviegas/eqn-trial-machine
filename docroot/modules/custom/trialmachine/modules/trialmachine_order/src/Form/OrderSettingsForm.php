<?php
/**
 * @file
 * Contains \Drupal\trialmachine_order\Form\TermSettingsForm.
 */

namespace Drupal\trialmachine_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContentEntityExampleSettingsForm.
 *
 * @package Drupal\trialmachine_order\Form
 *
 * @ingroup product
 */
class OrderSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'trialmachine_order_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['trialmachine_order_settings']['#markup'] = 'Settings form for order Term. Manage field settings here.';
    return $form;
  }

}

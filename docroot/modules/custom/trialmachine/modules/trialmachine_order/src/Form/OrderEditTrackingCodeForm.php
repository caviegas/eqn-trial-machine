<?php

namespace Drupal\trialmachine_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for updating the order's tracking code.
 */
class OrderEditTrackingCodeForm extends FormBase
{
    /**
     * The current order.
     * 
     * @var \Drupal\trialmachine_order\Entity\Order
     */
    protected $order;

    /**
     * 
     */
    protected $mailManager;

    /**
     * Constructs a new OrderEditTrackingCodeForm object.
     * 
     * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
     *   The current route match.
     */
    public function __construct(CurrentRouteMatch $current_route_match)
    {
        $this->order = $current_route_match->getParameter('trialmachine_order');
        $this->mailManager = \Drupal::service('trialmachine_mail.mail');
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('current_route_match')
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'trialmachine_order_edit_tracking_code_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['tracking_code'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Código de Rastreio'),
            '#required' => TRUE,
            '#maxlength' => 180,
            '#default_value' => $this->order->getTrackingCode(),
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => t('Salvar'),
        ];
        
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        /** @var \Drupal\correios_web_service\Service\CorreiosTracking $correiosTracking */
        $correiosTracking = \Drupal::service('correios_web_service.tracking');

        $tracking_code = $form_state->getValue('tracking_code');

        $correios_object = $correiosTracking->checkObjectExists($tracking_code);

        if ($correios_object->ok === false) {
            \Drupal::logger('correios_web_service')->error("Correios - {$correios_object->error} ({$tracking_code})");
            $form_state->setErrorByName('tracking_code', $this->t("Correios Web Service - {$correios_object->error}"));
            //! Comentado para caso precise ser revertido mais tarde.
            //$form_state->setErrorByName('tracking_code', $this->t('Código de rastreamento inválido.'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        /** Only way found to get the original value for comparison. */
        $old_tracking_code = (string) $this->order->getTrackingCode();
        
        $new_tracking_code = (string) $form_state->getValue('tracking_code');

        $this->order->set('tracking_code', $new_tracking_code);

        $this->order->save();

        \Drupal::messenger()->addMessage("Código de rastreio {$new_tracking_code} salvo com sucesso.");

        /** SEND EMAIL LOGIC */
        $email_send = FALSE;

        if (empty($old_tracking_code) && !empty($new_tracking_code)) {
            $email_send = TRUE;
            $email_key = 'tracking_code_ok';
        }

        if (!empty($old_tracking_code) && $old_tracking_code !== $new_tracking_code) {
            $email_send = TRUE;
            $email_key = 'tracking_code_update';
        }

        if ($email_send === TRUE) {
            $to_email = $this->order->getCustomer()->getEmail();

            $params = [
                'data' => ['order' => $this->order],
                'users' => [$this->order->getCustomer()],
            ];

            if ($this->mailManager->sendMail($params, $email_key) === TRUE) {
                \Drupal::messenger()->addMessage("Email enviado com sucesso para: {$to_email}");
            } else {
                \Drupal::messenger()->addError("Ocorreu um problema ao enviar o email para: {$to_email}");
            }
        }

        $form_state->setRedirectUrl($this->order->toUrl('collection'));
  }
}
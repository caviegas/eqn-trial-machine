<?php

/**
 * @file providing axuliar methods for \Drupal\trialmachine_order\Entity\Order
 * Contains \Drupal\trialmachine_order\Service\OrderService
 */

namespace Drupal\trialmachine_order\Service;

use Drupal\trialmachine_order\Entity\Order;
use Symfony\Component\HttpFoundation\RedirectResponse;

class OrderService
{
    /**
     * Array with all statuses from Order entity.
     * \Drupal\trialmachine_order\Entity\Order
     * 
     * @var array
     */
    private $status = [
        'aguardando' => [
            'id' => 1,
            'description' => 'Aguardando envio'
        ],
        'caminho' => [
            'id' => 2,
            'description' => 'A caminho'
        ],
        'entregue' => [
            'id' => 3,
            'description' => 'Não avaliado'
        ],
        'avaliado' => [
            'id' => 4,
            'description' => 'Avaliado'
        ],
        'problema' => [
            'id' => 5,
            'description' => 'Problema na entrega'
        ],
        'cancelado' => [
            'id' => 6,
            'description' => 'Entrega cancelada'
        ],
        'congelado' => [
            'id' => 7,
            'description' => 'Pedido congelado'
        ],
    ];

    private $session;

    public function __construct() {
        $this->session = \Drupal::service('session');
    }

    /**
     * Get Status.
     * 
     * @param int $return_type
     * Value is an integer number to display how it will returns the data from status.
     * 
     * @return array
     * It will return the status without modifications if $return_type is 0.
     * It will return the status with keys indexed numerically if $return_type is 1.
     */
    public function getStatus($return_type = 0)
    {
        if ($return_type === 1) {
            return array_values($this->status);
        }

        return $this->status;
    }

    /**
     * Get the data from Status by its alias.
     * 
     * @param string $alias
     * Must be a string that is the alias of the status. It is the key of the associative array.
     * 
     * @param int $return_type
     * Must be a value between {0,1,2} representing what should be the return like:
     * - 0: Return both values from keys "id" and "description"
     * - 1: Return the value from key "id"
     * - 2: Return the value from key "description"
     * 
     * @return mixed
     * It will return FALSE if there isn't a status with the alias informed.
     * It will return an array if $return_type is 0
     * It will return an int ID if $return_type is 1
     * It will return a string Description if $return_type is 2
     */
    public function getStatusByAlias($alias, $return_type = 0)
    {
        if (empty($this->status[$alias])) {
            return false;
        }

        if ($return_type === 1) {
            return $this->status[$alias]['id'];
        }

        if ($return_type === 2) {
            return $this->status[$alias]['description'];
        }

        return $this->status[$alias];
    }

    /**
     * Get the Description from Status by its ID.
     * 
     * @param int $id
     * ID value for the Status.
     * 
     * @return string
     * The Description from the Status.
     */
    public function getStatusById($id)
    {
        if (empty($id)) {
            return false;
        }

        $status = $this->getStatusKeyed();

        return $status[(int)$id];
    }

    /**
     * Get all Status with array keys as ID and array values as Description.
     * 
     * @return array
     * Returns an array keyed with ID -> Description.
     */
    public function getStatusKeyed()
    {
        $status = [];

        foreach ($this->status as $data) {
            $status[$data['id']] = $data['description'];
        }

        return $status;
    }

    /**
     * Search for all opened orders of a given user.
     * Status = aguardando, caminho.
     * 
     * @param int $user_id
     * It will get the current user id if it is NULL.
     * 
     * @return array \Drupal\trialmachine_order\Entity\Order
     * Array with IDs from Orders to load the entities.
     */
    public function searchUserOpenOrders($user_id = NULL)
    {
        if (empty($user_id)) {
            /** @var \Drupal\user\Entity\User $user_id */
            $user_id = \Drupal::currentUser()->id();
        }

        return \Drupal::entityTypeManager()->getStorage('trialmachine_order')
            ->getQuery()
            ->condition('status', [
                $this->getStatusByAlias('aguardando', 1),
                $this->getStatusByAlias('caminho', 1)
            ], 'IN')
            ->condition('user_id', $user_id)
            ->execute();
    }

    /**
     * Search for all delivered orders of a given user.
     * Status = não avaliado.
     * 
     * @param int $user_id
     * It will get the current user id if it is NULL.
     * 
     * @return array \Drupal\trialmachine_order\Entity\Order
     * Array with IDs from Orders to load the entities.
     */
    public function searchUserDeliveredOrders($user_id = NULL)
    {
        if (empty($user_id)) {
            /** @var \Drupal\user\Entity\User $user_id */
            $user_id = \Drupal::currentUser()->id();
        }

        return \Drupal::entityTypeManager()->getStorage('trialmachine_order')
            ->getQuery()
            ->condition('status', $this->getStatusByAlias('entregue', 1))
            ->condition('user_id', $user_id)
            ->execute();
    }

    /**
     * Search for all orders of a given user from the specified campaign.
     * 
     * @param int $campaign_id
     * 
     * @param int $user_id
     * It will get the current user id if it is NULL.
     * 
     * @return array \Drupal\trialmachine_order\Entity\Order
     * Array with IDs from Orders to load the entities.
     */
    public function searchUserOrderFromCampaign($campaign_id, $user_id = NULL)
    {
        if (empty($user_id)) {
            /** @var \Drupal\user\Entity\User $user_id */
            $user_id = \Drupal::currentUser()->id();
        }

        return \Drupal::entityTypeManager()->getStorage('trialmachine_order')
            ->getQuery()
            ->condition('campaign_id', $campaign_id)
            ->condition('user_id', $user_id)
            ->condition('status', $this->getStatusByAlias('congelado', 1), '!=')
            ->execute();
    }

    // * function CreateFrozenOrder
    // * function CheckIfUserHasFrozenOrder
    public function createFrozenOrder($user_id = NULL) {

        if (!$this->session->has('selected_campaign')) {
            return false;
        } 

        $user_id = $user_id == null ? \Drupal::currentUser()->id() : $user_id;
        $campaign = $this->session->get('selected_campaign');

        // * Check If user has any frozen order already, then delete and return product quantity
        if ($this->checkFrozenOrders($user_id)) {
            // Create a frozen order
            $orderData = [
                'status' => 7,
                'user_id' => $user_id,
                'campaign_id' => $campaign->id()
            ];

            $campaign->takeOneFromQuantity();

            $order = Order::create($orderData);
            $order->save();
            return true;
        }
    }

    private function checkFrozenOrders($user_id) {

        $orderId = \Drupal::entityQuery('trialmachine_order')
            ->condition('user_id', $user_id)
            ->condition('status', [
                $this->getStatusByAlias('congelado', 1),
            ])
            ->execute();
       
        if (!empty($orderId)) {
            // Carrega o Pedido congelado encontrado encontrado
            $order = Order::load(array_shift($orderId));
            
            // Devolve o produto à campanha
            $campaign = $order->campaign_id->entity;
            $campaign->addToQuantity();

            // Deleta o pedido
            $order->delete();
        }

        return true;
    }

    public function redirectToOrder($user_id) {
        $orderId = \Drupal::entityQuery('trialmachine_order')
        ->condition('user_id', $user_id)
        ->condition('status', [
            $this->getStatusByAlias('congelado', 1),
        ])
        ->execute();

        if (!empty($orderId)) {
            // Carrega o Pedido congelado encontrado encontrado
            $order = Order::load(array_shift($orderId));
            
            $campaign = $order->campaign_id->entity;
            $this->session->migrate();
            $this->session->set('selected_campaign', $campaign);
            $this->session->save();

            $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.delivery')->toString();
            $response = new RedirectResponse($path);

            return $response->send();
        }
    }

    public function unfreezeOldFrozenOnes() {
        $currentTime = \Drupal::time()->getCurrentTime();
        $dayAgo = strtotime('-1 day', $currentTime);

        $orderIds = \Drupal::entityQuery('trialmachine_order')
        ->condition('status', 7)
        ->condition('created', $dayAgo, '<')
        ->execute();

        // 86600 = A day

        // Se retornar alguma coisa, limpe-os
        if (!empty($orderIds)) {
            $ordersArr = Order::loadMultiple($orderIds);

            foreach ($ordersArr as $order) {
                // Devolve o produto à campanha
                $campaign = $order->campaign_id->entity;
                $campaign->addToQuantity();

                $order->delete();
            }
        } 
    }  
}
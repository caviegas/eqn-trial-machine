<?php
/**
 * @file
 * Contains \Drupal\trialmachine_order\Entity\Order.
 */

namespace Drupal\trialmachine_order\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup order
 *
 *
 * @ContentEntityType(
 *   id = "trialmachine_order",
 *   label = @Translation("Order entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\trialmachine_order\Entity\Controller\OrderListBuilder",
 *     "form" = {
 *       "add" = "Drupal\trialmachine_order\Form\OrderForm",
 *       "edit" = "Drupal\trialmachine_order\Form\OrderForm",
 *       "delete" = "Drupal\trialmachine_order\Form\OrderDeleteForm",
 *     },
 *     "access" = "Drupal\trialmachine_order\OrderAccessControlHandler",
 *   },
 *   list_cache_contexts = { "user" },
 *   base_table = "trialmachine_order",
 *   admin_permission = "administer trialmachine_order entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "created" = "created",
 *     "changed" = "changed",
 *    },
 *   links = {
 *     "canonical" = "/pedido/{trialmachine_order}",
 *     "collection" = "/trialmachine/pedido/listar",
 *     "delete-form" = "/trialmachine/pedido/{trialmachine_order}/deletar",
 *     "edit-form" = "/trialmachine/pedido/{trialmachine_order}/editar", 
 *     "edit-tracking-code" = "/trialmachine/pedido/{trialmachine_order}/rastreio",
 *     "resend-email-tracking" = "/trialmachine/pedido/{trialmachine_order}/reenviar-email-rastreio",
 *   },
 *   field_ui_base_route = "entity.order.settings",
 *   common_reference_target = TRUE,
 * )
 */
class Order extends ContentEntityBase
{
  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  // public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
  //   parent::preCreate($storage_controller, $values);
  //   // Default author to current user.
  //   $values += array(
  //     'author_id' => \Drupal::currentUser()->id(),
  //   );
  // }

  /**
   * GET the address from the customer.
   * 
   * @return \Drupal\trialmachine_address\Entity\Address
   * The address entity.
   */
  public function getAddress()
  {
    return $this->get('address_id')->entity;
  }

  /**
   * GET the campaign.
   * 
   * @return \Drupal\trialmachine_campaign\Entity\Campaign
   * The campaign entity.
   */
  public function getCampaign()
  {
    return $this->get('campaign_id')->entity;
  }

  /**
   * GET the customer user.
   * 
   * @return \Drupal\user\Entity\User
   * The user entity.
   */
  public function getCustomer()
  {
    return $this->get('user_id')->entity;
  }

  /**
   * GET the coupon available for the customer of this order.
   * There must be only one coupon available per user per campaign, and order.
   * 
   * @return \Drupal\trialmachine_coupon\Entity\Coupon
   * The coupon entity.
   */
  public function getCustomerCoupon()
  {
    // @var array \Drupal\trialmachine_coupon\Entity\Coupon $coupon
    $coupon = \Drupal::entityTypeManager()->getStorage('trialmachine_coupon')
      ->loadByProperties([
        'user_id' => $this->getCustomer()->get('uid')->value,
        'campaign_id' => $this->getCampaign()->get('id')->value,
      ]);

    return reset($coupon);
  }

  /**
   * GET the delivery date.
   * 
   * @return datetime
   * The datetime object from Correios Web Service.
   */
  public function getDeliveryDate()
  {
    return $this->get('delivery_date')->value;
  }
  
  /**
   * GET the event code.
   * 
   * @return string
   * The last event code from Correios Web Service.
   */
  public function getEventCode()
  {
    return $this->get('event_code')->value;
  }

  /**
   * GET the event message.
   * 
   * @return string
   * The last event message from Correios Web Service.
   */
  public function getEventMessage()
  {
    return $this->get('event_message')->value;
  }

  /**
   * GET the ID.
   * 
   * @return int
   * The ID of the order.
   */
  public function getId()
  {
    return $this->get('id')->value;
  }

  /**
   * GET the status.
   * 
   * @return int
   * The number code of order's status.
   */
  public function getStatus()
  {
    return (int) $this->get('status')->value;
  }

  /**
   * GET the status name.
   * 
   * @return string
   * The name of the status from \Drupal\trialmachine_order\Service\OrderService
   */
  public function getStatusName()
  {
    // @var \Drupal\trialmachine_order\Service\OrderService $order_service
    $order_service = \Drupal::service('trialmachine_order.order');
    return $order_service->getStatusById($this->getStatus());
  }

  /**
   * GET the tracking code.
   * 
   * @return string
   * The tracking code from Correios Web Service.
   */
  public function getTrackingCode()
  {
    return $this->get('tracking_code')->value;
  }

  /**
   * Check if the Status is equal to the status alias.
   * 
   * @return boolean
   * FALSE if it is different to the status tested by its alias, TRUE if it is equal.
   */
  public function checkStatusByAlias($alias)
  {
    // @var \Drupal\trialmachine_order\Service\OrderService $order_service
    $order_service = \Drupal::service('trialmachine_order.order');

    if ($this->getStatus() === $order_service->getStatusByAlias($alias, 1)) {
      return true;
    }

    return false;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Order entity.'))
      ->setReadOnly(TRUE);

    /** \Drupal\user\Entity\User */
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Customer'))
      ->setDescription(t('The customer.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\trialmachine_order\Entity\Order::getCurrentUserId')
      ->setDisplayConfigurable('view', TRUE);

    /** \Drupal\trialmachine_campaign\Entity\Campaign */
    $fields['campaign_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Campaign'))
      ->setDescription(t('The campaign.'))
      ->setSetting('target_type', 'trialmachine_campaign')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'campaign',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    /** \Drupal\trialmachine_address\Entity\Address */
    $fields['address_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Delivery Address'))
      ->setDescription(t('The address for the delivery.'))
      ->setSetting('target_type', 'trialmachine_address')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'address',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    /**
     * Status
     * It can have more than one status type as an integer number.
     * 1 = Aguardando envio
     * 2 = A caminho
     * 3 = Não avaliado
     * 4 = Avaliado
     * 5 = Problema na entrega
     * 6 = Entrega cancelada
     * 7 = Pedido congelado
     */
    $fields['status'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of the order, it will be updated whenever "Correios Web Service" changes the object status.'))
      ->setSetting('unsigned', TRUE)
      // Default to 1, so that the first status is "waiting".
      ->setDefaultValue(1)
      ->setDisplayConfigurable('view', TRUE);

    /** Trackig Code from "Correios Web Service" */
    $fields['tracking_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Tracking Code'))
      ->setDescription(t('The tracking code of the product sent to the customer.'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    /** Delivery Date from "Correios Web Service" */
    $fields['delivery_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Delivery Date'))
      ->setDescription(t('The date when the product is set to be delivered to the customer.'))
      ->setDisplayConfigurable('view', TRUE);

    /** Event Code from "Correios Web Service" */
    $fields['event_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Event Code'))
      ->setDescription(t('The event code of the most recent product status from "Correios Web Service".'))
      ->setSetting('max_length', 128)
      ->setDisplayConfigurable('view', TRUE);

    /** Event Message from "Correios Web Service" */
    $fields['event_message'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Event Message'))
      ->setDescription(t('The event message of the most recent product status from "Correios Web Service".'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Default value callback for 'user_id' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId()
  {
    return [\Drupal::currentUser()->id()];
  }

}

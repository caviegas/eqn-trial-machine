<?php

/**
 * @file
 * Contains \Drupal\trialmachine_order\Entity\Controller\TermListBuilder.
 */

namespace Drupal\trialmachine_order\Entity\Controller;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for trialmachine_order entity.
 *
 * @ingroup order
 */
class OrderListBuilder extends EntityListBuilder {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * Constructs a new OrderListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type term.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    UrlGeneratorInterface $url_generator,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct($entity_type, $storage);

    $this->urlGenerator = $url_generator;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type)
  {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('url_generator'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the trialmachine_order list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader()
  {
    $header['id'] = [
      'data' => $this->t('ID'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['created'] = [
      'data' => $this->t('DT Solicitação'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['customer'] = [
      'data' => $this->t('Solicitante'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['campaign'] = [
      'data' => $this->t('Campanha'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['address'] = [
      'data' => $this->t('Endereço'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['status'] = [
      'data' => $this->t('Situação'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['tracking_code'] = [
      'data' => $this->t('Código de Rastreio'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['delivery_date'] = [
      'data' => $this->t('DT para Entrega'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\trialmachine_order\Entity\Order $entity */

    $delivery_date = $entity->getDeliveryDate();

    $row['id'] = $entity->getId();
    $row['created'] = $this->dateFormatter->format($entity->get('created')->value, 'short');
    $row['customer'] = empty($entity->getCustomer()) ? NULL : $entity->getCustomer()->getEmail();
    $row['campaign'] = empty($entity->getCampaign()) ? NULL : $entity->getCampaign()->get('title')->value;
    $row['address'] = $entity->get('address_id')->value;
    $row['status'] = $entity->getStatusName();
    $row['tracking_code'] = $entity->getTrackingCode();
    $row['delivery_date'] = empty($delivery_date) ? NULL : $this->dateFormatter->format(strtotime($delivery_date), 'short');
    
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity)
  {
    //! Comentado para remover as operações "Edit" e "Delete" por enquanto.
    //$operations = parent::getDefaultOperations($entity);
    $operations = [];

    /** @var \Drupal\trialmachine_order\Entity\Order $entity */

    if ($entity->access('edit')) {
      $operations['edit_tracking_code'] = [
        'title' => $this->t('Código de rastreio'),
        'weight' => 5,
        'url' => $this->ensureDestination($entity->toUrl('edit-tracking-code')),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ];
    }

    if (!empty($entity->getTrackingcode())) {
      $operations['resend_email_tracking'] = [
        'title' => $this->t('Reenviar email'),
        'weight' => 10,
        'url' => $this->ensureDestination($entity->toUrl('resend-email-tracking')),
        'class' => [RESPONSIVE_PRIORITY_MEDIUM],
      ];
    }

    return $operations;
  }

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\trialmachine_order\Form\OrderListFormFilter');
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * 
   */
  protected function getEntityIds()
  {
    $query = \Drupal::entityQuery($this->entityTypeId);
    $request = \Drupal::request();

    /** @var \Drupal\trialmachine_campaign\Entity\Campaign */
    if (!empty($request->get('campaign_id'))) {
      $campaign_ids = [];

      foreach ($request->get('campaign_id') as $campaign) {
        $campaign_ids[] = $campaign['target_id'];
      }

      $query->condition('campaign_id', $campaign_ids, 'IN');
    }

    if (!empty($request->get('user_id'))) {
      $user_ids = [];

      foreach ($request->get('user_id') as $user) {
        $user_ids[] = $user['target_id'];
      }

      $query->condition('user_id', $user_ids, 'IN');
    }

    if (!empty($request->get('status'))) {
      $query->condition('status', $request->get('status'));
    }

    if (!empty($request->get('tracking_code'))) {
      switch ($request->get('tracking_code')) {
        case 'yes':
          $query->condition('tracking_code', '', '<>');
          break;
        case 'no':
          $query->condition('tracking_code', '');
          break;
      }
    }

    /** Search within 24 hour range. */
    if (!empty($request->get('created'))) {
      $created_start = new DrupalDateTime($request->get('created'));
      $created_end = new DrupalDateTime($request->get('created') . ' 23:59:59');

      $query->condition('created', [
        $created_start->getTimestamp(),
        $created_end->getTimestamp(),
      ], 'BETWEEN');
    }

    return $query->execute();
  }

}

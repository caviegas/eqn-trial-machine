<?php

namespace Drupal\trialmachine_campaign\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "checkout_credentials_block",
 *   admin_label = "Checkout Credentials Block",
 *   category = "Meeg blocks",
 * )
 */
class CheckoutCredentialsBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
        
    $data['path'] = \Drupal::service('path.current')->getPath();

    return [
      '#theme' => 'checkout_credentials_block',
      '#data' => $data,
    ];
  }
}

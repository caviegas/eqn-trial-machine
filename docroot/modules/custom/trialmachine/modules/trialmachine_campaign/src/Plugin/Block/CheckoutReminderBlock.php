<?php

namespace Drupal\trialmachine_campaign\Plugin\Block;

use Drupal\trialmachine_campaign\Entity\Campaign;
use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "checkout_reminder_block",
 *   admin_label = "Checkout Reminder Block",
 *   category = "Campanha",
 * )
 */
class CheckoutReminderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $session = \Drupal::service('session');
    $campaign = $session->get('selected_campaign');

    $data['campaign_name'] = $campaign->title->value;
    $data['product_name'] = $campaign->product_id->entity->title->value;
    if (isset($campaign->product_id->entity->weight)) {
      $data['product_weight'] = $campaign->product_id->entity->weight->value;
    }
    if (isset($campaign->product_id->entity->image)) {
     $data['product_image'] = file_create_url($campaign->product_id->entity->image->entity->getFileUri());
    }

    return [
      '#theme' => 'checkout_reminder_block',
      '#data' => $data,
    ];
  }

  public function getCacheMaxAge() {
    return 0;
  }
}

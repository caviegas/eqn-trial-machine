<?php

namespace Drupal\trialmachine_campaign\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\trialmachine_campaign\Entity\Campaign;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Query\QueryInterface;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\meeg_content\Controller\UserController;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "campaign_feed",
 *   admin_label = "Campaign feed",
 *   category = "Campanha",
 * )
 */
class CampaignBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    \Drupal::service('page_cache_kill_switch')->trigger();

    // Limpar Pedidos há muito tempo congelados.
    \Drupal::service('trialmachine_order.order')->unfreezeOldFrozenOnes();

    /** @var \Drupal\trialmachine_campaign\Entity\Campaign $campaigns */
    $campaigns = Campaign::loadMultiple();

    /** TAGS - INTERESTS - LOGIC */
    $interests = NULL;

    /** @var \Drupal\meeg_content\Controller\UserController $userController */
    $userController = new UserController;

    if ($userController->hasBasicData() === TRUE) {
      $session_attributes = $userController->getUserAttr();

      if ($userController->hasTags($session_attributes) === TRUE) {
        $interests = $userController->getTags($session_attributes['nm_tags'], 'consumidor');
      }
    }

    /** SOLDOFF CAMPAIGNS LOGIC */
    //* Priority 1 -> It should always show last in the campaigns order.
    $soldoff_campaigns = [];

    foreach ($campaigns as $key => $campaign) {
      $campaign->soldoff = FALSE;

      if ($campaign->getQuantity() <= 0) {
        $campaign->soldoff = TRUE;
        $soldoff_campaigns[] = $campaign;
        unset($campaigns[$key]);
      }
    }

    /** ARCHIVED CAMPAIGNS LOGIC */
    //* Priority 2 -> It should always show before Priority 1 in the campaigns order.
    $archived_campaigns = [];

    foreach ($campaigns as $key => $campaign) {
      $end_date = new DrupalDateTime($campaign->getEndDate());
      $now = new DrupalDateTime();
      $campaign->archived = FALSE;

      if ($end_date->getTimestamp() < $now->getTimestamp()) {
        $campaign->archived = TRUE;
        $archived_campaigns[] = $campaign;
        unset($campaigns[$key]);
      }
    }

    /** FORYOU CAMPAIGNS LOGIC */
    //* Priority 3 -> It should always come first in the campaigns order.
    $foryou_campaigns = [];

    if (!empty($interests->labels)) {

      foreach ($campaigns as $key => $campaign) {
        $author = $campaign->get('user_id')->entity;
        $campaign->foryou = FALSE;

        foreach ($interests->labels as $brand) {

          if ($brand == $author->get('name')->value) {
            $campaign->foryou = TRUE;
            $foryou_campaigns[] = $campaign;
            unset($campaigns[$key]);
            break 1;
          }
        }
      }
    }

    /** NEW CAMPAIGNS LOGIC */
    //*Priority 4 -> It should always come after FORYOU, but before "old campaigns" in the campaigns order.
    $new_campaigns = [];

    foreach ($campaigns as $key => $campaign) {
      $start_date = new DrupalDateTime($campaign->getStartDate());
      $now = DrupalDateTime::createFromDateTime(\date_create());
      $campaign->new = FALSE;

      if (
        $now->getTimestamp() >= $start_date->getTimestamp() &&
        $now->getTimestamp() <= $start_date->modify('+7 days')->getTimestamp()
      ) {
        $campaign->new = TRUE;
        $new_campaigns[] = $campaign;
        unset($campaigns[$key]);
      }
    }

    //* The order for this merge is important, if you change the order they will appear differently on the view.
    $data_campaigns = array_merge($foryou_campaigns, $new_campaigns, $campaigns, $archived_campaigns, $soldoff_campaigns);

    $data['campaigns'] = $data_campaigns;
    $data['interests'] = $interests;

    return [
      '#theme' => 'campaign_block',
      '#data' => $data,
    ];
  }

}

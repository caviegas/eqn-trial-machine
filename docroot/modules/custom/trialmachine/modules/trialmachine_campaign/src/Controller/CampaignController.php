<?php
/**
 * @file
 * Contains \Drupal\trialmachine_campaign\Controller\CampaignController
 */

 namespace Drupal\trialmachine_campaign\Controller;

 use Drupal\Core\Controller\ControllerBase;
 use Drupal\Core\Datetime\DrupalDateTime;
 use Drupal\trialmachine_campaign\Entity\Campaign;
 use Drupal\trialmachine_order\Entity\Order;
 use Drupal\Core\Session\AccountInterface;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\HttpFoundation\RedirectResponse;


 class CampaignController extends ControllerBase
 {
    public $session;

    /**
     * @var \Drupal\trialmachine_mail\Service\TrialmachineMailService
     */
    protected $mailManager;

    public function __construct() {
        $this->session = \Drupal::service('session');
        $this->mailManager = \Drupal::service('trialmachine_mail.mail');
    }

    public function mountSession(Request $request) {
        $campaign = $request->attributes->get('trialmachine_campaign');

        /** @var \Drupal\trialmachine_order\Service\OrderService $orderService */
        $orderService = \Drupal::service('trialmachine_order.order');
        
        /** Redirect - waiting orders - at least 1 order */
        if (count($orderService->searchUserOpenOrders()) > 0) {
            $path = \Drupal\Core\Url::fromRoute('trialmachine_order.waiting_orders')
                ->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        }

        /** Redirect - rating orders - at least 1 order */
        if (count($orderService->searchUserDeliveredOrders()) > 0) {
            $path = \Drupal\Core\Url::fromRoute('trialmachine_order.rating_orders')
                ->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        }

        /** Redirect - already ordered for this campaign */
        $order_id = $orderService->searchUserOrderFromCampaign($campaign->id());

        if (count($order_id) > 0) {
            $path = \Drupal\Core\Url::fromRoute('trialmachine_order.already_ordered', ['trialmachine_order' => reset($order_id)])
                ->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        }

        // Set session
        $this->session->migrate();
        $this->session->set('selected_campaign', $campaign);
        $this->session->save();

        /** Create frozen order */
        $orderService->createFrozenOrder();
    
        $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.checkout')->toString();
        $response = new RedirectResponse($path);

        return $response->send();
    }

    public function checkout() {
        if(!$this->session->has('selected_campaign')) {
            global $base_url;
            $url = $base_url.'/';
            $response = new RedirectResponse($url);
            return $response->send(); // don't send the response yourself inside controller and form.
        }

        /** @var \Drupal\trialmachine_order\Service\OrderService $orderService */
        $orderService = \Drupal::service('trialmachine_order.order');
            
        /** Redirect - waiting orders - at least 1 order */
        if (count($orderService->searchUserOpenOrders()) > 0) {
            $path = \Drupal\Core\Url::fromRoute('trialmachine_order.waiting_orders')
                ->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        }

        /** Redirect - rating orders - at least 1 order */
        if (count($orderService->searchUserDeliveredOrders()) > 0) {
            $path = \Drupal\Core\Url::fromRoute('trialmachine_order.rating_orders')
                ->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        }

        /** Redirect - already ordered for this campaign */
        $campaign = $this->session->get('selected_campaign');
        $order_id = $orderService->searchUserOrderFromCampaign($campaign->id());

        if (count($order_id) > 0) {
            $path = \Drupal\Core\Url::fromRoute('trialmachine_order.already_ordered', ['trialmachine_order' => reset($order_id)])
                ->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        }

        $campaign = $this->session->get('selected_campaign');
        $data['campaign'] = $campaign;
        return [
            '#theme' => 'checkout',
            '#data' => $data,
        ];
    }

    public function addOrder()
    {
        if(!$this->session->has('selected_campaign')) {
            global $base_url;
            $url = $base_url.'/';
            $response = new RedirectResponse($url);
            return $response->send(); 
        } elseif (!\Drupal::currentUser()->isAuthenticated()) {
        // * Tenha item na sessão, mas não está logado. -> /checkout
            $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.checkout')->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        } elseif (!$_SESSION['get_attributes']['sg_estado']) {
            // * Tenha item na sessão, está logado mas não tem endereço. -> /entrega
            $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.delivery')->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        } elseif (!$_SESSION['get_attributes']['nm_tags'] || $_SESSION['get_attributes']['nm_tags'] == 'null') {
            // * Tenha item na sessão, está logado mas não tem endereço. -> /entrega
            $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.interest')->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        }
        
        $campaignObj = $this->session->get('selected_campaign');

        // Fields for adding the order.
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

        $user_id = $user->id();
        $campaign_id = $campaignObj->get('id')->value;
        $address_id = $user->field_address_id->entity->id();

        $data = [
            'user_id' => $user_id,
            'campaign_id' => $campaign_id,
            'address_id' => $address_id,
        ];

        $order = \Drupal::entityTypeManager()->getStorage('trialmachine_order')
            ->create($data);

        try {
            // @var \Drupal\trialmachine_campaign\Entity\Campaign
            $campaign = \Drupal::entityTypeManager()->getStorage('trialmachine_campaign')
                ->load($campaign_id);
            
            if (!$campaign->takeOneFromQuantity()) {
                throw new Exception("Ocorreu um erro ao tentar remover 1 produto da campanha (id {$campaign_id})");
            }

            $order->save();
        }
        catch (\Throwable $e) {
            \Drupal::logger('trialmachine_order')->error($e->getMessage());
        }

        $data['product'] = $campaignObj->product_id->entity;
        $this->session->migrate();
        $this->session->set('product', $data);
        $this->session->save();

        /** SEND EMAIL LOGIC */
        $params = [
            'data' => [
                'product' => $campaignObj->getProduct(),
                'user' => $user
            ],
            'users' => [$user],
        ];
        $this->mailManager->sendMail($params, 'checkout_confirmation');

        $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.confirmation')->toString();
        $response = new RedirectResponse($path);
        return $response->send();
    }

    public function completeOrder() {
        if(!$this->session->has('selected_campaign')) {
            global $base_url;
            $url = $base_url.'/';
            $response = new RedirectResponse($url);
            return $response->send(); 
        } elseif (!\Drupal::currentUser()->isAuthenticated()) {
        // * Tenha item na sessão, mas não está logado. -> /checkout
            $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.checkout')->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        } elseif (!$_SESSION['get_attributes']['sg_estado']) {
            // * Tenha item na sessão, está logado mas não tem endereço. -> /entrega
            $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.delivery')->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        } elseif (!$_SESSION['get_attributes']['nm_tags'] || $_SESSION['get_attributes']['nm_tags'] == 'null') {
            // * Tenha item na sessão, está logado mas não tem endereço. -> /entrega
            $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.interest')->toString();
            $response = new RedirectResponse($path);
            return $response->send();
        }

        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

        $orderId = \Drupal::entityQuery('trialmachine_order')
        ->condition('user_id', $user->id())
        ->condition('status', 7)
        ->execute();

        $order = Order::load(array_shift($orderId));
        
        $order->set('address_id', $user->field_address_id->entity->id());
        $order->set('status', 1);
        $order->save();

        $campaignObj = $this->session->get('selected_campaign');
        $data['product'] = $campaignObj->product_id->entity;
        $this->session->migrate();
        $this->session->set('product', $data);
        $this->session->save();

        /** SEND EMAIL LOGIC */
        $params = [
            'data' => [
                'product' => $campaignObj->getProduct(),
                'user' => $user
            ],
            'users' => [$user],
        ];
        $this->mailManager->sendMail($params, 'checkout_confirmation');

        $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.confirmation')->toString();
        $response = new RedirectResponse($path);
        return $response->send();
    }

    public function checkoutConfirmation() {
        if(!$this->session->has('product')) {
            global $base_url;
            $url = $base_url.'/';
            $response = new RedirectResponse($url);
            return $response->send(); // don't send the response yourself inside controller and form.
        }

        /** DATA TO VIEW */
        $data = $this->session->get('product');
        
        return [
            '#theme' => 'checkout_confirmation',
            '#data' => $data,
        ];
    }
 }

//  $path = \Drupal\Core\Url::fromRoute('trialmachine_campaign.add_order')->toString();
//  $response = new RedirectResponse($path);
//  $response->send();
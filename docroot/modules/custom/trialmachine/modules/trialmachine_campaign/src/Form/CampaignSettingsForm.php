<?php
/**
 * @file
 * Contains \Drupal\trialmachine_campaign\Form\TermSettingsForm.
 */

namespace Drupal\trialmachine_campaign\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContentEntityExampleSettingsForm.
 *
 * @package Drupal\trialmachine_campaign\Form
 *
 * @ingroup campaign
 */
class CampaignSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'trialmachine_campaign_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['trialmachine_campaign_settings']['#markup'] = 'Settings form for campaign Term. Manage field settings here.';
    return $form;
  }

}

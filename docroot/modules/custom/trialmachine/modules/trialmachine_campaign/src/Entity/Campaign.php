<?php
/**
 * @file
 * Contains \Drupal\content_entity_example\Entity\ContentEntityExample.
 */

namespace Drupal\trialmachine_campaign\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup campaign
 *
 *
 * @ContentEntityType(
 *   id = "trialmachine_campaign",
 *   label = @Translation("Campaign entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\trialmachine_campaign\Entity\Controller\CampaignListBuilder",
 *     "form" = {
 *       "add" = "Drupal\trialmachine_campaign\Form\CampaignForm",
 *       "edit" = "Drupal\trialmachine_campaign\Form\CampaignForm",
 *       "delete" = "Drupal\trialmachine_campaign\Form\CampaignDeleteForm",
 *     },
 *     "access" = "Drupal\trialmachine_campaign\CampaignAccessControlHandler",
 *   },
 *   list_cache_contexts = { "user" },
 *   base_table = "trialmachine_campaign",
 *   admin_permission = "administer trialmachine_campaign entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "title",
 *     "summary" = "summary",
 *     "description" = "description",
 *     "start_date" = "start_date", 
 *     "end_start" = "end_start", 
 *     "user_id" = "user_id",
 *     "created" = "created",
 *     "changed" = "changed",
 *     "quantity" = "quantity",
 *     "image__target_id" = "image__target_id",
 *     "image__alt" = "image__alt",
 *     "image__title" = "image__title",
 *     "image__width" = "image__width",
 *     "image__height" = "image__height",
 *    },
 *   links = {
 *     "canonical" = "/trialmachine_campaign/{trialmachine_campaign}",
 *     "edit-form" = "/trialmachine_campaign/{trialmachine_campaign}/edit",
 *     "delete-form" = "/trialmachine_campaign/{trialmachine_campaign}/delete",
 *     "collection" = "/trialmachine_campaign/list"
 *   },
 *   field_ui_base_route = "entity.campaign.settings",
 *   common_reference_target = TRUE,
 * )
 */
class Campaign extends ContentEntityBase {

  use EntityChangedTrait;

  /**
   * * GETTERS
  */

  /**
   * GET the start date.
   * 
   * @return string
   */
  public function getEndDate()
  {
    return $this->get('end_date')->value;
  }

  /**
   * GET the product.
   * 
   * @return \Drupal\trialmachine_product\Entity\Product
   * The related entity.
   */
  public function getProduct()
  {
    return $this->get('product_id')->entity;
  }

  /**
   * GET the quantity of products.
   * 
   * @return int
   */
  public function getQuantity()
  {
    return (int) $this->get('quantity')->value;
  }

  /**
   * GET the title.
   * 
   * @return string
   */
  public function getTitle()
  {
    return $this->get('title')->value;
  }

  /**
   * GET the start date.
   * 
   * @return string
   */
  public function getStartDate()
  {
    return $this->get('start_date')->value;
  }

  /**
   * * DATA MANIPULATION
   */

  /**
   * Add one or more products to quantity field in the campaign entity and save the result.
   * @param int $add
   * The value to add. Default is 1.
   * @return boolean
   * Return false if caught an exception, true if not.
   */
  public function addToQuantity(int $add = 1)
  {
    try {
      $quantity = ((int) $this->get('quantity')->value) + $add;
      
      $this->set('quantity', $quantity);
      
      $this->save();
    }
    catch (\Throwable $e) {
      return false;
    }

    return true;
  }

  /**
   * Gets the campaign's duration in days.
   * 
   * @return int
   * The difference number in days between start_date and end_date.
   * 
   * @return boolean
   * FALSE if date_diff is FALSE.
   */
  public function getDurationInDays()
  {
    $start_date = date_create($this->get('start_date')->value);
    $end_date = date_create($this->get('end_date')->value);

    $date_diff = date_diff($start_date, $end_date);

    if ($date_diff === false) {
      return false;
    }

    return $date_diff->days;
  }

  /**
   * Gets the number of products ordered from the campaign.
   * 
   * @return int
   * The total of orders/products.
   */
  public function getTotalOfRequestedProducts()
  {
    return (int) \Drupal::entityTypeManager()->getStorage('trialmachine_order')
      ->getQuery()
      ->condition('campaign_id', $this->id())
      ->count()
      ->execute();
  }

  /**
   * Remove one product from quantity field in the campaign entity and save the result.
   * @return boolean
   * Return false if caught an exception, true if not.
   */
  public function takeOneFromQuantity()
  {
    try {
      $quantity = $this->get('quantity')->value;
      
      if ($quantity <= 0 || is_null($quantity)) {
        throw new Exception(t('There are no products in stock for this campaign.'));
      }

      $this->set('quantity', --$quantity);

      $this->save();
    }
    catch (\Throwable $e) {
      return false;
    }

    return true;
  }

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    // Default author to current user.
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Campaign entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Contact entity.'))
      ->setReadOnly(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Título'))
    ->setDescription(t('Título da campanha.'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => -1,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -1,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
    ->setLabel(t('Descrição'))
    ->setDescription(t('Descrição da campanha.'))
    ->setRequired(TRUE)
    ->setSettings([
      'default_value' => '',
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'text_textarea_with_summary',
      'weight' => 1,
    ])
    ->setDisplayOptions('form', [
      'type' => 'text_textarea_with_summary',
      'settings' => array(
        'rows' => 4,
      ),
      'weight' => 1,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['summary'] = BaseFieldDefinition::create('string_long')
    ->setLabel(t('Resumo'))
    ->setDescription(t('Resumo da campanha.'))
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'text_textfield',
      'weight' => 0,
    ])
    ->setDisplayOptions('form', [
      'label' => 'above',
      'type' => 'text_textfield',
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['image'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Imagem'))
      ->setDescription(t('Imagem da campanha.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'image',
        'weight' => 2,
        'label' => 'hidden',
        'settings' => [
          'image_style' => 'large',
        ],
      ])
      ->setDisplayOptions('form',  [
        'type' => 'image',
        'weight' => 2,
        'label' => 'hidden',
        'settings' => [
          'image_style' => 'thumbnail',
        ]
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setReadOnly(TRUE);

    // Owner field of the contact.
    // Entity reference field, holds the reference to the user object.
    // The view shows the user name field of the user.
    // The form presents a auto complete field for the user name.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Autor'))
      ->setDescription(t('O criador da campanha.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 3,
      ))
      ->setDisplayOptions('form', array(
        'label' => 'above',
        'type' => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ),
        'weight' => 3,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['product_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Produto'))
      ->setDescription(t('Produto man'))
      ->setSetting('target_type', 'trialmachine_product')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'product',
        'weight' => 4,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'entity_reference_autocomplete',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ),
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['quantity'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Quantidade de produtos'))
      ->setDescription(t('Quantidade de produtos disponíveis nesta campanha.'))
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'integer',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'integer',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['start_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Início da campanha'))
      ->setDescription(t('Inicio da campanha.'))
      ->setDefaultValue(DrupalDateTime::createFromTimestamp(time()))
      ->setRequired(TRUE)
      ->setSetting('datetime_type', 'datetime')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'datetime',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime',
        'weight' => 6,
      ]);
      // ->setDisplayConfigurable('form', TRUE)
      // ->setDisplayConfigurable('view', TRUE)


    $fields['end_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Término da campanha'))
      ->setDescription(t('Término da campanha.'))
      ->setDefaultValue(DrupalDateTime::createFromTimestamp(time()))
      ->setSetting('datetime_type', 'datetime')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'datetime',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime',
        'weight' => 7,
      ])
      // ->setDisplayConfigurable('form', TRUE)
      // ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);
  
    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setDescription(t('The product URL alias.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setComputed(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}

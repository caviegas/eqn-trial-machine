(function ($, jQuery) {
	$(document).ready(function(){
        // buttons
        const submitButton = $('#edit-submit');
        const cancelButton = $('#edit-cancel');
        const editButton = $('#edit-button');
        const forwardButton = $('#forward-button');

        let formAddress;
        if($('#form-review').length == 1) {
            formAddress = $('#form-review');
        }
        

        formAddress.trigger('changeControls');
        if (formAddress.hasClass('form-disabled')) {
            formAddress.children('fieldset').attr('disabled', true)
        } 

        editButton.on('click', function () {
            formAddress.removeClass('form-disabled')
            formAddress.addClass('form-edit')
            formAddress.children('fieldset').attr('disabled', false).trigger('changeControls');
        })

        cancelButton.on('click', function () {
            location(reload);
        })

        formAddress.on('changeControls',function() {
            if (formAddress.hasClass('form-disabled')) {
                formAddress.children('fieldset').attr('disabled', true)
            }
        })
	});
})(jQuery);

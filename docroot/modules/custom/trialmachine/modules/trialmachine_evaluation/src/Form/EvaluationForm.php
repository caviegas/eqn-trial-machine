<?php
/**
 * @file
 * Contains \Drupal\trialmachine_evaluation\Form\TermForm.
 */

namespace Drupal\trialmachine_evaluation\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the content_entity_example entity edit forms.
 *
 * @ingroup content_entity_example
 */
class EvaluationForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\trialmachine_evaluation\Entity\Evaluation */
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // dd($form_state->getValues());
    // Redirect to term list after save.
    $form_state->setRedirect('entity.trialmachine_evaluation.collection');
    $entity = $this->getEntity();
    $entity->save();
  }
}

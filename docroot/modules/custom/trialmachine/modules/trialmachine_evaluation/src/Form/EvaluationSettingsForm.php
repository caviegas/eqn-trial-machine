<?php
/**
 * @file
 * Contains \Drupal\trialmachine_evaluation\Form\EvaluationSettingsForm.
 */

namespace Drupal\trialmachine_evaluation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContentEntityExampleSettingsForm.
 *
 * @package Drupal\trialmachine_evaluation\Form
 *
 * @ingroup evaluation
 */
class EvaluationSettingsForm extends FormBase {
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'trialmachine_evaluation_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['trialmachine_evaluation_settings']['#markup'] = 'Settings form for Evaluation Term. Manage field settings here.';
    return $form;
  }

}

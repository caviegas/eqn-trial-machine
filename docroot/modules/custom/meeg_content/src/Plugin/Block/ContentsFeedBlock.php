<?php

namespace Drupal\block_content\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityTypeManager;
Use \Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\views\Entity\View;


/**
 * Provides a 'Feed' Block.
 *
 * @Block(
 *   id = "block_content",
 *   admin_label = "Content feed",
 *   category = "Meeg feeds",
 * )
 */
class ContentsFeedBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $logged_in = false;
    $tagList = [];
    $userController = new UserController;
    $contentController = new ContentController;
    $ageRange = null;

    // Verifica se usuário está logado pela API
    if ($userController->hasBasicData()) {
      $logged_in = true;

      // Pega os atributos do usuário
      $userAttr = $userController->getUserAttr();

      if ($userController->hasChildren($userAttr)) {
        $activeAgeRange = $userController->getActiveAge($userAttr);
      }

      // Pega id dos termos de interesse
      if ($userController->hasTags($userAttr)) {
        $tagList = $userController->getTags($userAttr['nm_tags']);
      }
    }

    // Pegar conteúdos já lido pelo usuário
    if ($logged_in && isset($userAttr['ndb_contents'])) {
      $readContentIds = $userController->getUserReadContents($userAttr['ndb_contents']);
    } else {
      $readContentIds = [];
    }

    // Pega a página atual
    $view_id = \Drupal::routeMatch()->getParameter('view_id');
    if ($view_id) {
      $view = View::load($view_id);
    }

    $routeName = \Drupal::routeMatch()->getRouteName();
    $fullContent = (bool) strpos($routeName, 'full_content');

    $isUserView = ($view_id == 'listagem_conteudo') ? true : false;

    $currentTime = \Drupal::time()->getCurrentTime();

    // Obter os conteúdos por condições aplicadas
    $nidQuery = \Drupal::entityQuery('node')
    ->condition('status', 1)
    ->condition('created', $currentTime, '<')
    ->condition('type', ['forum', 'ad', 'page'], 'NOT IN');

    if ($logged_in && $tagList != []) {
      $nidQuery->condition('field_tag_interesses', $tagList->ids, 'IN');
    }

    $altHome = $contentController->isAlternativeHome();

    // Caso seja uma home alternativa
    if ($altHome != null) {
      $term = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['name' => $altHome, 'vid' => 'faixa_etaria']);
      $termId = key($term);
      $ageRange = new \stdClass();
      $ageRange->id = $term[$termId]->id();
      $ageRange->label = $term[$termId]->getName();
      $nidQuery->condition('field_tag_faixaetaria', [$ageRange->id, 21], 'IN');

    } elseif ($logged_in && isset($activeAgeRange)) {
      $nidQuery->condition('field_tag_faixaetaria', [$activeAgeRange->id, 21], 'IN');
    }

    $nids = $nidQuery->sort('created', 'DESC')->execute();

    // Posts fixos apenas em homes alternativas ou usuário logado com filho
    if (($altHome != null) || ($logged_in && isset($activeAgeRange))) {
      $fixedNids = $nidQuery->condition('sticky', 1)->execute();
      $nids = array_merge($fixedNids, $nids);
      $nids = array_unique($nids);      
    }

    // dd($nids);

    $nodes = Node::loadMultiple($nids); 

    $list = [];
    $filteredTags = null;
    foreach ($nodes as $node) {

      // Objeto para Array
      $nodeArray = $node->toArray();

      // Pegar apenas os campos com o prefixo de field tipo tag
      $nodeTags = array_filter($nodeArray, function($key) {  
          return strpos($key, 'field_tag_') === 0;
      }, ARRAY_FILTER_USE_KEY);
    
      $tags = null;
      // Se o conteúdo tiver tags:
      if ($nodeTags != null) {
      
        // Função pra retornar apenas arrays com conteúdo.
        $filterFunction = function($v){
          return array_filter($v) != array();
        };

        // Chamando a função acima na variavel das tags
        $tags = array_filter($nodeTags, $filterFunction);

        // Cria objeto tags
        $filteredTags = [];
        foreach ($tags as $tagbundle) {
          foreach ($tagbundle as $tag) {
            // Tag info
            $id = $tag['target_id'];
            $term = Term::load($id);
            $vocabulary = $term->bundle();
            
            // Se ainda não existir esta chave, criar. (Usado para evitar sobrescricao de tags do mesmo vocabulario)
            if (!array_key_exists($vocabulary, $filteredTags)) {
              $filteredTags[$vocabulary] = [];
            }

            $filteredTags[$vocabulary] = array_merge($filteredTags[$vocabulary], [
              [
                'id' => $id,
                'name' => $term->getName(),
              ]
            ]);
          }
        }
      }

      $isRead = false;
      if (in_array($node->id(), $readContentIds)) {
        $isRead = true;
      }

      // Pega imagem do conteúdo
      $picturePath = $contentController->getNodeImagePath($node);

      // Object creation
      $list = array_merge($list, [
          [
            'title' => $node->getTitle(),
            'type' => $node->getType(),
            'tags' => $filteredTags,
            'url' => $node->url(),
            'summary' => $node->body->summary,
            'read' => $isRead,
            'picture' => $picturePath,
            'sticky' => $isSticky = ($node->isSticky() && ($altHome != null || $logged_in && isset($activeAgeRange))) ? true : false,
          ]
        ]);
    }

    if (!$fullContent) {
      if (count($list) > 5) {
        $showMore = true;
        $list = array_slice($list, 0, 5);
      }
    } 

    return [
      '#theme' => 'block_content',
      '#nodes' => $list,
      '#fullContent' => ($fullContent || $isUserView),
      '#altHome' => ($altHome == null) ? false : true,
      '#interestTags' => $tagList,
      '#showMore' => $showMore ?? false,
      '#ageTags' => ($view_id == 'frontpage' && isset($_SESSION['get_user']) && isset($activeAgeRange)) ? $activeAgeRange : $ageRange,
    ];
  }
}

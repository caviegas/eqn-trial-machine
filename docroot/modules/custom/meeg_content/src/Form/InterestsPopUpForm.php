<?php

namespace Drupal\block_content\Form;

use Drupal\block_content\Controller\UserController;
use Drupal\block_content\Controller\ContentController;
use Drupal\Core\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Url;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;

/**
 * Our custom ajax form.
 */
class InterestsPopUpForm extends FormBase {

    /**
    * {@inheritdoc}
    */
    public function getFormId() {
        return "block_content_interestspopup";
    }

    public function buildForm(array $form, FormStateInterface $form_state) {
    
		$userController = new UserController;
		$contentController = new ContentController;
		
		if ($userController->hasBasicData()) {
			$userAttr = $userController->getUserAttr();
			$altHome = $contentController->isAlternativeHome();

			// Se não for null, então ele está em uma home alternativa
			if ($altHome != null) {
				$form['section-agerange'] = [
					'#type' => 'markup',
					'#markup' => '<span class="section-age">'.$altHome.'</span><span class="ages-caption">Baseado na seção atual</span>'
				];			
		 	} elseif ($userController->hasChildren($userAttr)) {
				$form['children-title'] = [
					'#type' => 'markup',
					'#markup' => '<span class="fieldset-legend">Filtro de idade</span>'
				];		
				
				$activeAge = $userAttr['actives']['age_filter'];
				$age_filters = $userAttr['age_filters'];

				$arrayAges = [];
				foreach ($age_filters as $key => $value) {
				$arrayAges[] = $value->label;
				};

				$arrayAges = array_unique($arrayAges);

				

				// Selecione idade ativa
				$form['child'] = [
				'#type' => 'radios',
				'#options' => $arrayAges,
				//'#title' => $this->t('Suas idades'),
				'#default_value' => $activeAge
				];

				$host = \Drupal::request()->getHost();
				$form['add-children'] = [
					'#type' => 'markup',
					'#markup' => '<span class="ages-caption">Baseado na idade dos seus filhos | <a href="/user/familia">editar</a></span>'
				];	
			} else {
				$form['add-children'] = [
					'#type' => 'markup',
					'#markup' => '<span class="ages-caption">Você ainda não tem filhos cadastrados | <a href="/user/familia">adicionar</a></span>'
				];	
			}
	
  

        $form['gridopen'] = [
          '#type' => 'markup',
          '#markup' => '<div class="grid-options">'
        ];

        $termObjects = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['vid' => 'interesses']);
        
        $terms = [];
        $selectedTerms = array();
        $unSelectedTerms = array();

        $selectedTags = array();
        $unSelectedTags = array();
        foreach ($termObjects as $term) {
          $terms[$term->id()] = $term->label();
          array_push($unSelectedTags, $term->id());
        }

        if(isset($_SESSION["get_attributes"]["nm_tags"]) && $_SESSION["get_attributes"]["nm_tags"] != 'null'){
          $nm_tags = json_decode($_SESSION["get_attributes"]["nm_tags"])->interesses;
          
          foreach($nm_tags AS $key => $value){
            array_push($selectedTags, $value->id);
            unset($unSelectedTags[$value->id]);
          }
        }

	  	$form['selectedTags'] = array(
          '#type' => 'checkboxes',
          '#id' => 'selectedTags',
          //'#title' => 'Seus interesses',
          '#options' => $terms,
          '#default_value' => $selectedTags
        );

        $form['gridclose'] = [
          '#type' => 'markup',
          '#markup' => "</div>"
        ];
		
				
			$form['actions']['#type'] = 'actions';
			$form['actions']['submit'] = [
				'#type' => 'submit',
				'#value' => $this->t('Atualizar'),
				'#button_type' => 'primary',
				// '#ajax' => [
				//   'callback' => '::submitFormAjax', 
				//   'effect' => 'fade',
				//   "wrapper" => "test-ajax",
				//   'progress' => [
				//     'type' => 'throbber',
				//     'message' => $this->t('Atualizando seus dados...'),
				//   ],
				// ],
				'#suffix' => '<div class="result_message"></div>'
	
			];
			return $form;
		}
    }

		public function submitForm(array &$form, FormStateInterface $form_state){
			
			$_SESSION['get_attributes']['actives']['age_filter'] = ($form_state->getValue('child') + 0);


			$response = new AjaxResponse();
			$count_errors = 0;
			$count_interesses = 0;
	
			// Colocar aqui o numero de atributos existentes no Form.
			$num_atributos = 12;
	
			$msg_erro = "";
			$msg_erroa = "";
			$get_attributes = "";
	
			try {
				$wsdl = $GLOBALS['CADU_API_URL'];
				$client = new SoapClient($wsdl, array(
					'trace' => 1,
				));
			
				// SET HEADERS
				$headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
				$header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
				$client->__setSoapHeaders($header);
				
				// Check if service is available
				$serviceStatus = $client->IsServiceAvailable();
				if ($serviceStatus != true) {
						$message = 'Serviço indisponível';
						return $message;
				}
	
				$codeuser = $_SESSION["get_user"]['codigo'];
				
				
		 
				$checkedInteresses = array_filter($form_state->getValues()['selectedTags'], function ($value) {
					return ($value !== 0);
				});
				$checkedInteresses2 = array();
				foreach($checkedInteresses AS $key => $value){
					$checkedInteresses2[] = $value;
				}
				// $checkedInteresses2Implode = implode(", ", $checkedInteresses2);
				// $checkedInteresses2 = "[" . $checkedInteresses2Implode . "]";
				// echo $checkedInteresses2;
	
				
				$selectedInteresses = [];
				$termObjects = \Drupal::entityTypeManager()
				->getStorage('taxonomy_term')
				->loadByProperties(['vid' => 'interesses']);
				// ->loadMultiple($checkedInteresses2);
				// ->loadByProperties(['vid' => 'interesses']);
	
				// print_r($termObjects);
				foreach ($termObjects as $term) {
					// $newInteresse = new \stdClass();
					if(in_array($term->id(), $checkedInteresses2)){
						// echo $term->id();
						$newInteresse = new \stdClass();
						$newInteresse->id = intval($term->id());
						$newInteresse->label = $term->label();
						$selectedInteresses[] = $newInteresse;
					}
					
					// $newInteresse->id = $term->id();
					// $newInteresse->label = $term->label();
					// $selectedInteresses[] = $newInteresse;
					
				}
				
				// print_r($selectedInteresses);
				$nm_tags = [ 'interesses' => $selectedInteresses];

	      		// Se nenhum interesse tiver check, salva como 'null'
				if ($checkedInteresses == []) {
					$nm_tags = 'null';
				} else {
					$nm_tags = json_encode($nm_tags, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
				}
				
				// print_r(json_encode($nm_tags, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
				// dd($selectedInteresses);
				$data_atributos['atributos'] = [
					[
						'CodigoVisitante' => $codeuser,
						'NomeAtributo' => 'nm_tags',
						'Valor' => $nm_tags,
						'Items' => null
					]
				];
	
			
				$res = $client->SaveAttributes($data_atributos);
				// $res = $client->SetPropertyValues($data_atributos);
				$res_atributes = $res->SaveAttributesResult;
	
					/**
												 * Cria seesion com atributos do usuário
												 */
	
												// $res_attr = $client->GetAttributes(['userName' => $_SESSION["get_user"]['email']]);
												// $attributes = $res_attr->GetAttributesResult->Atributo;
	
												// $attr = [];
												// foreach ($attributes as $value) {
											
												//   if($value->ValorMultivalorado == null && $value->Valor !== null){
												//     $attr = array_merge($attr, [
												//       $value->NomeAtributo => $value->Valor   
												//     ]);
												//   }else{
												//     $attr = array_merge($attr, [
												//       $value->NomeAtributo => $value->ValorMultivalorado   
												//   ]);
												//   }
												// }
												// $_SESSION["get_attributes"] = $attr;
	
												/**
												* Cria seesion do usuário
												*/
												// $res = $client->GetUser(['username' => $_SESSION["get_user"]['email']]);
	
												// $attr_user = [
												//   "ativo" => $res->GetUserResult->Ativo,
												//   "cpf" => $res->GetUserResult->CPF,
												//   "cpf_responsavel" => $res->GetUserResult->CPFResponsavel,
												//   "codigo" => $res->GetUserResult->Codigo,
												//   "codigo_area_site_criacao" => $res->GetUserResult->CodigoAreaSiteCriacao,
												//   "confirmacao_email" => $res->GetUserResult->ConfirmacaoEmail,
												//   "data_acesso_bloquado" => $res->GetUserResult->DataAcessoBloqueado,
												//   "data_alteracao" => $res->GetUserResult->DataAlteracao,
												//   "data_criacao" => $res->GetUserResult->DataCriacao,
												//   "data_nascimento" => $res->GetUserResult->DataNascimento,
												//   "data_ultimo_login" => $res->GetUserResult->DataUltimoLogin,
												//   "email" => $res->GetUserResult->Email,
												//   "email_responsavel" => $res->GetUserResult->EmailResponsavel,
												//   "expiration_date_token" => $res->GetUserResult->ExpirationDateToken,
												//   "forgot_password_token" => $res->GetUserResult->ForgotPasswordToken,
												//   "ip_criacao" => $res->GetUserResult->IPCriacao,
												//   "login_result" => $res->GetUserResult->LoginResult,
												//   "match_code_id" => $res->GetUserResult->MatchCodeId,
												//   "nome" => $res->GetUserResult->Nome,
												//   "nome_responsavel" => $res->GetUserResult->NomeResponsavel,
												//   "quantidade_de_falhas_login" => $res->GetUserResult->QuantidadeDeFalhasLogin,
												//   "questao_seguranca" => $res->GetUserResult->QuestaoSeguranca,
												//   "resposta_questao_seguranca" => $res->GetUserResult->RespostaQuestaoSeguranca,
												//   "senha" => $res->GetUserResult->Senha,
												//   "tipo_cadastro" => $res->GetUserResult->Tipo_Cadastro,
												//   "user_social_id" => $res->GetUserResult->UserSocialId
	
												// ];
	
												// $_SESSION["get_user"] = $attr_user;
	
												updateUserSession();
	
												$redirect_path = "/";
												$url = url::fromUserInput($redirect_path);
												$form_state->setRedirectUrl($url);
				$response->addCommand(
					new HtmlCommand(
						'.result_message',
						'<div class="result_message">Salvo</div>'),
				);
	
	
	
			} catch (SoapFault $exception) {
				$response = '<pre>'.$exception->getMessage()."\n REQUEST:\n" . htmlentities($client->__getLastRequest()) . "\n";    
				\Drupal::messenger()->addError($response);
			}   
	
			return $response;
		 }
	
}
?>
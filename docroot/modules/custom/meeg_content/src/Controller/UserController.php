<?php

namespace Drupal\meeg_content\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines HelloController class.
 */
class UserController extends ControllerBase {

  /* 
  Verifica se a sessão tem os dados básicos de um usuário. 
  Se tiver, isso significa que ele foi logado através da API.
  */
  public function hasBasicData() {
    $res = (isset($_SESSION['get_user']) && isset($_SESSION['get_attributes'])) ? true : false;
    return $res;
  }

  public function getUserAttr() { 
    $userAttributes = $_SESSION['get_attributes'];

    return $userAttributes;
  }
  
  // Verifica se o usuário tem tags (de interesse por padrão)
  public function hasTags($userAttributes) {
    $res = (isset($userAttributes['nm_tags']) && $userAttributes['nm_tags'] != 'null') ? true : false;

    return $res;
  }

  public function getTags($tags, $key = 'interesses') {
    
    $tags = json_decode($tags);

    $tagIdList = [];
    $tagLabelList = [];

    foreach ($tags->$key as $tag) {
      $tagIdList[] = $tag->id;
      $tagLabelList[] = $tag->label;
    }

    $tagList = new \stdClass();
    $tagList->labels = $tagLabelList;
    $tagList->ids = $tagIdList;

    return $tagList;
  }


  public function hasChildren($userAttributes) { 
    $res = (isset($userAttributes['children'])) ? true : false;

    return $res;
  }

  /* 
    Retorna a faixa etária ativa dentre as disponíveis para o usuário. 
    As faixas são habilidadas baseado na idade dos filhos deste usuário
    usando o método setAvailableAgeFilters()
  */
  public function getActiveAge($userAttributes) { 
    $index = $userAttributes["actives"]["age_filter"];
    $activeAgeRange = $userAttributes["age_filters"][$index];

    return $activeAgeRange;
  }

  // Transforma a data de nascimento em tag de faixa etária
  public function getAgeTagId($birthday) {
    
    $dtz = new \DateTimeZone("America/Sao_Paulo");
    $age = \DateTime::createFromFormat('d/m/Y', $birthday, $dtz)
    ->diff(new \DateTime('now', $dtz))
    ->y;
  
    // Define a label da tag que representa a idade
    $tagLabel;
    switch(true) {
      case ($age >= 0 && $age <= 2);
        $tagLabel = '1 - 2';
        break;
      case ($age >= 3 && $age <= 4);
        $tagLabel = '3 - 4';
        break;
      case ($age >= 5 && $age <= 6);
        $tagLabel = '5 - 6';
        break;
      case ($age >= 7 && $age <= 8);
        $tagLabel = '7 - 8';
        break;
      case ($age >= 9);
        $tagLabel = '9+';
        break;
    };

    $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => $tagLabel, 'vid' => 'faixa_etaria']);

    $ageRange = new \stdClass();
    $ageRange->id = key($term); // Pega o ID do termo de taxonomia que fica localizado na primeira chave do objeto
    $ageRange->label = $tagLabel;

    return $ageRange;
  }

  // Seta as faixas etárias disponíveis
  public function setAvailableAgeFilters($children) {

    $ageFilters = [];
    foreach ($children as $child) {
      $ageFilters[] = $this->getAgeTagId($child['birthday']);
    }

    return $ageFilters;
  }

  // Retorna apenas o ID dos conteúdos
  public function getUserReadContents($content) {

    if ($content != "null") {

        $readContents = json_decode($content, true);
        foreach ($readContents as $content) {
          $readContentsIds[] = $content['id'];
        }
      
      return $readContentsIds;

    } else {
      
      return [];
    }
  }

  public function hasAddress() {
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

    return !$user->field_address_id->isEmpty();
  }

}
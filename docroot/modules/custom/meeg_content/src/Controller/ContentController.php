<?php

namespace Drupal\meeg_content\Controller;

use Drupal\block_content\Controller\UserController;
use Drupal\Core\Controller\ControllerBase;
use Drupal\views\Entity\View;
Use Drupal\taxonomy\Entity\Term;
use Drupal\file\Entity\File;
use \SoapClient;
use \SoapVar;
use \SoapHeader;
use \SoapFault;


/**
 * Defines HelloController class.
 */

class ContentController extends ControllerBase {

  public function getCurrentPage() {
    // $alternativeHomeLabels = ['0 - 2', '3 - 4', '5 - 6', '7 - 8', '9+', 'Todas as fases'];

    $view_id = \Drupal::routeMatch()->getParameter('view_id');
    if ($view_id) {
      $view = View::load($view_id);
    } else {
      $view = null;
    }

    return $view;
  }

  public function isAlternativeHome() {
    $alternativeHomeLabels = ['0_2', '0_3', '5_6', '7_8', '9', 'todas_faixasetarias'];

    // Pega a View da página atual pela ID
    $view_id = \Drupal::routeMatch()->getParameter('view_id');
    // workaround for nodes
    if ($view_id != null) {
      $view = View::load($view_id);
      // if (in_array($view->label(), $alternativeHomeLabels)) {
      // return $view->label();
      // } else {
      return (in_array($view->id(), $alternativeHomeLabels)) ? $view->label() : null;
      // }
    } else {
      return null;
    }

  }

  // FORUMS
  public function getForumAgeRange($altHome) {

    // Primeiro pegamos a taxonomia de idade correspondente ao label (parâmetro) fornecido. 
    $ageTerm = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['name' => $altHome, 'vid' => 'faixa_etaria']);
    
    $ageTermId = key($ageTerm);

    // Agora buscamos pela taxonomia de forum que possui o campo de faixa etaria com este id 
    $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['field_faixaetaria' => $ageTermId]);
    $term = $term[key($term)];
    $ageRange = new \stdClass();
    $ageRange->id = $term->id();
    $ageRange->label = $term->getName();
    
    return $ageRange;
  }

  // Conversor de taxonomia Forums > Faixa etaria 
  public function convertForumToAgeRangeId($id) {

    $term = Term::load($id);
    $ageRangeId = $term->get('field_faixaetaria')->getValue()[0]['target_id'];
    
    return $ageRangeId;
  }

  // Conversor de taxonomia Faixa etaria > Forum 
  public function convertAgeRangeIdToForum($id) {

    $term = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['tid' => $id, 'vid' => 'faixa_etaria']);
    $term = $term[key($term)];

    $forumTerm = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_term')
    ->loadByProperties(['field_faixaetaria' => $term->id()]);
    $forumTerm = $forumTerm[key($forumTerm)];

    return $forumTerm;
  }


  // Extrai a URL da imagem do objeto Node
  public function getNodeImagePath($node) {

    if (($node->field_image != null) && ($node->field_image->getValue() != [])) {
      $imageId = $node->field_image->getValue()[0]['target_id'];
      $file = File::load($imageId);
      $picturePath = file_create_url($file->get('uri')->value);
    } else {
      $picturePath = 'https://res.cloudinary.com/meeg/image/upload/v1619183105/NDB/placeholder-ninho_emd7wq.png';
    }

    return $picturePath;
  }

  public function getNodeTagIds($node) {
    $nodeArray = $node->toArray();

    // Pegar apenas os campos com o prefixo de field tipo tag
    $nodeTags = array_filter($nodeArray, function($key) {  
        return strpos($key, 'field_tag_') === 0;
    }, ARRAY_FILTER_USE_KEY);
  
    $tags = null;
    // Se o conteúdo tiver tags:
    if ($nodeTags != null) {
    
      // Função pra retornar apenas arrays com conteúdo.
      $filterFunction = function($v){
        return array_filter($v) != array();
      };

      // Chamando a função acima na variavel das tags
      $tags = array_filter($nodeTags, $filterFunction);

      // Cria objeto tags
      $tagArray = [];
      foreach ($tags as $tagbundle) {
        foreach ($tagbundle as $tag) {
          // Tag info
          $id = $tag['target_id'];
          array_push($tagArray, $id);
        }
      }
    }
    return $tagArray;
  }

  public function isUserView() {

    $view_id = \Drupal::routeMatch()->getParameter('view_id');
    $bool = ($view_id == 'conteudos_lidos') ? true : false;

    return $bool;
  }


  /*
    Esse verificar se: 
      - O conteúdo está sendo lido pela prmeira vez
      - O conteúdo está sendo relido
  */
  public function verifyContent($content) {
    $userController = new UserController;
    $userAttr = $userController->getUserAttr();

    // Usada para decidir se é preciso comunicar com a API
    $needUpdate = false;
    // Se torna verdadeira caso o conteúdo exista na lista
    $contentExists = false;

    // Essas próximas linhas servem pra colocar o objeto dentro de um array caso ele venha sozinho, possibilitando o uso no foreach

    if (isset($userAttr['ndb_contents']) && $userAttr['ndb_contents'] != "null") {
      $typeof = gettype(json_decode($_SESSION['get_attributes']['ndb_contents']));

      if ($typeof == 'object') {
        $userReadContents[] = json_decode($_SESSION['get_attributes']['ndb_contents']);
      } else {
        $userReadContents = json_decode($_SESSION['get_attributes']['ndb_contents']);
      }
    } else {
      $userReadContents = null;
    }

    // Roda se já tiver algum conteúdo
    if ($userReadContents != null) {
      foreach ($userReadContents as $readContent) {
        // Verifica se o conteúdo já existe nos lidos do usuário
        if ($readContent->id == $content->id) {
          $contentExists = true;
          // Caso encontre, verifica se a data coincide
          if ($readContent->date != $content->date) {
          // Se for diferente, atualize
            $readContent->date = $content->date;
            $needUpdate = true;
          }
        }
      } 
      if (!$contentExists) {
        $userReadContents = array_merge($userReadContents, [$content]);
        $needUpdate = true;
      }
    } else { // Se não tiver conteúdo, este será o primeiro
      $userReadContents[] = $content;
      $needUpdate = true;
    }

    if ($needUpdate) {
      // Encode de volta pra json
      $userReadContents = json_encode($userReadContents, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
      
      $_SESSION['get_attributes']['ndb_contents'] = $userReadContents;
      $this->updateReadContents($userReadContents);
    }

    return;
  }

  public function updateReadContents($userReadContents) {

    // Pega o código do usuário
    $visitorCode = $_SESSION['get_user']['codigo'];
    $data['atributos'] = [
      [
        'CodigoVisitante' => $visitorCode,
        'NomeAtributo' => 'ndb_contents',
        'Valor' => $userReadContents,
        'Items' => null
      ],
    ];

    $wsdl = $GLOBALS['CADU_API_URL'];
    $client = new SoapClient($wsdl, array(
        'trace' => 1,
    ));
    // SET HEADERS
    $headerVar = new SoapVar('<PartnerCode xmlns="ns">16</PartnerCode><CryptoAreaSite xmlns="ns">TmVzdGxlVHJpYWxNYWNoaW5l</CryptoAreaSite>',XSD_ANYXML);
    $header = new SoapHeader('http://tempuri.org/','RequestParams', $headerVar);                
    $client->__setSoapHeaders($header);
    
    // Check if service is available
    $serviceStatus = $client->IsServiceAvailable();
    if ($serviceStatus != true) {
        $message = 'Serviço indisponível';
        return $message;
    }

    $res = $client->SaveAttributes($data);
  }
}
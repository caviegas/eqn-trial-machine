## Quick Start Guide For D9 with Lightnest Setup in Few Steps

### Drupal 9 (Lightnest 4.x):

* composer create-project --no-install drupal/recommended-project lightnest
* cd lightnest
* composer.json replace "minimum-stability": "dev" with "minimum-stability": "stable"
* Make sure you set "enable-patching": true in the Root Composer.json file. Check in extra key or other composer file
  example.
* composer config repositories.drupal composer https://packages.drupal.org/8
* composer config repositories.lightnest composer https://webcms:NPJ4fj5Nvw2zBTCw@satis.lightnest.nestle.com/
* composer config repositories.asset-packagist composer https://asset-packagist.org
* composer clear-cache
* composer require lightnest/lightnest:^4

### Drupal 8 (Lightnest 3.x):

* composer create-project --no-install drupal/recommended-project:8.x lightnest
* cd lightnest
* composer.json replace "minimum-stability": "dev" with "minimum-stability": "stable"
* Make sure you set "enable-patching": true in the Root Composer.json file. Check in extra key or other composer file
  example.
* composer config repositories.drupal composer https://packages.drupal.org/8
* composer config repositories.lightnest composer https://webcms:NPJ4fj5Nvw2zBTCw@satis.lightnest.nestle.com/
* composer config repositories.asset-packagist composer https://asset-packagist.org
* composer clear-cache
* composer require acquia/lightning:^4
* composer require lightnest/lightnest:^3

<?php

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *
 * n.b. The settings.pantheon.php file makes some changes
 *      that affect all envrionments that this site
 *      exists in.  Always include this file, even in
 *      a local development environment, to insure that
 *      the site settings remain consistent.
 */
include __DIR__ . "/settings.pantheon.php";

/**
 * If there is a local settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}

// Elimima o limite de tempo de execução.
ini_set('max_execution_time', 0);

$requestUri = $_SERVER['REQUEST_URI'];

if(preg_match('/user\/([0-9])/', $requestUri)) {
  $redirectHome = true;
} elseif (strpos($requestUri, 'user/register') || strpos($requestUri, 'user/login')) {
 $redirectHome = true;
} else {
  $redirectHome = false;
}

if(preg_match('/estados_cidades\.json$/', $requestUri)) {
  $redirectHome = true;
}

if ($redirectHome) {
  global $base_url;
  $url = $base_url.'/';
  $response = new Symfony\Component\HttpFoundation\RedirectResponse($url);
  $response->send(); // don't send the response yourself inside controller and form.
}
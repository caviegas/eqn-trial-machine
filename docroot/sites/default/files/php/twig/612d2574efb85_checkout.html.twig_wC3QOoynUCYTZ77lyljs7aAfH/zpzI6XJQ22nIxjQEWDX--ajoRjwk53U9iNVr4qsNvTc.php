<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/trialmachine/modules/trialmachine_campaign/templates/checkout.html.twig */
class __TwigTemplate_508500509a9eebf1eaa7c17d5e3cee34be258c5f133b7ec9d617ce02165fb96c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["campaign_url"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("entity.trialmachine_campaign.canonical", ["trialmachine_campaign" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "campaign", [], "any", false, false, true, 1), "id", [], "any", false, false, true, 1), "value", [], "any", false, false, true, 1)]);
        // line 2
        $context["login_page"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("module_nestle_connect.form_login");
        // line 3
        echo "<table>
    <tr>
        <th>Campanha</th>
        <th>Produto</th>
        <th>Peso</th>
        <th>Quantidade</th>
        <th>Valor</th>
    </tr>
    <tr>
        <td>";
        // line 12
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "campaign", [], "any", false, false, true, 12), "title", [], "any", false, false, true, 12), "value", [], "any", false, false, true, 12), 12, $this->source), "html", null, true);
        echo "</td>
        <td>";
        // line 13
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "campaign", [], "any", false, false, true, 13), "product_id", [], "any", false, false, true, 13), "entity", [], "any", false, false, true, 13), "title", [], "any", false, false, true, 13), "value", [], "any", false, false, true, 13), 13, $this->source), "html", null, true);
        echo "</td>
        <td>";
        // line 14
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "campaign", [], "any", false, false, true, 14), "product_id", [], "any", false, false, true, 14), "entity", [], "any", false, false, true, 14), "weight", [], "any", false, false, true, 14), "value", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
        echo "</td>
        <td>1</td>
        <td>R\$ 0,00</td>
    </tr>
</table>
<div class=\"checkout-footer\">
    <a class=\"back-to-campaign\" href=\"";
        // line 20
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["campaign_url"] ?? null), 20, $this->source), "html", null, true);
        echo "\">Voltar para página de campanha</a>
    ";
        // line 21
        if (($context["logged_in"] ?? null)) {
            // line 22
            echo "        <a class=\"next-step\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("trialmachine_campaign.delivery"));
            echo "\">Avançar</a>
    ";
        } else {
            // line 24
            echo "        <a class=\"next-step\" id=\"auth-button\">Faça log in para continuar</a>
    ";
        }
        // line 26
        echo "</div>        ";
    }

    public function getTemplateName()
    {
        return "modules/custom/trialmachine/modules/trialmachine_campaign/templates/checkout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 26,  83 => 24,  77 => 22,  75 => 21,  71 => 20,  62 => 14,  58 => 13,  54 => 12,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set campaign_url = path('entity.trialmachine_campaign.canonical', {'trialmachine_campaign': data.campaign.id.value}) %}
{% set login_page = path('module_nestle_connect.form_login') %}
<table>
    <tr>
        <th>Campanha</th>
        <th>Produto</th>
        <th>Peso</th>
        <th>Quantidade</th>
        <th>Valor</th>
    </tr>
    <tr>
        <td>{{ data.campaign.title.value }}</td>
        <td>{{ data.campaign.product_id.entity.title.value }}</td>
        <td>{{ data.campaign.product_id.entity.weight.value }}</td>
        <td>1</td>
        <td>R\$ 0,00</td>
    </tr>
</table>
<div class=\"checkout-footer\">
    <a class=\"back-to-campaign\" href=\"{{ campaign_url }}\">Voltar para página de campanha</a>
    {% if logged_in%}
        <a class=\"next-step\" href=\"{{ path('trialmachine_campaign.delivery') }}\">Avançar</a>
    {% else %}
        <a class=\"next-step\" id=\"auth-button\">Faça log in para continuar</a>
    {% endif %}
</div>        ", "modules/custom/trialmachine/modules/trialmachine_campaign/templates/checkout.html.twig", "/Applications/MAMP/htdocs/dig0031080-nestle-corporate-brazil/docroot/modules/custom/trialmachine/modules/trialmachine_campaign/templates/checkout.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 21);
        static $filters = array("escape" => 12);
        static $functions = array("path" => 1);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}

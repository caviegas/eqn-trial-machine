<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/trialmachine/modules/trialmachine_campaign/templates/checkout-credentials-block.html.twig */
class __TwigTemplate_d4138d07972ccefefb11ca00293385e19fdb05329d53d1c841ef46d8a3b83c96 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["delivery_path"] = $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("trialmachine_campaign.delivery");
        // line 2
        echo "<div id=\"auth-popup\">
    <div class=\"close\"></div>
    <h2>Para prosseguir, você precisa se identificar</h2>
    <p>Clique nos botões abaixo para fazer login ou criar uma conta no sistema de Eu Quero Nestlé.</p>
    <div class=\"auth-popup-buttons\">
        <a href=\"";
        // line 7
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("module_nestle_connect.form_login"));
        echo "?destination=";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["delivery_path"] ?? null), 7, $this->source), "html", null, true);
        echo "\">
            Entrar
        </a>
        <a href=\"";
        // line 10
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("module_nestle_connect.postsubmissionform"));
        echo "?destination=";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["delivery_path"] ?? null), 10, $this->source), "html", null, true);
        echo "\">
            Criar conta
        </a>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "modules/custom/trialmachine/modules/trialmachine_campaign/templates/checkout-credentials-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 10,  48 => 7,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set delivery_path = path('trialmachine_campaign.delivery') %}
<div id=\"auth-popup\">
    <div class=\"close\"></div>
    <h2>Para prosseguir, você precisa se identificar</h2>
    <p>Clique nos botões abaixo para fazer login ou criar uma conta no sistema de Eu Quero Nestlé.</p>
    <div class=\"auth-popup-buttons\">
        <a href=\"{{ path('module_nestle_connect.form_login') }}?destination={{ delivery_path }}\">
            Entrar
        </a>
        <a href=\"{{ path('module_nestle_connect.postsubmissionform') }}?destination={{ delivery_path }}\">
            Criar conta
        </a>
    </div>
</div>", "modules/custom/trialmachine/modules/trialmachine_campaign/templates/checkout-credentials-block.html.twig", "/Applications/MAMP/htdocs/dig0031080-nestle-corporate-brazil/docroot/modules/custom/trialmachine/modules/trialmachine_campaign/templates/checkout-credentials-block.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1);
        static $filters = array("escape" => 7);
        static $functions = array("path" => 1);

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}

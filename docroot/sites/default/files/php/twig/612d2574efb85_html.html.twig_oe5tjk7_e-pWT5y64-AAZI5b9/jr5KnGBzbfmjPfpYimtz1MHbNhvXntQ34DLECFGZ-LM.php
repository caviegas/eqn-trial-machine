<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/nest/templates/html.html.twig */
class __TwigTemplate_3386bf71b03b912b02ebf05f86c8018ecebb3d4777b5957951b7735e131b3128 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "<!DOCTYPE html>
<html";
        // line 29
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["html_attributes"] ?? null), 29, $this->source), "html", null, true);
        echo ">
  <head>
    <head-placeholder token=\"";
        // line 31
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 31, $this->source), "html", null, true);
        echo "\">
    <css-placeholder token=\"";
        // line 32
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 32, $this->source), "html", null, true);
        echo "\">
    <js-placeholder token=\"";
        // line 33
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 33, $this->source), "html", null, true);
        echo "\">
    <link rel=\"canonical\" href=\"";
        // line 34
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["canonical_url"] ?? null), 34, $this->source), "html", null, true);
        echo "\"/>
    <meta name=\"theme-color\" content=\"#cccccc\">
    <meta property=\"og:title\" content=\"";
        // line 36
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->safeJoin($this->env, $this->sandbox->ensureToStringAllowed(($context["head_title"] ?? null), 36, $this->source), " | "));
        echo "\" />
    <meta property=\"og:description\" content=\"";
        // line 37
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["current_node"] ?? null), "summary", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
        echo "\"/>
    <meta property=\"og:image\" content=\"";
        // line 38
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["current_node"] ?? null), "image_url", [], "any", false, false, true, 38), 38, $this->source), "html", null, true);
        echo "\" />
    <meta property=\"og:locale\" content=\"pt_BR\" />
    <meta name=\"description\" content=\"";
        // line 40
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["current_node"] ?? null), "summary", [], "any", false, false, true, 40), 40, $this->source), "html", null, true);
        echo "\"/>
    ";
        // line 41
        if (($context["is_front"] ?? null)) {
            // line 42
            echo "      <meta name=\"facebook-domain-verification\" content=\"43wouzas48vgsfyooblhiwipthud5xx\" />
      <title>";
            // line 43
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->safeJoin($this->env, $this->sandbox->ensureToStringAllowed(($context["head_title"] ?? null), 43, $this->source), " | "));
            echo "</title>
    ";
        } else {
            // line 45
            echo "      <title>";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed((($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = ($context["head_title"] ?? null)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4["title"] ?? null) : null), 45, $this->source), "html", null, true);
            echo "</title>
    ";
        }
        // line 47
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["current_node"] ?? null), "keywords", [], "any", false, false, true, 47)) {
            // line 48
            echo "      <meta name=\"keywords\" content=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["current_node"] ?? null), "keywords", [], "any", false, false, true, 48), 48, $this->source), "html", null, true);
            echo "\" />
    ";
        }
        // line 50
        echo "    <script>
      window.dataLayer = window.dataLayer || [];
      dataLayer.push({
        'event': 'Acesso Conteúdo',
        'ContentName': '";
        // line 54
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->safeJoin($this->env, $this->sandbox->ensureToStringAllowed(($context["head_title"] ?? null), 54, $this->source), " | "));
        echo "',
        'ContentCategory': '";
        // line 55
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["current_node"] ?? null), "category", [], "any", false, false, true, 55), 55, $this->source), "html", null, true);
        echo "',
        'ContentTags': '";
        // line 56
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["current_node"] ?? null), "tags", [], "any", false, false, true, 56), 56, $this->source), "html", null, true);
        echo "',
        ";
        // line 57
        if (($context["user_id"] ?? null)) {
            // line 58
            echo "        'visitorId': '";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["user_id"] ?? null), 58, $this->source), "html", null, true);
            echo "',
        ";
        }
        // line 60
        echo "      });
    </script>
    <!-- Google Tag Manager --> 
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MQ3KPRL');</script> 
    <!-- End Google Tag Manager -->
  </head>
  <body";
        // line 66
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["attributes"] ?? null), 66, $this->source), "html", null, true);
        echo " id=\"status_";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["logged_in"] ?? null), 66, $this->source), "html", null, true);
        echo "\">
    <!-- Google Tag Manager (noscript) --> 
    <noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-MQ3KPRL\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript> 
    <!-- End Google Tag Manager (noscript) -->
    ";
        // line 74
        echo "    ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_top"] ?? null), 74, $this->source), "html", null, true);
        echo "
    ";
        // line 75
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page"] ?? null), 75, $this->source), "html", null, true);
        echo "
    ";
        // line 76
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["page_bottom"] ?? null), 76, $this->source), "html", null, true);
        echo "
    <js-bottom-placeholder token=\"";
        // line 77
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["placeholder_token"] ?? null), 77, $this->source), "html", null, true);
        echo "\">
  </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/nest/templates/html.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 77,  158 => 76,  154 => 75,  149 => 74,  140 => 66,  132 => 60,  126 => 58,  124 => 57,  120 => 56,  116 => 55,  112 => 54,  106 => 50,  100 => 48,  97 => 47,  91 => 45,  86 => 43,  83 => 42,  81 => 41,  77 => 40,  72 => 38,  68 => 37,  64 => 36,  59 => 34,  55 => 33,  51 => 32,  47 => 31,  42 => 29,  39 => 28,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Default theme implementation for the basic structure of a single Drupal page.
 *
 * Variables:
 * - logged_in: A flag indicating if user is logged in.
 * - root_path: The root path of the current page (e.g., node, admin, user).
 * - node_type: The content type for the current node, if the page is a node.
 * - head_title: List of text elements that make up the head_title variable.
 *   May contain one or more of the following:
 *   - title: The title of the page.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site.
 * - page_top: Initial rendered markup. This should be printed before 'page'.
 * - page: The rendered page markup.
 * - page_bottom: Closing rendered markup. This variable should be printed after
 *   'page'.
 * - db_offline: A flag indicating if the database is offline.
 * - placeholder_token: The token for generating head, css, js and js-bottom
 *   placeholders.
 *
 * @see template_preprocess_html()
 *
 * @ingroup themeable
 */
#}
<!DOCTYPE html>
<html{{ html_attributes }}>
  <head>
    <head-placeholder token=\"{{ placeholder_token }}\">
    <css-placeholder token=\"{{ placeholder_token }}\">
    <js-placeholder token=\"{{ placeholder_token }}\">
    <link rel=\"canonical\" href=\"{{ canonical_url }}\"/>
    <meta name=\"theme-color\" content=\"#cccccc\">
    <meta property=\"og:title\" content=\"{{ head_title|safe_join(' | ') }}\" />
    <meta property=\"og:description\" content=\"{{ current_node.summary }}\"/>
    <meta property=\"og:image\" content=\"{{ current_node.image_url }}\" />
    <meta property=\"og:locale\" content=\"pt_BR\" />
    <meta name=\"description\" content=\"{{ current_node.summary }}\"/>
    {% if is_front %}
      <meta name=\"facebook-domain-verification\" content=\"43wouzas48vgsfyooblhiwipthud5xx\" />
      <title>{{ head_title|safe_join(' | ') }}</title>
    {% else %}
      <title>{{ head_title['title'] }}</title>
    {% endif %}
    {% if current_node.keywords %}
      <meta name=\"keywords\" content=\"{{ current_node.keywords }}\" />
    {% endif %}
    <script>
      window.dataLayer = window.dataLayer || [];
      dataLayer.push({
        'event': 'Acesso Conteúdo',
        'ContentName': '{{ head_title|safe_join(' | ') }}',
        'ContentCategory': '{{ current_node.category }}',
        'ContentTags': '{{ current_node.tags }}',
        {% if user_id %}
        'visitorId': '{{ user_id }}',
        {% endif %}
      });
    </script>
    <!-- Google Tag Manager --> 
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-MQ3KPRL');</script> 
    <!-- End Google Tag Manager -->
  </head>
  <body{{ attributes }} id=\"status_{{ logged_in }}\">
    <!-- Google Tag Manager (noscript) --> 
    <noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-MQ3KPRL\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript> 
    <!-- End Google Tag Manager (noscript) -->
    {#
      Keyboard navigation/accessibility link to main content section in
      page.html.twig.
    #}
    {{ page_top }}
    {{ page }}
    {{ page_bottom }}
    <js-bottom-placeholder token=\"{{ placeholder_token }}\">
  </body>
</html>
", "themes/custom/nest/templates/html.html.twig", "/Applications/MAMP/htdocs/dig0031080-nestle-corporate-brazil/docroot/themes/custom/nest/templates/html.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("if" => 41);
        static $filters = array("escape" => 29, "safe_join" => 36);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape', 'safe_join'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}

<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/nest/templates/page--checkout.html.twig */
class __TwigTemplate_65013d3da5a7290d4dae22fa9e6f7094c1f20567bb619f7e820faae980579791 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "
<header class=\"menu\">
  <div class=\"logo\">
      <a href=\"";
        // line 56
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
        echo "\">
          <img alt=\"Nestlé Com Você\" title=\"Nestlé Com Você\" src=\"https://res.cloudinary.com/meeg-cloud/image/upload/v1627240132/TM/comvoce-gray_obdx4e.svg\"/>
      </a>
  </div>
  <ul class=\"menu_options\">
      <li><a href=\"";
        // line 61
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
        echo "\">Eu quero Nestlé</a></li>
      <li><a href=\"https://www.nestle.com.br/comvoce/experiencias\">Experiências</a></li>
      <li><a href=\"https://www.nestle.com.br/comvoce/promocoes\">Promoções</a></li>
      <li><a href=\"https://www.nestle.com.br/comvoce/produtos\">Produtos</a></li>
      <li><a href=\"https://www.nestle.com.br/comvoce/pesquisas\">Pesquisas</a></li>
  </ul>
  <div class=\"menu_mobile\">
      <span></span>
      <span></span>
      <span></span>
  </div>
  ";
        // line 72
        if (($context["logged_in"] ?? null)) {
            // line 73
            echo "    <div class=\"hello_logged\">
        <div>Olá, ";
            // line 74
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["cut_username"] ?? null), 74, $this->source), "html", null, true);
            echo "</div>
        <ul class=\"hello_logged_options\">
            <li><a href=\"";
            // line 76
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "painel/pedidos\">Pedidos</a></li>
            <li><a href=\"";
            // line 77
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "painel/identificacao\">Identificação</a></li>
            <li><a href=\"";
            // line 78
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "painel/entrega\">Entrega</a></li>
            <li><a href=\"";
            // line 79
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "painel/interesses\">Interesses</a></li>
            <li><a href=\"";
            // line 80
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "user/logout\">Sair</a></li>
        </ul>
    </div>
  ";
        } else {
            // line 84
            echo "    <div class=\"hello_unlogged\">
        <a href=\"";
            // line 85
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "user/entrar\" class=\"login_link\">Fazer login</a>
        <a href=\"";
            // line 86
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "user/registrar\" class=\"create_link\">Criar conta</a>
    </div>
  ";
        }
        // line 89
        echo "</header>

<section class=\"campaign\">
  <div class=\"follow_up_mobile\">
    ";
        // line 93
        if (($context["logged_in"] ?? null)) {
            // line 94
            echo "        <div><a href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "painel/pedidos\">Pedidos</a></div>
        <div><a href=\"";
            // line 95
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "painel/identificacao\">Identificação</a></div>
        <div><a href=\"";
            // line 96
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "painel/interesses\">Interesses</a></div>
        <div><a href=\"";
            // line 97
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "painel/entrega\">Entrega</a></div>
    ";
        } else {
            // line 99
            echo "        <a href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "user/entrar\" class=\"login_link\">Fazer login</a>
        <a href=\"";
            // line 100
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
            echo "user/registrar\" class=\"create_link\">Criar conta</a>
    ";
        }
        // line 102
        echo "  </div>
  ";
        // line 103
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 103)) {
            // line 104
            echo "    <div class=\"highlighted\">
      <aside class=\"layout-container section clearfix\" role=\"complementary\">
        ";
            // line 106
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 106), 106, $this->source), "html", null, true);
            echo "
      </aside>
    </div>
  ";
        }
        // line 110
        echo "  ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 110), 110, $this->source), "html", null, true);
        echo "
</section>
<footer>
  <ul class=\"information_footer\">
      <li><a href=\"";
        // line 114
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
        echo "detalhes/sobre\">Sobre</a></li>
      <li><a href=\"";
        // line 115
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
        echo "detalhes/politica-de-privacidade\">Política de Privacidade</a></li>
      <li><a href=\"";
        // line 116
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getUrl("<front>"));
        echo "detalhes/duvidas-frequentes\">Dúvidas Frequentes</a></li>
  </ul>
  <div class=\"logo_footer\">
      <img alt=\"Nestlé Com Você\" title=\"Nestlé Com Você\" src=\"https://res.cloudinary.com/meeg-cloud/image/upload/v1627240133/TM/comvoce-white_tzphsg.svg\"/>
  </div>
  <p>&copy;2021 Nestlé Brasil Ltda. Todos os direitos reservados.</p>
</footer>";
    }

    public function getTemplateName()
    {
        return "themes/custom/nest/templates/page--checkout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 116,  176 => 115,  172 => 114,  164 => 110,  157 => 106,  153 => 104,  151 => 103,  148 => 102,  143 => 100,  138 => 99,  133 => 97,  129 => 96,  125 => 95,  120 => 94,  118 => 93,  112 => 89,  106 => 86,  102 => 85,  99 => 84,  92 => 80,  88 => 79,  84 => 78,  80 => 77,  76 => 76,  71 => 74,  68 => 73,  66 => 72,  52 => 61,  44 => 56,  39 => 53,);
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Bartik's theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template normally located in the
 * core/modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.header: Items for the header region.
 * - page.highlighted: Items for the highlighted region.
 * - page.primary_menu: Items for the primary menu region.
 * - page.secondary_menu: Items for the secondary menu region.
 * - page.featured_top: Items for the featured top region.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.featured_bottom_first: Items for the first featured bottom region.
 * - page.featured_bottom_second: Items for the second featured bottom region.
 * - page.featured_bottom_third: Items for the third featured bottom region.
 * - page.footer_first: Items for the first footer column.
 * - page.footer_second: Items for the second footer column.
 * - page.footer_third: Items for the third footer column.
 * - page.footer_fourth: Items for the fourth footer column.
 * - page.footer_fifth: Items for the fifth footer column.
 * - page.breadcrumb: Items for the breadcrumb region.
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}

<header class=\"menu\">
  <div class=\"logo\">
      <a href=\"{{ url('<front>') }}\">
          <img alt=\"Nestlé Com Você\" title=\"Nestlé Com Você\" src=\"https://res.cloudinary.com/meeg-cloud/image/upload/v1627240132/TM/comvoce-gray_obdx4e.svg\"/>
      </a>
  </div>
  <ul class=\"menu_options\">
      <li><a href=\"{{ url('<front>') }}\">Eu quero Nestlé</a></li>
      <li><a href=\"https://www.nestle.com.br/comvoce/experiencias\">Experiências</a></li>
      <li><a href=\"https://www.nestle.com.br/comvoce/promocoes\">Promoções</a></li>
      <li><a href=\"https://www.nestle.com.br/comvoce/produtos\">Produtos</a></li>
      <li><a href=\"https://www.nestle.com.br/comvoce/pesquisas\">Pesquisas</a></li>
  </ul>
  <div class=\"menu_mobile\">
      <span></span>
      <span></span>
      <span></span>
  </div>
  {% if logged_in %}
    <div class=\"hello_logged\">
        <div>Olá, {{ cut_username }}</div>
        <ul class=\"hello_logged_options\">
            <li><a href=\"{{ url('<front>') }}painel/pedidos\">Pedidos</a></li>
            <li><a href=\"{{ url('<front>') }}painel/identificacao\">Identificação</a></li>
            <li><a href=\"{{ url('<front>') }}painel/entrega\">Entrega</a></li>
            <li><a href=\"{{ url('<front>') }}painel/interesses\">Interesses</a></li>
            <li><a href=\"{{ url('<front>') }}user/logout\">Sair</a></li>
        </ul>
    </div>
  {% else %}
    <div class=\"hello_unlogged\">
        <a href=\"{{ url('<front>') }}user/entrar\" class=\"login_link\">Fazer login</a>
        <a href=\"{{ url('<front>') }}user/registrar\" class=\"create_link\">Criar conta</a>
    </div>
  {% endif %}
</header>

<section class=\"campaign\">
  <div class=\"follow_up_mobile\">
    {% if logged_in %}
        <div><a href=\"{{ url('<front>') }}painel/pedidos\">Pedidos</a></div>
        <div><a href=\"{{ url('<front>') }}painel/identificacao\">Identificação</a></div>
        <div><a href=\"{{ url('<front>') }}painel/interesses\">Interesses</a></div>
        <div><a href=\"{{ url('<front>') }}painel/entrega\">Entrega</a></div>
    {% else %}
        <a href=\"{{ url('<front>') }}user/entrar\" class=\"login_link\">Fazer login</a>
        <a href=\"{{ url('<front>') }}user/registrar\" class=\"create_link\">Criar conta</a>
    {% endif %}
  </div>
  {% if page.highlighted %}
    <div class=\"highlighted\">
      <aside class=\"layout-container section clearfix\" role=\"complementary\">
        {{ page.highlighted }}
      </aside>
    </div>
  {% endif %}
  {{ page.content }}
</section>
<footer>
  <ul class=\"information_footer\">
      <li><a href=\"{{ url('<front>') }}detalhes/sobre\">Sobre</a></li>
      <li><a href=\"{{ url('<front>') }}detalhes/politica-de-privacidade\">Política de Privacidade</a></li>
      <li><a href=\"{{ url('<front>') }}detalhes/duvidas-frequentes\">Dúvidas Frequentes</a></li>
  </ul>
  <div class=\"logo_footer\">
      <img alt=\"Nestlé Com Você\" title=\"Nestlé Com Você\" src=\"https://res.cloudinary.com/meeg-cloud/image/upload/v1627240133/TM/comvoce-white_tzphsg.svg\"/>
  </div>
  <p>&copy;2021 Nestlé Brasil Ltda. Todos os direitos reservados.</p>
</footer>", "themes/custom/nest/templates/page--checkout.html.twig", "/Applications/MAMP/htdocs/dig0031080-nestle-corporate-brazil/docroot/themes/custom/nest/templates/page--checkout.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("if" => 72);
        static $filters = array("escape" => 74);
        static $functions = array("url" => 56);

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}

(function ($,jQuery) {
	"use strict";
	var carousels = function () {
		$(".pictures").owlCarousel({
			loop: false,
			center: false,
			margin: 15,
			responsiveClass: true,
			nav: true,
			dots: false,
			responsive: {
	            0: {
	            	items: 1
	            },
	            400: {
	            	items: 1
	            },
	            680: {
	            	items: 1
	            },
	            1000: {
	            	items: 2
		            //autoplaySpeed:3000,
		            //autoplayTimeout:6000,
		            //autoplay:true,
		            //autoplayHoverPause:true,
		            //slideSpeed: 600,
		            //paginationSpeed: 600,
		            //rewind: true
            	}
        	}
		});
	};
	(function ($) { carousels(); })(jQuery);
})(jQuery);

(function ($,jQuery) {
	$(document).ready(function(){
        $(".carousel_options a:nth-child(1)").click(function(){
            $(".carousel_options a:nth-child(1)").addClass("active");
            $(".carousel_options a:nth-child(2)").removeClass("active");
            $(".content_images_home .for_you_home").addClass("active");
            $(".see_it_all_home").removeClass("active");
        });
    });
    $(document).ready(function(){
        $(".carousel_options a:nth-child(2)").click(function(){
            $(".carousel_options a:nth-child(2)").addClass("active");
            $(".carousel_options a:nth-child(1)").removeClass("active");
            $(".see_it_all_home").addClass("active");
            $(".content_images_home .for_you_home").removeClass("active");
        });
    });
    $(document).ready(function(){
        $("header.menu .hello_logged div").click(function(){
            $("header.menu .hello_logged").toggleClass("active");
        });
    });
    $(document).ready(function(){
        $("header.menu .menu_mobile").click(function(){
            $("body").toggleClass("menu_active");
        });
    });
    $(document).ready(function(){
        $("#auth-button").click(function(){
            $("#auth-popup").addClass("active");
        });
        $("#auth-popup .close").click(function(){
            $("#auth-popup").removeClass("active");
        });
    });
    $(document).ready(function(){
        $(".question_more").click(function(){
            $(this).parent().parent().addClass("active");
        });
    });
    $(document).ready(function(){
        $(".question_less").click(function(){
            $(this).parent().parent().removeClass("active");
        });
    });
    $(document).ready(function(){
        $(".management_menu").click(function(){
            $(".about_menu").toggleClass("active");
        });
    });
    $(document).ready(function(){
        $(".order-header-action").click(function(){
            $(this).parent().parent().toggleClass("active");
        });
    });
})(jQuery);
